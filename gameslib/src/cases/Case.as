package cases
{
	import common.Background;
	
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import ru.flexdev.ui.Button;
	import ru.flexdev.ui.Image;
	import ru.flexdev.ui.ScrollerEx;
	import ru.flexdev.ui.SpriteEx;
	
	public class Case extends Sprite
	{
		[Embed(source="assets/plakat.jpg")]private var plakatClass:Class;
		[Embed(source="assets/case1.png")]private var case1Class:Class;
		[Embed(source="assets/case2.png")]private var case2Class:Class;
		[Embed(source="assets/case3.png")]private var case3Class:Class;
		[Embed(source="assets/case4.png")]private var case4Class:Class;
		
		
		public static const WIDTH:int=871;
		public static const HEIGHT:int=542;
		
		private var background:Background;

		private var index:int;
		private var btnNext:Button;
		private var plakat:SpriteEx;
		
		public function Case(index:int)
		{
			super();
			this.index=index;
			this.background=new Background(WIDTH, HEIGHT);
			this.addChild(this.background);
			this.background.setTitles('', '').hideButton('toMenu').hideButton('toMap');
			
			var scroller:ScrollerEx=new ScrollerEx();
			scroller.x=53;
			scroller.y=55;
			scroller.width=790;
			scroller.height=415;
			this.background.addChild(scroller);
			
			var bmp:Bitmap;
			if(index==1)bmp=new case1Class() as Bitmap;
			if(index==2)bmp=new case2Class() as Bitmap;
			if(index==3)bmp=new case3Class() as Bitmap;
			if(index==4)bmp=new case4Class() as Bitmap;
			var image:Image=new Image();
			image.source=bmp.bitmapData;
			scroller.addChild(image);
			
			
			
			this.btnNext=new Button();
			this.background.addChild(this.btnNext);
			this.btnNext.skinClass=common.ButtonSkin;
			this.btnNext.label='Далее';
			this.btnNext.width=100;
			this.btnNext.height=35;
			this.btnNext.horizontalCenter=0;
			this.btnNext.y=image.y+image.height+20;
			
			if(index==4)
			{
				this.background.hideButton('cross');
				scroller.addChild(this.btnNext);
				this.btnNext.addEventListener(MouseEvent.CLICK, this.onButtonClick);
				this.plakat=new SpriteEx();
				var img:Bitmap=new plakatClass() as Bitmap;
				this.plakat.addChild(img);
				this.plakat.width=img.width;
				this.plakat.height=img.height;
				this.plakat.x=(WIDTH-this.plakat.width)/2;
				this.plakat.y=(HEIGHT-this.plakat.height)/2+40;
				this.addChild(this.plakat);
				this.plakat.visible=false;
			}
			
		}
		
		private function onButtonClick(event:MouseEvent):void
		{
			this.plakat.visible=true;
			this.background.visible=false;
			this.btnNext.removeEventListener(MouseEvent.CLICK, this.onButtonClick);
			this.btnNext.addEventListener(MouseEvent.CLICK, this.onCloseClick);
			this.plakat.addChild(this.btnNext);
			this.btnNext.label='Согласен';
			this.btnNext.bottom=15;
		}
		
		private function onCloseClick(event:MouseEvent):void
		{
			this.dispatchEvent(new Event('game_close', true));
		}
		
		public function center(parentWidht:int, parentHeight:int):void
		{
			this.x=(parentWidht-WIDTH)/2;
			this.y=(parentHeight-HEIGHT)/2;
		}
	}
}