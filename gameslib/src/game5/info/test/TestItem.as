package game5.info.test
{
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import common.Common;
	
	import fontmanager.FontManager;
	
	import ru.flexdev.ui.SpriteEx;
	
	public class TestItem extends SpriteEx
	{
		private var _correct:Boolean;
		private var selected:Boolean=false;
		private var passed:Boolean=false;
		private var title:TextField;
		
		public function TestItem(text:String, correct:Boolean, passed:Boolean=false)
		{
			super();
			this.width=746;

			this._correct=correct;
			this.passed=passed;
			this.buttonMode=!this.passed;
			
			this.title=FontManager.instance.textField;
			var frmt:TextFormat=FontManager.instance.textFormat(13, Common.GRAY, 'bold');
			this.title.defaultTextFormat=frmt;
			this.title.width=740;
			this.title.wordWrap=true;
			this.title.x=6;
			this.title.y=-4;
			this.title.text=text;
			this.addChild(this.title);
			
			this.title.height=this.title.textHeight+4;
			this.height=this.title.height;
			
				
			this.addEventListener(MouseEvent.CLICK, this.onClick);
			
			if(this.passed)this.selected=this._correct;
			this.update();
		}
		
		private function onClick(event:MouseEvent):void
		{
			if(this.passed)return;
			this.selected=!this.selected;
			this.update();
		}
		
		private function update():void
		{
			var frmt:TextFormat=this.title.getTextFormat();
			if(this.selected)
			{
				frmt.color=0xf7ac3b;
			}
			else
			{
				frmt.color=Common.GRAY;
			}
			
			this.title.setTextFormat(frmt);
		}
		
		public function get correct():Boolean
		{
			return this._correct==this.selected;
		}
		
		public function get isCorrect():Boolean
		{
			return this._correct;
		}
	}
}