package game5.info.test
{
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import common.Common;
	
	import fontmanager.FontManager;
	
	import ru.flexdev.ui.SpriteEx;
	
	public class Test extends SpriteEx
	{
		private var _data:Object;
		private var question:TextField;
		private var items:Vector.<TestItem>;
		private var itemsHolder:SpriteEx;
		
		
		
		public function Test()
		{
			super();
			this.question=FontManager.instance.textField;
			var frmt:TextFormat=FontManager.instance.textFormat(15, Common.GRAY, 'bold');
			this.question.defaultTextFormat=frmt;
			this.question.wordWrap=true;
			this.question.width=746;
			this.question.x=-2;
			this.question.y=-5;
			this.addChild(this.question);
		
			this.itemsHolder=new SpriteEx();
			this.addChild(this.itemsHolder);

		}
		
		public function set data(value:Object):void
		{
			this._data=value;
			var passed:Boolean=value['passed'];
			
			this.items=new Vector.<TestItem>();
			//this.itemsHolder.removeChildren();
			Common.removeChildren(this.itemsHolder);
			
			this.question.text=this._data.question;
			this.question.height=this.question.textHeight+4;

			this.itemsHolder.y=this.question.y+this.question.height+16;
			
			var prevItem:TestItem=null;
			
			for(var i:int=0;i<this._data['answers'].length;i++)
			{
				var item_data:Object=this._data['answers'][i];
				var item:TestItem=new TestItem(item_data.text, item_data.correct, passed);
				this.itemsHolder.addChild(item);
				if(prevItem!=null)item.y=prevItem.y+prevItem.height+10;
				prevItem=item;
				this.items.push(item);
			}
			this.height=this.itemsHolder.y+this.itemsHolder.contentHeight;
		}
		
		public function get correct():Boolean
		{
			for(var i:int=0;i<this.items.length;i++)
			{
				if(this.items[i].correct==false)return false;
			}
			return true;
		}
		
		public function get count():int
		{
			var res:int=0;
			for(var i:int=0;i<this.items.length;i++)
			{
				if(this.items[i].isCorrect)res++;
			}
			return res;
		}
	}
}