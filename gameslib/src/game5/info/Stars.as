package game5.info
{
	import common.Common;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	
	import ru.flexdev.ui.SpriteEx;
	
	public class Stars extends SpriteEx
	{
		[Embed(source="assets/star.png")]private var star:Class;
		
		private var baseImage:BitmapData;
		public function Stars()
		{
			super();
			this.baseImage=(new this.star() as Bitmap).bitmapData;
			this.height=this.baseImage.height;
			this.visible=false;
		}
		
		public function set count(value:int):void
		{
			//this.removeChildren();
			Common.removeChildren(this);
			
			if(value==0)
			{
				this.visible=false;
				return;
			}
			
			for(var i:int=0;i<value;i++)
			{
				var img:Bitmap=new Bitmap(this.baseImage.clone());
				this.addChild(img);
				img.x=i*41;
			}
			
			this.width=this.baseImage.width*value+(value-1)*(41-this.baseImage.width);
			this.visible=true;
		}
	}
}