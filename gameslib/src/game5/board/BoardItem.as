package game5.board
{
	import common.Common;
	
	import flash.display.Bitmap;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import fontmanager.FontManager;
	
	import ru.flexdev.events.EventEx;
	import ru.flexdev.ui.Image;
	import ru.flexdev.ui.Rect;
	import ru.flexdev.ui.SpriteEx;
	
	public class BoardItem extends SpriteEx
	{
		public var id:int;
		private var _passed:Boolean=false;
		private var data:Object;
		
		private var rect:Rect;
		private var overSprite:Rect;
		
		public function BoardItem(data:Object)
		{
			super();
			this.data=data;
			this.id=this.data.id;
			this.width=142;
			this.height=142;
			this.buttonMode=true;
			
			this.rect=new Rect();
			this.addChild(this.rect);
			this.rect.width=this.width;
			this.rect.height=this.height;
			
			var image:Image=new Image();
			var bmp:Bitmap=(new this.data.image() as Bitmap);
			image.source=bmp.bitmapData;
			image.x=6;
			image.y=6;
			this.addChild(image);
			image.mouseEnabled=false;
			
			this.addEventListener(MouseEvent.ROLL_OVER, this.onMouseEvent);
			this.addEventListener(MouseEvent.ROLL_OUT, this.onMouseEvent);
			this.addEventListener(MouseEvent.CLICK, this.onMouseEvent);
			
			var tf:TextField=FontManager.instance.textField;
			var frmt:TextFormat=FontManager.instance.textFormat(15, Common.GRAY, 'black');
			frmt.align='center';
			tf.defaultTextFormat=frmt;
			tf.wordWrap=true;
			tf.width=210;
			tf.y=147;
			tf.text=this.data.name;
			this.addChild(tf);
			tf.height=tf.textHeight+4;
			tf.x=(this.width-tf.width)/2;
			
			
			var tf2:TextField=FontManager.instance.textField;
			frmt=FontManager.instance.textFormat(15, Common.GRAY, 'regular');
			frmt.align='center';
			tf2.defaultTextFormat=frmt;
			tf2.wordWrap=true;
			tf2.width=250;
			tf2.y=tf.y+tf.height;
			tf2.text=this.data.position;
			this.addChild(tf2);
			tf2.height=tf2.textHeight+4;
			tf2.x=(this.width-tf2.width)/2;
			
			
			this.overSprite=new Rect();
			//this.overSprite.bounds(6, 6, 6, 6);
			this.overSprite.width=142;
			this.overSprite.height=142;
			this.overSprite.fill(0xf7ac3b, 0.5);
			this.overSprite.visible=false;
			this.addChild(this.overSprite);
			
			this.update();
			
			this.height=tf2.y+tf2.height+10;
		}
		
		public function get passed():Boolean
		{
			return this._passed;
		}

		public function set passed(value:Boolean):void
		{
			this._passed = value;
			this.update();
		}
		
		private function onMouseEvent(event:MouseEvent):void
		{
			switch(event.type)
			{
				case MouseEvent.ROLL_OVER:
					//if(event.localX>this.width || event.localY>this.height)return;
					this.update(true);
					break;
				
				case MouseEvent.ROLL_OUT:
					this.update(false);
					break;
				
				case MouseEvent.CLICK:
					this.dispatchEvent(new EventEx('pers_select', this, true));
					break;
			}
		}
		
		private function update(value:Boolean=false):void
		{
			if(this.passed)
			{
				this.rect.fill(0xf7ac3b, 1);
				//return;
			}
			
			if(value)
			{
				//this.fill(0xf7ac3b, 1);
				this.overSprite.visible=true;
			}
			else
			{
				//this.fill(0xffffff, 0.01);
				this.overSprite.visible=false;
			}
		}
	}
}
