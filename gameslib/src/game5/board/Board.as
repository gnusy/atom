package game5.board
{
	import by.blooddy.crypto.serialization.JSON;
	
	import game5.Game5;
	
	import ru.flexdev.ui.ScrollerEx;
	import ru.flexdev.ui.SpriteEx;
	
	public class Board extends SpriteEx
	{
		private var scroller:ScrollerEx;
		private var peoples:Array;
		
		private var items:Vector.<BoardItem>;
		
		public function Board(peoples:Array)
		{
			super();
			this.peoples=peoples;
			this.width=Game5.WIDTH;
			this.height=Game5.HEIGHT;
			
			
			this.scroller=new ScrollerEx();
			this.addChild(this.scroller);
			this.scroller.width=this.width-40;
			this.scroller.height=this.height-79-20;
			this.scroller.y=79;
			
			
			this.items=new Vector.<BoardItem>();
			for(var i:int=0;i<this.peoples.length;i++)
			{
				var item:BoardItem=new BoardItem(this.peoples[i]);
				this.scroller.addChild(item);
				this.items.push(item);
				
				var x_modificator:int=i-3*Math.floor(i/3);
				var y_modificator:int=Math.floor(i/3);
				item.x=102+x_modificator*246;
				item.y=0+y_modificator*251;
			}
		}
		
		public function set suspendData(value:String):void
		{
			if(value=='')value='{}';
			var data:Object=JSON.decode(value);
			for(var i:int=0;i<this.items.length;i++)
			{
				var name:String='pers'+String(this.items[i].id);
				if(data.hasOwnProperty(name))this.items[i].passed=data[name];
			}
		}
		
		public function get suspendData():String
		{
			var res:Object=new Object;
			for(var i:int=0;i<this.items.length;i++)
			{
				var name:String='pers'+String(this.items[i].id);
				var passed:Boolean=this.items[i].passed;
				res[name]=passed;
			}
			return JSON.encode(res);	
		}
		
		public function get complete():Boolean
		{
			for(var i:int=0;i<this.items.length;i++)
			{
				if(this.items[i].passed==false)return false;
			}
			return true;	
		}
	}
}