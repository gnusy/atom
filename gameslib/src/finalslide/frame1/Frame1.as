package finalslide.frame1
{
	import flash.display.Bitmap;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.utils.setTimeout;
	
	import caurina.transitions.Tweener;
	
	import common.Background;
	import common.ButtonSkin;
	
	import finalslide.Frame;
	
	import ru.flexdev.ui.Button;
	import ru.flexdev.ui.Image;
	
	public class Frame1 extends Frame
	{
		private var background:Background;
		
		private var button:Button;
		private var test:Test;
		private var reflection:Reflection;
		
		public function Frame1(width:int, height:int)
		{
			super(width, height);
			
			this.background=new Background(width, height);
			this.addChild(this.background);
			this.background.setTitles('Ваше путешествие подходит к концу!\nОсталось только вспомнить и ответить, что оказывает максимальное\nвлияние на достижение целей дивизиона и предприятий?', '');
			this.background.hideButton('toMap').hideButton('toMenu');
			this.background.topTitle('title1', 56);
			//this.background.topTitle('title2', 53);
			//this.background.top=75;
			//this.background.left=368;
			
			this.button=new Button();
			this.button.left=54;
			this.button.bottom=58;
			this.button.width=120;
			this.button.height=28;
			this.button.label='Проверить';
			this.button.skinClass=common.ButtonSkin;
			this.background.addChild(this.button);
			this.button.addEventListener(MouseEvent.CLICK, this.onCheckTest);
			
			this.test=new Test();
			this.background.addChild(this.test);
			this.test.bounds(55, 151);
			
			this.reflection=new Reflection();
			this.reflection.bounds(0, 0, 0, 0);
			this.reflection.addEventListener(Event.CLOSE, this.onReflectionClose);
			this.addChild(this.reflection);
		}
		
		override public function show(now:Boolean=false):void
		{
			super.show(now)
		
		}
		
		override public function hide():void
		{
			super.hide()
		}
		
		private function onCheckTest(event:MouseEvent):void
		{
			if(this.test.complete==false)
			{
				this.reflection.show();
				return;
			}
			else
			{
				super.complete();
			}
		}
		
		private function onReflectionClose(event:Event):void
		{
			this.reflection.hide();
		}
		
		
	}
}