package finalslide.frame1
{
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import common.Common;
	
	import fontmanager.FontManager;
	
	import ru.flexdev.ui.Rect;
	
	public class Test extends Rect
	{
		private var answers:Array=	[
										{text:'Количество предприятий', correct:false},
										{text:'Ориентация на 100%-ное достижение результата', correct:true},
										{text:'Следование корпоративным ценностям', correct:true},
										{text:'Количество сотрудников', correct:false},
										
										{text:'Продуманная стратегия', correct:true},
										{text:'Выполнение всех задач и проектов  точно в срок', correct:true},
										{text:'Эффективное использование ресурсов', correct:true},
										{text:'Постоянное внедрение сотрудниками\nв свою  работу лучших практик', correct:true}
									];
		
		private var items:Vector.<TestItem>;
		
		public function Test()
		{
			super();
			this.width=460;
			this.height=210;
			
			var title:TextField=FontManager.instance.textField;
			var frmt:TextFormat=FontManager.instance.textFormat(15, Common.GRAY, 'bold');
			title.defaultTextFormat=frmt;
			title.width=460;
			title.x=-3;
			title.y=-4;
			title.text='Выберите шесть правильных вариантов ответа';
			this.addChild(title);
			
			this.items=new Vector.<TestItem>();
			
			for(var i:int=0;i<this.answers.length;i++)
			{
				var item:TestItem=new TestItem(this.answers[i].text, this.answers[i].correct);
				
				var y_modificator:int=i-4*Math.floor(i/4);
				var x_modificator:int=Math.floor(i/4);
				
				item.left=380*x_modificator;
				item.top=45+50*y_modificator;
				this.addChild(item);
				this.items.push(item);
			}
		}
		
		public function get complete():Boolean
		{
			for(var i:int=0;i<this.items.length;i++)
			{
				if(this.items[i].correct==false)return false;
			}
			
			return true;
		}
	}
}