package finalslide.frame1
{
	import flash.display.Bitmap;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import common.Common;
	
	import fontmanager.FontManager;
	
	import ru.flexdev.ui.SpriteEx;
	
	public class TestItem extends SpriteEx
	{
		[Embed(source="assets/arrow_gray.png")]private var arrow_grayClass:Class;
		[Embed(source="assets/arrow_orange.png")]private var arrow_orangeClass:Class;
		
		private var _correct:Boolean;
		private var selected:Boolean=false;
		
		private var title:TextField;
		private var arrow_gray:Bitmap;
		private var arrow_orange:Bitmap;
		
		public function TestItem(text:String, correct:Boolean)
		{
			super();
			this.width=425;
			this.height=8;
			this.buttonMode=true;
			this._correct=correct;
			
			this.arrow_gray=new this.arrow_grayClass() as Bitmap;
			this.arrow_gray.smoothing=true;
			this.addChild(this.arrow_gray);
			
			this.arrow_orange=new this.arrow_orangeClass() as Bitmap;
			this.arrow_orange.smoothing=true;
			this.addChild(this.arrow_orange);
			
			
			this.title=FontManager.instance.textField;
			var frmt:TextFormat=FontManager.instance.textFormat(17, Common.GRAY, 'regular');
			this.title.wordWrap=true;
			this.title.defaultTextFormat=frmt;
			this.title.width=350;
			
			this.title.x=12;
			this.title.y=-6;
			this.title.text=text;
			this.title.height=this.title.textHeight+4;
			this.height=this.title.height;
			this.addChild(this.title);
			
			this.update();	
			this.addEventListener(MouseEvent.CLICK, this.onClick);
			
		}
		
		private function onClick(event:MouseEvent):void
		{
			this.selected=!this.selected;
			this.update();
		}
		
		private function update():void
		{
			var frmt:TextFormat=this.title.getTextFormat();
			if(this.selected)
			{
				this.arrow_gray.visible=false;
				this.arrow_orange.visible=true;
				frmt.color=Common.ORANGE;
			}
			else
			{
				this.arrow_gray.visible=true;
				this.arrow_orange.visible=false;
				frmt.color=Common.GRAY;
			}
			
			this.title.setTextFormat(frmt);
		}
		
		public function get correct():Boolean
		{
			return this._correct==this.selected;
		}
	}
}