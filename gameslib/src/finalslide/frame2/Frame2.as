package finalslide.frame2
{
	import caurina.transitions.Tweener;
	
	import common.Background;
	import common.ButtonSkin;
	
	import finalslide.Frame;
	
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.utils.setTimeout;
	
	import game4.frame5.Rotator;
	
	import ru.flexdev.ui.Button;
	import ru.flexdev.ui.Rect;
	
	public class Frame2 extends Frame
	{
		private var background:Background;
		//private var stars:Stars;
		private var rotator:Rotator;
		private var backMask:Rect;
		
		public function Frame2(width:int, height:int)
		{
			super(width, height);
			
			this.background=new Background(862, 446);
			this.addChild(this.background);
			this.background.setTitles('Миссия выполнена! Поздравляем!', 'Оставайтесь с нами,\nи вместе мы достигнем большего!');
			this.background.hideButton('toMap').hideButton('toMenu');
			this.background.topTitle('title2', 88);
			this.background.top=75;
			this.background.horizontalCenter=0;
			
			var button:Button=new Button();
			button.left=54;
			button.top=158;
			button.width=90;
			button.height=28;
			button.label='Далее';
			button.skinClass=common.ButtonSkin;
			this.background.addChild(button);
			button.addEventListener(MouseEvent.CLICK, this.onMouseClick);
		
			this.backMask=new Rect();
			this.backMask.width=1024;
			this.backMask.height=768;
			this.backMask.fill(0x000000, 0.5);
			this.addChild(this.backMask);
			this.backMask.visible=false;
			this.backMask.alpha=0;
			
			this.rotator=new Rotator();
			this.rotator.centerPosition=new Point(649, 298);
			this.addChild(this.rotator);
		}
		
		override public function show(now:Boolean=false):void
		{
			super.show(now)
			this.rotator.start();
		}
		
		override public function hide():void
		{
			super.hide()
			//this.stars.stop();
		}
		
		private function onMouseClick(event:MouseEvent):void
		{
			//super.complete();
			
			var local:Point=this.globalToLocal(new Point(0, 0));
			this.backMask.x=local.x;
			this.backMask.y=local.y;
			
			this.backMask.x=-76.5;
			this.backMask.y=-113;
			
			this.backMask.visible=true;
			
			
			var scale:Number=1.9;
			var localCenter:Point=this.globalToLocal(new Point(1024/2, 768/2));
			
			localCenter.x=435.5;
			localCenter.y=271+93/2;
			
			Tweener.addTween(this.rotator, {scaleX:scale, scaleY:scale, centerX:localCenter.x, centerY:localCenter.y, time:1, transition:'linear'});
			Tweener.addTween(this.backMask, {alpha:1, time:0.5, transition:'linear'});
			
			setTimeout(super.complete, 4000);
			
		}
	}
}