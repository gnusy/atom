package finalslide.frame2
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import flash.utils.setInterval;
	import flash.utils.setTimeout;
	
	import caurina.transitions.Tweener;
	
	import ru.flexdev.ui.SpriteEx;
	
	public class Star extends SpriteEx
	{
		public function Star(image:Class)
		{
			super();
			
			var img:Bitmap=new image() as Bitmap;
			img.smoothing=true;
			this.addChild(img);
			/*
			this.width=img.width;
			this.height=img.height;
			*/
			img.x=-img.width/2;
			img.y=-img.height/2;
			
			setInterval(this.tumble, 10000);
		
		}
		
		public function moveTo(point:Point, now:Boolean=false):void
		{
			if(now)
			{
				this.x=point.x-this.width/2;
				this.y=point.y-this.height/2;
				return;
			}
			/*
			var new_x:Number=point.x-this.width/2;
			var new_y:Number=point.y-this.height/2;
			*/
			var new_x:Number=point.x;
			var new_y:Number=point.y;
			
			Tweener.addTween(this, {x:new_x, y:new_y, time:0.25, transition:'linear'});
		}
		
		private function tumble():void
		{
			Tweener.addTween(this, {rotation:360, time:1, transition:'linear', onComplete:this.onTumbleComplete});
		}
		
		private function onTumbleComplete():void
		{
			this.rotation=0;
		}
	}
}