package finalslide
{
	import finalslide.frame1.Frame1;
	import finalslide.frame2.Frame2;
	
	import flash.events.Event;
	
	import ru.flexdev.ui.SpriteEx;
	
	public class FinalSlide extends SpriteEx
	{
		public static const WIDTH:int=871;
		public static const HEIGHT:int=542;
		
		private var holder:SpriteEx;
		private var frames:Vector.<Frame>;
		private var currentFrame:Frame;
		
		private var counter:int=-1;
		
		public function FinalSlide()
		{
			super();
			
			this.counter=-1;
			
			this.holder=new SpriteEx();
			this.holder.width=WIDTH;
			this.holder.height=HEIGHT;
			this.addChild(this.holder);
			//this.holder.clipped=true;
			
			this.frames=new Vector.<Frame>();
			
			var frame:Frame;
			
			frame=new Frame1(WIDTH, HEIGHT);
			frame.bottom=0;
			this.frames.push(frame);
			
			frame=new Frame2(WIDTH, HEIGHT);
			frame.bottom=0;
			this.frames.push(frame);
		
			
			this.nextFrame();
			this.addEventListener('frame_complete', this.onFrameComplete);
		}
		
		private function nextFrame():void
		{
			this.counter++;
			if(this.counter>=this.frames.length)
			{
				this.gameComplete();
				return;
			}
			if(this.currentFrame!=null)this.currentFrame.hide();
			
			this.currentFrame=this.frames[this.counter];
			this.holder.addChild(this.currentFrame);
			this.currentFrame.show(this.counter==0);
		}
		
		private function onFrameComplete(event:Event):void
		{
			this.nextFrame();
		}
		
		private function gameComplete():void
		{
			this.dispatchEvent(new Event(Event.COMPLETE));
		}
		
		public function center(parentWidht:int, parentHeight:int):void
		{
			this.x=(parentWidht-WIDTH)/2;
			this.y=(parentHeight-HEIGHT)/2;
		}
		
		private function onTestComplete(event:Event):void
		{
			this.dispatchEvent(new Event(Event.COMPLETE));
		}
	}
}