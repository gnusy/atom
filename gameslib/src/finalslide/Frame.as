package finalslide
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import caurina.transitions.Tweener;
	
	import ru.flexdev.ui.SpriteEx;
	
	public class Frame extends SpriteEx
	{
		public function Frame(width:int, height:int)
		{
			super();
			this.width=width;
			this.height=height;
			this.visible=false;
			this.alpha=0;
			//this.x=this.width;
			
			
			
			//this.addEventListener(MouseEvent.CLICK, this.onMouseClick);
		}
		
		public function show(now:Boolean=false):void
		{
			this.visible=true;
			if(now)
			{
				this.alpha=1;
				//this.x=0;
			}
			//else Tweener.addTween(this, {alpha:1, x:0, transition:'linear', time:0.5});
			else Tweener.addTween(this, {alpha:1, transition:'linear', time:0.5});
		}
		
		public function hide():void
		{
			//Tweener.addTween(this, {alpha:0, x:-this.width, time:0.5, transition:'linear', onComplete:this.onHideComplete});
			Tweener.addTween(this, {alpha:0, time:0.5, transition:'linear', onComplete:this.onHideComplete});
		}
		
		private function onHideComplete():void
		{
			this.visible=false;
			this.parent.removeChild(this);
		}
		
		protected function complete():void
		{
			this.dispatchEvent(new Event('frame_complete', true));
		}
		
		private function onMouseClick(event:MouseEvent):void
		{
			if(event.ctrlKey)this.complete();
		}
	}
}