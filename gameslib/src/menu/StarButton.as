package menu
{
	import common.Common;
	import common.ToolTip;
	
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import fontmanager.FontManager;
	
	public class StarButton extends Sprite
	{
		[Embed(source="assets/star_enabled.png")]private var star_enabled:Class;
		[Embed(source="assets/star_disabled.png")]private var star_disabled:Class;

		private var tf:TextField;
		private var image:Bitmap;
		
		private var toolTip:ToolTip;
		private var passed:Boolean=false;
		private var tip:String;
		private var type:String;
		
		public function StarButton(text:String, tip:String, passed:Boolean, type:String)
		{
			super();
			this.passed=passed;
			this.tip=tip;
			this.type=type;
			
			var star_sprite:Sprite=new Sprite();
			star_sprite.buttonMode=true;
			this.addChild(star_sprite);
			
			if(passed)this.image=new this.star_enabled() as Bitmap;
			else this.image=new this.star_disabled() as Bitmap;
			this.image.smoothing=true;
			star_sprite.addChild(this.image);
			
			var frmt:TextFormat=FontManager.instance.textFormat(21, Common.GRAY, 'regular');
			frmt.align='center';
			
			this.tf=FontManager.instance.textField;
			this.tf.defaultTextFormat=frmt;
			this.tf.wordWrap=true;
			this.tf.text=text;
			this.tf.width=230;
			this.tf.x=(this.image.width-this.tf.width)/2;
			this.tf.y=157;
			this.addChild(this.tf);
			
			this.toolTip=new ToolTip();
			this.addChild(this.toolTip);
			
			star_sprite.addEventListener(MouseEvent.ROLL_OVER, onMouseEvent);
			star_sprite.addEventListener(MouseEvent.ROLL_OUT, onMouseEvent);
			star_sprite.addEventListener(MouseEvent.CLICK, onMouseEvent);
			
		}
		
		private function onMouseEvent(event:MouseEvent):void
		{
			switch(event.type)
			{
				case MouseEvent.ROLL_OVER:
					
					this.toolTip.show(this.tip, new Point(this.image.width/2, this.image.height/2));
					
					break;
				
				case MouseEvent.ROLL_OUT:
					this.toolTip.hide();
					break;
				
				case MouseEvent.CLICK:
					this.dispatchEvent(new Event('game_select', true));
					break;
			}
		}
		
		public function get data():String
		{
			return this.type;
		}

	}
}