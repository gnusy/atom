package menu
{
	import common.Background;
	import common.Common;
	
	import flash.display.Sprite;
	
	public class Menu extends Sprite
	{
		
		
		public static const WIDTH:int=871;
		public static const HEIGHT:int=528;
		private var background:Background;
		private var buttonsHolder:Sprite;
		
		private var buttonsData:Array=[
										{text:'Направления бизнеса\nпредприятий', tip:'Игра о направлениях бизнеса предприятий'},
										{text:'Продукция\nмашиностроительного дивизиона', tip:'Игра на знание продукции машиностроительного дивизиона'},
										{text:'Викторина', tip:'Игра на закрепление знаний'},
									];
		
		public function Menu()
		{
			super();this.background=new Background(WIDTH, HEIGHT);
			this.addChild(this.background);
			this.background.setTitles('Выполните задания, чтобы проверить свои знания и узнать\nинформацию по предприятиям машиностроительного дивизиона');
			background.hideButton('toMenu').hideButton('toMap');
			this.buttonsHolder=new Sprite();
			this.addChild(this.buttonsHolder);
			this.passed={game1:false, game2:false, game3:false};
		}
		
		public function center(parentWidht:int, parentHeight:int):void
		{
			this.x=(parentWidht-WIDTH)/2;
			this.y=(parentHeight-HEIGHT)/2;
		}
		
		public function set passed(games:Object):void
		{
			//this.buttonsHolder.removeChildren();
			Common.removeChildren(this.buttonsHolder);
			for(var i:int=0;i<this.buttonsData.length;i++)
			{
				var game_name:String='game'+(i+1).toString();
				var star:StarButton=new StarButton(this.buttonsData[i].text, this.buttonsData[i].tip, games[game_name], game_name);
				star.x=107+272*i;
				star.y=187;
				this.buttonsHolder.addChild(star);
			}
			
		}
	}
}