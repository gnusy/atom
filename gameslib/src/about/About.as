package about
{
	import common.Background;
	import common.Common;
	import common.Helper;
	
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.DataEvent;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.utils.setTimeout;
	
	import fontmanager.FontManager;
	
	import ru.flexdev.ui.Image;
	
	public class About extends Sprite
	{
		[Embed(source="assets/logo.png")]private var logo:Class;
		
		public static const WIDTH:int=871;
		public static const HEIGHT:int=425;
		
		private var background:Background;
		
		public function About()
		{
			super();
			this.background=new Background(WIDTH, HEIGHT);
			this.addChild(this.background);
			this.background.setTitles('О курсе', '').hideButton('toMenu').hideButton('toMap').topTitle('title1', 52);
			
			var frmt:TextFormat=FontManager.instance.textFormat(20, Common.GRAY, 'regular');
			var text:TextField=FontManager.instance.textField;
			text.mouseEnabled=false;
			text.defaultTextFormat=frmt;
			text.width=770;
			text.wordWrap=true;
			text.x=50;
			text.y=100;
			text.text='Курс рассказывает о стратегии развития, деятельности и о достижениях маши-\nностроительного дивизиона ГК «Росатом».';
			text.height=text.textHeight+4;
			this.addChild(text);
			
			text=FontManager.instance.textField;
			text.mouseEnabled=false;
			text.defaultTextFormat=frmt;
			text.width=770;
			text.wordWrap=true;
			text.x=50;
			text.y=158;
			text.text='В результате прохождения курса обучаемый понимает причины эффективности\nи результативности  работы предприятий и сотрудников дивизиона.';
			text.height=text.textHeight+4;
			this.addChild(text);
			
			
			frmt=FontManager.instance.textFormat(15, Common.GRAY, 'light');
			text=FontManager.instance.textField;
			text.mouseEnabled=false;
			text.defaultTextFormat=frmt;
			text.width=770;
			text.wordWrap=true;
			text.x=50;
			text.y=283;
			text.text='Реализация проекта: ООО «ИнтЭдука» (LEVEL) при консультационной поддержке экспертов Департамента\nуправления персоналом АО «Атомэнергомаш».';
			text.height=text.textHeight+4;
			this.addChild(text);
			
			frmt=FontManager.instance.textFormat(15, Common.GRAY, 'medium');
			text.setTextFormat(frmt, 0, 19);
			text.height=text.textHeight+4;
			
			frmt=FontManager.instance.textFormat(15, Common.GRAY, 'light');
			text=FontManager.instance.textField;
			text.mouseEnabled=false;
			text.defaultTextFormat=frmt;
			text.width=770;
			text.wordWrap=true;
			text.x=50;
			text.y=328;
			text.text='Техническая реализация: ООО «ИнтЭдука» (LEVEL).';
			text.height=text.textHeight+4;
			this.addChild(text);
			
			frmt=FontManager.instance.textFormat(15, Common.GRAY, 'medium');
			text.setTextFormat(frmt, 0, 23);
			
			frmt=FontManager.instance.textFormat(15, Common.GRAY, 'light');
			text=FontManager.instance.textField;
			text.mouseEnabled=false;
			text.defaultTextFormat=frmt;
			text.width=770;
			text.wordWrap=true;
			text.x=50;
			text.y=355;
			text.text='Правообладатель: АО «Атомэнергомаш».';
			text.height=text.textHeight+4;
			this.addChild(text);
			
			frmt=FontManager.instance.textFormat(15, Common.GRAY, 'medium');
			text.setTextFormat(frmt, 0, 16);
			
			
			var bmp:Bitmap=new logo() as Bitmap;
			bmp.smoothing=true;
			var image:Image=new Image();
			image.source=bmp.bitmapData;
			this.addChild(image);
			image.right=42;
			image.bottom=52;
				
			image.buttonMode=true;
			image.addEventListener(MouseEvent.CLICK,this.onLogoClick);
			
		}
	
		private function onLogoClick(event:MouseEvent):void
		{
			navigateToURL(new URLRequest('http://www.levellab.ru/'), '_blank');
		}
		
		public function center(parentWidht:int, parentHeight:int):void
		{
			this.x=(parentWidht-WIDTH)/2;
			this.y=(parentHeight-HEIGHT)/2;
		}
		
	}
}