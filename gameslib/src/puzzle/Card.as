package puzzle
{
	import caurina.transitions.Tweener;
	
	import flash.display.Bitmap;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import ru.flexdev.events.EventEx;
	import ru.flexdev.ui.Rect;
	import ru.flexdev.ui.SpriteEx;
	
	public class Card extends SpriteEx
	{
		public var data:Object;
		private var dragged:Boolean=false;
		private var _fixed:Boolean=false;
		private var delta:int=75;
		
		public function Card(data:Object)
		{
			super();
			
			this.data=data;
			var img:Bitmap=new data['image']() as Bitmap;
			img.smoothing=true;
			this.addChild(img);
			
			this.width=img.width;
			this.height=img.height;
			
			
			this.x=data['init_position'].x;
			this.y=data['init_position'].y;
			
			this.scaleX=this.scaleY=1/2.5;
			
			//this.x=data['target_position'].x;
			//this.y=data['target_position'].y;
			
			this.buttonMode=true;
			
			this.addEventListener(MouseEvent.MOUSE_DOWN, this.onMouseEvent);
		}
		
		public function get fixed():Boolean
		{
			return this._fixed;
		}

		public function fix(position:Point):void
		{
			this._fixed = true;
			this.buttonMode=false;
			Tweener.addTween(this, {x:position.x, y:position.y, time:0.25 });
		}

		public function get rect():Rectangle
		{
			return new Rectangle(this.x, this.y, this.width, this.height);
		}
		
		private function onMouseEvent(event:MouseEvent):void
		{
			var parentPotition:Point=this.parent.globalToLocal(new Point(event.stageX, event.stageY));
			if(this._fixed==true)return;
			switch(event.type)
			{
				case MouseEvent.MOUSE_DOWN:
					this.parent.setChildIndex(this, this.parent.numChildren-1);
					//this.scaleX=this.scaleY=1;
					Tweener.addTween(this, {scaleX:1, scaleY:1, time:0.1 });
					this.x=parentPotition.x-this.width/2;
					this.y=parentPotition.y-this.height/2;
					this.startDrag(false);
					this.dragged=true;
					
					this.addEventListener(MouseEvent.MOUSE_UP, this.onMouseEvent);
					this.addEventListener(MouseEvent.MOUSE_OUT, this.onMouseEvent);
					this.addEventListener(MouseEvent.MOUSE_MOVE, this.onMouseEvent);
					break;
				
				case MouseEvent.MOUSE_UP:
			//	case MouseEvent.MOUSE_OUT:
					this.stopDrag();
					this.dragged=false;
					this.dispatchEvent(new EventEx('item_drag_end', this, true));
					this.removeEventListener(MouseEvent.MOUSE_UP, this.onMouseEvent);
					this.removeEventListener(MouseEvent.MOUSE_OUT, this.onMouseEvent);
					this.removeEventListener(MouseEvent.MOUSE_MOVE, this.onMouseEvent);
					this.check();
					
					break;
				
				case MouseEvent.MOUSE_MOVE:
					if(this.dragged==false)return;
					//this.dispatchEvent(new EventEx('item_dragged', this, true));
					break;
			}
		}
		
		private function check():void
		{
			if(Math.abs(this.x-this.data['target_position'].x)<=this.delta && Math.abs(this.y-this.data['target_position'].y)<=this.delta)this.fix(this.data['target_position']);
			else this.returnToInitPosition();
		}
		
		private function returnToInitPosition():void
		{
			if(this.fixed==true)return;
			Tweener.addTween(this, {x:this.data['init_position'].x, y:this.data['init_position'].y, scaleX:1/2.5, scaleY:1/2.5, time:0.25 });
		}
		
		public function reset():void
		{
			this.returnToInitPosition();
			this._fixed=false;
			this.buttonMode=true;
		}
	}
}