package puzzle
{
	import common.Helper;
	
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.text.TextField;
	
	import fontmanager.FontManager;
	
	import ru.flexdev.ui.Button;
	import ru.flexdev.ui.Image;
	import ru.flexdev.ui.Rect;
	
	public class Puzzle extends Sprite
	{
		
		[Embed(source="assets/background.png")]private var backgroundClass:Class;
		
		[Embed(source="assets/item01.png")]private var item01Class:Class;
		[Embed(source="assets/item02.png")]private var item02Class:Class;
		[Embed(source="assets/item03.png")]private var item03Class:Class;
		[Embed(source="assets/item04.png")]private var item04Class:Class;
		[Embed(source="assets/item05.png")]private var item05Class:Class;
		[Embed(source="assets/item06.png")]private var item06Class:Class;
		[Embed(source="assets/item07.png")]private var item07Class:Class;
		[Embed(source="assets/item08.png")]private var item08Class:Class;
		[Embed(source="assets/item09.png")]private var item09Class:Class;
		[Embed(source="assets/item10.png")]private var item10Class:Class;
		[Embed(source="assets/item11.png")]private var item11Class:Class;
		[Embed(source="assets/item12.png")]private var item12Class:Class;
		
	
		public static const WIDTH:int=975;
		public static const HEIGHT:int=630;
		private var background:Rect;
		
		
		private var cardsData:Array=[
			{target_position:new Point(44, 81), init_position:new Point(851, 302), image:item01Class},
			{target_position:new Point(151, 81), init_position:new Point(739, 393), image:item02Class},
			{target_position:new Point(313, 81), init_position:new Point(840, 472), image:item03Class},
			{target_position:new Point(478, 81), init_position:new Point(870, 555), image:item04Class},
			
			{target_position:new Point(44, 228), init_position:new Point(848, 190), image:item05Class},
			{target_position:new Point(150, 228), init_position:new Point(728, 473), image:item06Class},
			{target_position:new Point(372, 171), init_position:new Point(754, 170), image:item07Class},
			{target_position:new Point(478, 171), init_position:new Point(731, 75), image:item08Class},
			
			{target_position:new Point(44, 374), init_position:new Point(849, 98), image:item09Class},
			{target_position:new Point(207, 316), init_position:new Point(753, 530), image:item10Class},
			{target_position:new Point(314, 374), init_position:new Point(830, 392), image:item11Class},
			{target_position:new Point(534, 316), init_position:new Point(751, 274), image:item12Class}
		];
		
		
		private var cards:Vector.<Card>;
		
		
		private var helper:Helper;
		private var btnNewGame:Button;
		private var btnExit:Button;
		
		public function Puzzle()
		{
			super();
			this.background=new Rect();
			this.background.width=WIDTH;
			this.background.height=HEIGHT;
			this.background.fill(0xffffff);
			this.addChild(this.background);
			
			var tf:TextField=FontManager.instance.textField;
			tf.defaultTextFormat=FontManager.instance.textFormat(30, 0x000000, 'bold');
			tf.width=250;
			tf.text='Атомные пазлы';
			tf.x=39;
			tf.y=22;
			this.addChild(tf);
			
			this.btnNewGame=new Button();
			this.background.addChild(this.btnNewGame);
			this.btnNewGame.left=43;
			this.btnNewGame.bottom=26;
			this.btnNewGame.label='Начать заново';
			this.btnNewGame.width=164;
			this.btnNewGame.height=40;
			this.btnNewGame.skinClass=puzzle.ButtonSkin;
			this.btnNewGame.addEventListener(MouseEvent.CLICK, this.onNewGameClick);
			
			this.btnExit=new Button();
			this.background.addChild(this.btnExit);
			this.btnExit.left=237;
			this.btnExit.bottom=26;
			this.btnExit.label='Завершить игру';
			this.btnExit.width=164;
			this.btnExit.height=40;
			this.btnExit.skinClass=puzzle.ButtonSkin;
			this.btnExit.addEventListener(MouseEvent.CLICK, this.onExitClick);
			
			
			var img:Image=new Image();
			img.source=(new backgroundClass() as Bitmap).bitmapData;
			img.top=80;
			img.left=42;
			this.background.addChild(img);
			
			
					
			
			this.cards=new Vector.<Card>();
			for(var i:int=0;i<this.cardsData.length;i++)
			{
				var card:Card=new Card(this.cardsData[i]);
				this.cards.push(card);
				this.background.addChild(card);
			}
			
			
			this.helper=new Helper('Хорошо!', '100%-й результат машиностроительного дивизиона складывается из нескольких элементов. В рамках курса Вы познакомитесь с ключевыми составляющими успеха дивизиона.');
			this.helper.bounds(0, 0);
			this.helper.width=WIDTH;
			this.helper.height=HEIGHT;
			this.helper.windowSize=new Point(390, 240);
			this.addChild(this.helper);
			this.helper.addEventListener(Event.CLOSE, this.onHelperClose);
			
			
		}
		
		private function onNewGameClick(event:MouseEvent):void
		{
			for(var i:int=0;i<this.cards.length;i++)
			{
				this.cards[i].reset();
			}
			
		}
		
		private function onExitClick(event:MouseEvent):void
		{
			this.dispatchEvent(new Event(Event.COMPLETE));
		}
		
		private function onHelperClose(event:Event):void
		{
			this.helper.hide();
			this.dispatchEvent(new Event('game_close'));
		}
		
		public function center(parentWidht:int, parentHeight:int):void
		{
			this.x=(parentWidht-WIDTH)/2;
			this.y=(parentHeight-HEIGHT)-22;
		}
		
		public function show(index:int):void
		{
			
		}
	}
}