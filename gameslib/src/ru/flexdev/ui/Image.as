package ru.flexdev.ui
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;

	public class Image extends SpriteEx
	{
		private var content:Bitmap;
		
		public function Image()
		{
			super();
		}
		
		public function set source(value:BitmapData):void
		{
			if(value==null)return;
			
			if(this.content!=null)
			{
				this.removeChild(this.content);
				this.content.bitmapData.dispose();
				this.content=null;
			}
			
			this.content=new Bitmap(value, 'auto', true);
			if(super.width==-1)super.width=this.contentWidth;
			if(super.height==-1)super.height=this.contentHeight;
			
			this.addChild(this.content);
			this.updateSize();
		}
		
		override public function get contentWidth():Number
		{
			if(this.content!=null)return this.content.bitmapData.width; 			
			return 0;
		}
		
		override public function get contentHeight():Number
		{
			if(this.content!=null)return this.content.bitmapData.height;
			return 0;
		}
		
		public function updateSize():void
		{
			if(this.content==null)return;
			
			var k:Number=Math.max(this.contentWidth/super.width, this.contentHeight/super.height);
			this.content.width=this.contentWidth/k;
			this.content.height=this.contentHeight/k;
			this.content.x=(super.width-this.content.width)/2;
			this.content.y=(super.height-this.content.height)/2;
		}
		
		override protected function resize():void
		{
			super.resize();
			this.updateSize();
		}
		
		override public function dispose():void
		{
			if(this.content!=null)
			{
				this.content.bitmapData.dispose();
				this.content=null;
			}
		}
	}
}
