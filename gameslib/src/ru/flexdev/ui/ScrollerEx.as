﻿package ru.flexdev.ui
{
	import common.Common;
	
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	
	public class ScrollerEx extends SpriteEx
	{
		[Embed(source="assets/thumb_up.png")]
		private var thumb_up_class:Class;
		[Embed(source="assets/thumb_over.png")]
		private var thumb_over_class:Class;
		
		private var holder:SpriteEx;
		private var holderMask:SpriteEx;
		
		private var scroller:SpriteEx;
		private var thumb:SpriteEx;
		private var thumb_image:Bitmap;
		
		private var thumbPressed:Boolean=false;
		private var thumbPosition:int;
		
		private var positionPercent:Number;
		
		
		private var thumb_width:int=15;
		private var thumb_height:int=94;
		
		public function ScrollerEx()
		{
			this.createChildren();
		}
		
		public function reset():void
		{
			this.thumb.y=0;
			this.updateHolderPosition();
		}
		
		public function get holderSize():Rectangle
		{
			this.updateHolderWidth();
			return new Rectangle(0, 0, this.holder.width, this.holder.height);
		}
		
		public function scrollTo(value:int):void
		{
			this.holder.y=-value;
			var dy:int=this.holderHeight-this.height;
			this.thumb.y=-this.holder.y*(this.height-thumb_height)/dy;
			this.updateHolderPosition();
		}
		
		private function createChildren():void
		{
			this.holder=new SpriteEx();
			//this.holder.width=this.width-6;
			super.addChild(this.holder);
			this.holder.externalMask=true;
			
			this.holderMask=new SpriteEx();
			super.addChild(this.holderMask);
			
			this.holderMask.graphics.beginFill(0x000000);
			this.holderMask.graphics.drawRect(0, 0, this.width-this.thumb_width, this.height+1);
			this.holderMask.graphics.endFill();
			
			this.holderMask.width=this.width-this.thumb_width;
			this.holderMask.height=this.height+1;
			
			this.holder.mask=this.holderMask;
			
			
			this.holder.addEventListener(Event.EXIT_FRAME, this.update);
			
			this.scroller=new SpriteEx();
			this.scroller.visible=true;
			this.scroller.x=this.width-this.thumb_width;
			this.scroller.y=0;
			
			super.addChild(this.scroller);
			
			this.scroller.graphics.beginFill(0xe2e2e2, 1);
			this.scroller.graphics.drawRoundRect(4, 0, 7, this.height, 4, 4);
			this.scroller.graphics.endFill();
			
			this.thumb=new SpriteEx();
			this.scroller.addChild(this.thumb);
			this.thumb_image=new this.thumb_up_class() as Bitmap;
			this.thumb.addChild(this.thumb_image);
			
			
			this.thumb.addEventListener(MouseEvent.MOUSE_OVER, this.onThumbMouseEvent);
			this.thumb.addEventListener(MouseEvent.MOUSE_OUT, this.onThumbMouseEvent);
			this.thumb.addEventListener(MouseEvent.MOUSE_DOWN, this.onThumbMouseEvent);
			
			this.updateHolderWidth();
		}
		
		override protected function addedToStage():void
		{
			this.stage.addEventListener(MouseEvent.MOUSE_WHEEL, this.onMouseEvent);
		}
		
		private function onMouseEvent(event:MouseEvent):void
		{
			switch(event.type)
			{
				case MouseEvent.MOUSE_OVER:
					this.stage.addEventListener(MouseEvent.MOUSE_WHEEL, this.onMouseEvent);
					break;
				
				case MouseEvent.MOUSE_OUT:
					this.stage.removeEventListener(MouseEvent.MOUSE_WHEEL, this.onMouseEvent);
					break;
				
				case MouseEvent.MOUSE_WHEEL:
					if(!this.hitTestPoint(event.stageX, event.stageY))return;
					if(!this.holderMask.hitTestPoint(event.stageX, event.stageY))return;
					
					
					var dy:Number=this.holderHeight/this.height;
					
					this.thumb.y=this.thumb.y-event.delta*10/dy;
					
					if(this.thumb.y<0)this.thumb.y=0;
					if(this.thumb.y>this.height-thumb_height)this.thumb.y=this.height-thumb_height;
					
					
					this.updateHolderPosition();
					
					break;
			}
		}
		
		private function onThumbMouseEvent(event:MouseEvent):void
		{
			switch(event.type)
			{
				case MouseEvent.MOUSE_OVER:
					//this.thumb.removeChildren();
					Common.removeChildren(this.thumb);
					this.thumb_image=new this.thumb_over_class() as Bitmap;
					this.thumb.addChild(this.thumb_image);
					break;
				
				case MouseEvent.MOUSE_OUT:
					//this.thumb.removeChildren();
					Common.removeChildren(this.thumb);
					this.thumb_image=new this.thumb_up_class() as Bitmap;
					this.thumb.addChild(this.thumb_image);
					break;
				
				case MouseEvent.MOUSE_DOWN:
					this.stage.addEventListener(MouseEvent.MOUSE_MOVE, this.onThumbMouseEvent);
					this.stage.addEventListener(MouseEvent.MOUSE_UP, this.onThumbMouseEvent);
					this.thumbPressed=true;
					this.thumbPosition=event.stageY-this.thumb.y;
					break;
				
				case MouseEvent.MOUSE_MOVE:
					if(!this.thumbPressed)return;
					this.thumb.y=event.stageY-this.thumbPosition;
					
					if(this.thumb.y<0)this.thumb.y=0;
					if(this.thumb.y>this.height-thumb_height)this.thumb.y=this.height-thumb_height;
					
					this.updateHolderPosition();
					break;
				
				case MouseEvent.MOUSE_UP:
					if(this.stage!=null)
					{
						this.stage.removeEventListener(MouseEvent.MOUSE_MOVE, this.onThumbMouseEvent);
						this.stage.removeEventListener(MouseEvent.MOUSE_UP, this.onThumbMouseEvent);
					}
					
					this.thumbPressed=false;
					break;
			}
		}
		
		private function updateHolderPosition():void
		{
			if(this.thumb.y>this.height-thumb_height)this.thumb.y=this.height-thumb_height;
			
			this.positionPercent=this.thumb.y/(this.height-thumb_height);
			
			if(this.positionPercent>1)this.positionPercent=1;
			
			var dy:int=this.holderHeight-this.height;
			
			if(dy<=0)
			{
				this.holder.y=0;
				return;
			}
			
			this.holder.y=-(this.positionPercent*dy);
			
		}
		
		override public function addChild(child:DisplayObject):DisplayObject
		{
			this.holder.addChild(child);
			this.updateHolderPosition();
			return child;
		}
		
		override public function removeChild(child:DisplayObject):DisplayObject
		{
			this.holder.removeChild(child);
			this.updateHolderPosition();
			return child;
		}
		
		public function clear(thumbReset:Boolean=true):void
		{
			//this.holder.removeChildren();
			Common.removeChildren(this.holder);
			if(!thumbReset)return;
			this.thumb.y=0;
			this.updateHolderPosition();			
		}
		
		private function get holderHeight():int
		{
			return this.holder.contentHeight;
		}
		
		private var prevHolderHeight:int;
		public function update(event:Event=null):void
		{
			if(prevHolderHeight==this.holderHeight)return;
			if(this.holderHeight>this.height)
			{
				this.scroller.visible=true;
			}
			else
			{
				this.scroller.visible=false;
				this.thumb.y=0;
			}
			this.updateHolderWidth();
			prevHolderHeight=this.holderHeight;
			
			this.updateHolderPosition();
		}
		
		override public function set height(value:Number):void
		{
			super.height=value;
			this.updateMask();
		}
		
		override public function set width(value:Number):void
		{
			super.width=value;
			//if(this.holder)this.holder.width=this.width-6;
			if(this.holder)this.updateHolderWidth();
			this.updateMask();
		}
		
		private function updateHolderWidth():void
		{
			if(this.scroller.visible==false)
			{
				this.holder.width=this.width;
			}
			else
			{
				this.holder.width=this.width-this.thumb_width;
			}
		}
		
		private function updateMask():void
		{
			if(this.holderMask==null)return;
			this.holderMask.graphics.clear();
			this.holderMask.graphics.beginFill(0xff0000, 0.75);
			this.holderMask.graphics.drawRect(0, 0, this.holder.width, this.height+1);
			this.holderMask.graphics.endFill();
			
			this.holderMask.width=this.holder.width;
			this.holderMask.height=this.height+1;
			
			if(this.scroller==null)return;
			
			
			this.scroller.graphics.clear();
			this.scroller.graphics.beginFill(0xe2e2e2, 1);
			this.scroller.graphics.drawRoundRect(4, 0, 7, this.height, 4, 4);
			this.scroller.graphics.endFill();
			
		}
		
		override protected function resize():void
		{
			this.scroller.x=this.width-this.thumb_width;
		}
		
	}
}