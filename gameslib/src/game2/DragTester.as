package game2
{
	import flash.geom.Rectangle;
	
	public class DragTester
	{
		private static var padding:int=15;
		/*
		private static var companies:Array=[
										new Rectangle(448-padding, 152-padding, padding*2, padding*2),
										new Rectangle(554-padding, 190-padding, padding*2, padding*2),
										new Rectangle(610-padding, 288-padding, padding*2, padding*2),
										new Rectangle(590-padding, 399-padding, padding*2, padding*2),
										new Rectangle(504-padding, 472-padding, padding*2, padding*2),
										new Rectangle(391-padding, 472-padding, padding*2, padding*2),
										new Rectangle(306-padding, 399-padding, padding*2, padding*2),
										new Rectangle(286-padding, 288-padding, padding*2, padding*2),
										new Rectangle(341-padding, 190-padding, padding*2, padding*2)
									];
		*/
		public static var companies:Array=[
			new Rectangle(347, 137, 195, 79),
			new Rectangle(563, 203, 149, 62),
			new Rectangle(613, 326, 255, 47),
			new Rectangle(588, 452, 187, 50),
			new Rectangle(466, 540, 229, 52),
			new Rectangle(161, 547, 280, 35),
			new Rectangle(154, 435, 150, 70),
			new Rectangle(112, 322, 171, 45),
			new Rectangle(187, 210, 156, 55)
		];
		

		public static function hasIntersection(restangle:Rectangle, company:int):Boolean
		{
			if(restangle.intersects(companies[company]))return true;
			return false;
		}
	}
}