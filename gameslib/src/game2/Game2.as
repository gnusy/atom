package game2
{
	import common.Background;
	import common.Common;
	import common.Helper;
	
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fontmanager.FontManager;
	
	public class Game2 extends Sprite
	{
		[Embed(source="assets/companies.png")] private var companies:Class;
		
		public static const WIDTH:int=932;
		public static const HEIGHT:int=645;
		
		private var background:Background;
		private var circle:Circle;
		private var itemsHolder:ItemsHolder;
		private var helper:Helper;
		
		
		public function Game2()
		{
			super();
			this.background=new Background(WIDTH, HEIGHT);
			this.addChild(this.background);
			this.background.setTitles('Ключевая продукция предприятий', 'Определите, какое предприятие производит данную продукцию\n(переместите продукцию к соответствующему логотипу)');
			this.background.setTitleFormat('title2', FontManager.instance.textFormat(17, Common.GRAY, 'light'));
			this.background.hideButton('toMenu').hideButton('toMap');
			
			var image:Bitmap=new this.companies() as Bitmap;
			image.smoothing=true;
			this.addChild(image);
			image.x=114;
			image.y=138;
			
			this.circle=new Circle();
			this.addChild(this.circle);
			this.circle.x=296;
			this.circle.y=234;
			
			this.itemsHolder=new ItemsHolder();
			this.addChild(this.itemsHolder);
			this.itemsHolder.addEventListener('update_circle', this.onUpdateCircle);
			this.itemsHolder.addEventListener('items_complete', this.onItemsComplete);
			
			
			this.helper=new Helper('Отлично!', 'Теперь Вы знаете ключевую продукцию наших предприятий! Они – наша гордость.');
			this.helper.bounds(0, 0);
			this.helper.width=WIDTH;
			this.helper.height=HEIGHT;
			this.addChild(this.helper);
			this.helper.addEventListener(Event.CLOSE, this.onHelperClose);
			
			this.addEventListener(MouseEvent.MOUSE_DOWN, this.onMouseClick);
		}
		
		private function onMouseClick(event:MouseEvent):void
		{
			if(event.altKey && event.ctrlKey)this.onHelperClose(null);
		}
		
		public function center(parentWidht:int, parentHeight:int):void
		{
			this.x=(parentWidht-WIDTH)/2;
			this.y=(parentHeight-HEIGHT)/2;
		}
		
		private function onItemsComplete(event:Event):void
		{
			this.helper.show();
		}
		
		private function onHelperClose(event:Event):void
		{
			this.helper.hide();
			this.dispatchEvent(new Event(Event.COMPLETE));
		}
		
		private function onUpdateCircle(event:Event):void
		{
			this.circle.drawSegment(event.target.data);
		}
	}
}

