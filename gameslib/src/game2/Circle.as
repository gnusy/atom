package game2
{
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	
	public class Circle extends Sprite
	{
		private var r_big:int=148;
		private var r_small:int=140;
		
		public function Circle()
		{
			super();
			this.drawInit();
			
		}
		
		private function drawInit():void
		{
			this.graphics.beginFill(0xe3e3e3);
			
			for(var i:int=0;i<360;i++)
			{
				var x1:Number=Math.sin(2*i*Math.PI/360)*r_big+r_big;
				var y1:Number=-Math.cos(2*i*Math.PI/360)*r_big+r_big;
				
				var x2:Number=Math.sin(2*(i+1)*Math.PI/360)*r_big+r_big;
				var y2:Number=-Math.cos(2*(i+1)*Math.PI/360)*r_big+r_big;
				
				var x3:Number=Math.sin(2*(i+1)*Math.PI/360)*r_small+r_big;
				var y3:Number=-Math.cos(2*(i+1)*Math.PI/360)*r_small+r_big;
				
				var x4:Number=Math.sin(2*i*Math.PI/360)*r_small+r_big;
				var y4:Number=-Math.cos(2*i*Math.PI/360)*r_small+r_big;
				
				
				this.graphics.moveTo(x1, y1);
				this.graphics.lineTo(x2, y2);
				this.graphics.lineTo(x3, y3);
				this.graphics.lineTo(x4, y4);
				this.graphics.lineTo(x1, y1);
				
			}
			this.graphics.endFill();

		}
		
		public function drawInit2():void
		{
			/*
			for(var i:int=0;i<360;i+=40)
			{
				var x1:Number=Math.sin(2*i*Math.PI/360)*165+138;
				var y1:Number=-Math.cos(2*i*Math.PI/360)*165+138;
				
			//	trace(int(x1)+310, int(y1)+179);
				this.graphics.beginFill(0xff0000);	
				this.graphics.drawRect(x1-10, y1-10, 20, 20);
				this.graphics.endFill();	
				
			}
			*/
			for(var i:int=0;i<9;i++)
			{
				var rect:Rectangle=DragTester.companies[i];
				
				this.graphics.lineStyle(0, 0);	
				this.graphics.drawRect(rect.x-this.x, rect.y-this.y, rect.width, rect.height);
			}
		}
		
		public function drawSegment(segment:Number):void
		{
			this.graphics.beginFill(0xf8ad3b);
			
			var begin_angle:int=40*segment-20;
			var end_angle:int=40*segment+20;
			
			
			for(var i:int=begin_angle;i<end_angle;i++)
			{
				var x1:Number=Math.sin(2*i*Math.PI/360)*r_big+r_big;
				var y1:Number=-Math.cos(2*i*Math.PI/360)*r_big+r_big;
				
				var x2:Number=Math.sin(2*(i+1)*Math.PI/360)*r_big+r_big;
				var y2:Number=-Math.cos(2*(i+1)*Math.PI/360)*r_big+r_big;
				
				var x3:Number=Math.sin(2*(i+1)*Math.PI/360)*r_small+r_big;
				var y3:Number=-Math.cos(2*(i+1)*Math.PI/360)*r_small+r_big;
				
				var x4:Number=Math.sin(2*i*Math.PI/360)*r_small+r_big;
				var y4:Number=-Math.cos(2*i*Math.PI/360)*r_small+r_big;
				
				
				this.graphics.moveTo(x1, y1);
				this.graphics.lineTo(x2, y2);
				this.graphics.lineTo(x3, y3);
				this.graphics.lineTo(x4, y4);
				this.graphics.lineTo(x1, y1);
				
			}
			this.graphics.endFill();
		}
	}
}