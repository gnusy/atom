package keyproducts
{
	import flash.events.Event;
	
	import ru.flexdev.events.EventEx;
	import ru.flexdev.ui.Rect;
	import ru.flexdev.ui.SpriteEx;
	
	public class SlideTest extends Rect
	{
		[Embed(source="assets/image0.png")]private var image0:Class;
		[Embed(source="assets/image1.png")]private var image1:Class;
		[Embed(source="assets/image2.png")]private var image2:Class;
		[Embed(source="assets/image3.png")]private var image3:Class;
		
		private var questions_data:Array=[
			{text:'Атомная энергетика\n(основная продукция)', position:0},
			{text:'Судостроение', position:1},
			{text:'Атомная энергетика\n(новые продукты)', position:2},
			{text:'Прочие новые\nбизнес-направления', position:3}		
		];
		
		private var answers_data:Array=[
			{text:'Комплектная поставка\nЯдерной Паропроизводя-\nщей установки для АЭС\nВВЭР (ЯППУ)', position:0, image:image0},
			{text:'Комплектная поставка\nсудового реакторного\nострова', position:1, image:image1},
			{text:'Комплектная поставка\nоборудования машзала\nили  турбоустановки АЭС', position:2, image:image2},
			{text:'Оборудование для тепло-\nвой энергетики, ГНХ,\nобщей техники и спец-\nсталей', position:3, image:image3},		
		];
		
		private var questions:Vector.<Question>;
		private var answers:Vector.<Answer>;
		
		private var answersHolder:SpriteEx;
		
		public function SlideTest()
		{
			super();
			this.width=460;
			this.height=290;
			
			
			while(this.check_data())this.answers_data.sort(this.shuffleAnswers);
			
			
			this.placeQuestions();
			this.placeAnswers();
			
			this.addEventListener('item_dragged', this.onItemDragged);
			this.addEventListener('item_drag_end', this.onItemDragEnd);
		}
		
		private function placeQuestions():void
		{
			this.questions=new Vector.<Question>();
			
			for(var i:int=0;i<this.questions_data.length;i++)
			{
				var question:Question=new Question(this.questions_data[i]);
				this.addChild(question);
				question.y=71*i;
				this.questions.push(question);
			}
		}
		
		private function placeAnswers():void
		{
			this.answersHolder=new SpriteEx();
			this.addChild(this.answersHolder);
			this.answersHolder.x=231;
			
			this.answers=new Vector.<Answer>();
			
			for(var i:int=0;i<this.answers_data.length;i++)
			{
				var answer:Answer=new Answer(this.answers_data[i]);
				this.answersHolder.addChild(answer);
				answer.y=71*i;
				this.answers.push(answer);
			}
		}
		
		private function shuffleAnswers(val1:Object, val2:Object):Number
		{
			return Math.random()<0.5?-1:1;
		}
		
		private function onItemDragged(event:EventEx):void
		{
			var dragged_item:Answer=event.data as Answer;
			this.answers.sort(this.sortAnswersByTop);
			
			for(var i:int=0;i<this.answers.length;i++)
			{
				if(dragged_item==this.answers[i])continue;
				
				this.answers[i].moveTo(71*i);
			}
		}
		
		private function onItemDragEnd(event:EventEx):void
		{
			var dragged_item:Answer=event.data as Answer;
			this.answers.sort(this.sortAnswersByTop);
			
			for(var i:int=0;i<this.answers.length;i++)
			{
				this.answers[i].moveTo(71*i);
			}
			
			if(this.check())this.dispatchEvent(new Event('test_complete'));
		}
		
		private function sortAnswersByTop(val1:Answer, val2:Answer):Number
		{
			if(val1.y>val2.y)return 1;
			else return -1;
		}
		
		private function check():Boolean
		{
			for(var i:int=0;i<this.answers.length;i++)
			{
				if(this.answers[i].position!=this.questions[i].position)return false;
			}
			return true;
		}
		
		private function check_data():Boolean
		{
			for(var i:int=0;i<this.answers_data.length;i++)
			{
				if(this.answers_data[i].position!=this.questions_data[i].position)return false;
			}
			return true;
		}
	}
}