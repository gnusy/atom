package keyproducts
{
	import common.Common;
	
	import flash.display.Bitmap;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import fontmanager.FontManager;
	
	import ru.flexdev.ui.Image;
	import ru.flexdev.ui.SpriteEx;
	
	public class Question extends SpriteEx
	{
		[Embed(source="assets/arrow.png")]private var arrow:Class;
		public var position:int=-1;
		
		
		public function Question(data:Object)
		{
			super();
			this.width=205;
			this.height=71;
			this.position=data['position'];
			
			var bmp:Bitmap=new arrow() as Bitmap;
			var image:Image=new Image();
			image.source=bmp.bitmapData;
			image.verticalCenter=0;
			image.right=0;
			this.addChild(image);
			
			
			var frmt:TextFormat=FontManager.instance.textFormat(12, Common.GRAY, 'regular');
			var text:TextField=FontManager.instance.textField;
			text.defaultTextFormat=frmt;
			text.width=150;
			text.wordWrap=true;
			text.x=-4;
			text.text=data['text'];
			text.height=text.textHeight+4;
			text.y=(this.height-text.height)/2;
			this.addChild(text);
		}
	}
}