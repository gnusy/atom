package keyproducts
{
	import caurina.transitions.Tweener;
	
	import common.Common;
	
	import flash.display.Bitmap;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import fontmanager.FontManager;
	
	import ru.flexdev.events.EventEx;
	import ru.flexdev.ui.Image;
	import ru.flexdev.ui.Rect;
	import ru.flexdev.ui.SpriteEx;
	
	public class Answer extends Rect
	{
		public var position:int=-1;
		private var dragged:Boolean=false;
		
		public function Answer(data:Object)
		{
			super();
			this.width=215;
			this.height=71;

			this.fill(0xffffff, 0.01);
			this.position=data['position'];
			this.buttonMode=true;
			
			var image_class:Class=data['image'];
			var bmp:Bitmap=new image_class() as Bitmap;
			var image:Image=new Image();
			image.mouseEnabled=false;
			image.source=bmp.bitmapData;
			image.verticalCenter=0;
			image.right=0;
			this.addChild(image);
			
			var frmt:TextFormat=FontManager.instance.textFormat(12, Common.GRAY, 'regular');
			var text:TextField=FontManager.instance.textField;
			text.mouseEnabled=false;
			text.defaultTextFormat=frmt;
			text.width=160;
			text.wordWrap=true;
			text.x=-4;
			text.y=15;
			text.text=data['text'];
			text.height=text.textHeight+4;
			this.addChild(text);
			
			this.addEventListener(MouseEvent.MOUSE_DOWN, this.onMouseEvent);
		}
		
		private function onMouseEvent(event:MouseEvent):void
		{
			switch(event.type)
			{
				case MouseEvent.MOUSE_DOWN:
					this.parent.setChildIndex(this, this.parent.numChildren-1);
					this.startDrag(false, new Rectangle(0, -20, 0, 290-25));
					this.dragged=true;
					
					this.addEventListener(MouseEvent.MOUSE_UP, this.onMouseEvent);
					this.addEventListener(MouseEvent.MOUSE_OUT, this.onMouseEvent);
					this.addEventListener(MouseEvent.MOUSE_MOVE, this.onMouseEvent);
					break;
				
				case MouseEvent.MOUSE_UP:
				case MouseEvent.MOUSE_OUT:
					this.stopDrag();
					this.dragged=false;
					this.dispatchEvent(new EventEx('item_drag_end', this, true));
					this.removeEventListener(MouseEvent.MOUSE_UP, this.onMouseEvent);
					this.removeEventListener(MouseEvent.MOUSE_OUT, this.onMouseEvent);
					this.removeEventListener(MouseEvent.MOUSE_MOVE, this.onMouseEvent);
					break;
				
				case MouseEvent.MOUSE_MOVE:
					if(this.dragged==false)return;
					this.dispatchEvent(new EventEx('item_dragged', this, true));
					break;
			}
		}
		
		public function moveTo(y:int):void
		{
			Tweener.addTween(this, {y:y, time:0.25})
		}
	}
}