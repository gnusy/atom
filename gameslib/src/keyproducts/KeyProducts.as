package keyproducts
{
	import caurina.transitions.Tweener;
	
	import common.Background;
	import common.Common;
	import common.Helper;
	
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import fontmanager.FontManager;
	
	import ru.flexdev.ui.Rect;
	
	public class KeyProducts extends Sprite
	{
		[Embed(source="assets/hint.png")]private var hintClass:Class;
		
		public static const WIDTH:int=543;
		public static const HEIGHT:int=542;
		
		private var background:Background;
		private var test:SlideTest;
		private var helper:Helper;
		private var hint:Bitmap;
		
		
		public function KeyProducts()
		{
			super();
			this.background=new Background(WIDTH, HEIGHT);
			this.addChild(this.background);
			this.background.setTitles('', '').hideButton('toMenu').hideButton('toMap');
	
			
			
			
			var frmt:TextFormat=FontManager.instance.textFormat(20, Common.GRAY, 'regular');
			var text:TextField=FontManager.instance.textField;
			text.defaultTextFormat=frmt;
			text.width=440;
			text.wordWrap=true;
			text.x=50;
			text.y=46;
			text.text='Найдите правильное соответствие между ключевым продуктами и соответствующими направлениями в структуре портфеля дивизиона';
			this.addChild(text);
			
			text=FontManager.instance.textField;
			text.defaultTextFormat=frmt;
			text.width=140;
			text.wordWrap=true;
			text.x=590;
			text.y=46;
			text.text='«Что поможет реализовать стратегию?»';
			//this.addChild(text);
			
			frmt=FontManager.instance.textFormat(16, Common.ORANGE, 'light');
			text=FontManager.instance.textField;
			text.defaultTextFormat=frmt;
			text.width=200;
			text.wordWrap=true;
			text.x=590;
			text.y=134;
			text.text='Ключевые продукты\nв портфеле заказов\nДивизиона';
			//this.addChild(text);
			
			frmt=FontManager.instance.textFormat(16, 0xafafaf, 'light');
			text=FontManager.instance.textField;
			text.defaultTextFormat=frmt;
			text.width=200;
			text.wordWrap=true;
			text.x=590;
			text.y=225;
			text.text='Развитие новых бизнес-\nнаправлений\nв Машиностроительном\nДивизионе ГК «Росатом»';
			//this.addChild(text);
			
			frmt=FontManager.instance.textFormat(16, 0xafafaf, 'light');
			text=FontManager.instance.textField;
			text.defaultTextFormat=frmt;
			text.width=200;
			text.wordWrap=true;
			text.x=590;
			text.y=335;
			text.text='Развитие проектов ПСР';
			//this.addChild(text);
			
			frmt=FontManager.instance.textFormat(12, Common.GRAY, 'black');
			text=FontManager.instance.textField;
			text.defaultTextFormat=frmt;
			text.width=200;
			text.wordWrap=true;
			text.x=50;
			text.y=182;
			text.text='Структура портфеля заказов\nдивизиона по направлениям';
			this.addChild(text);
			
			frmt=FontManager.instance.textFormat(12, Common.GRAY, 'black');
			text=FontManager.instance.textField;
			text.defaultTextFormat=frmt;
			text.width=200;
			text.wordWrap=true;
			text.x=279;
			text.y=182;
			text.text='Ключевые продукты\nАтомэнергомаш';
			this.addChild(text);
			
			var rect:Rect=new Rect();
			rect.fill(0xafafaf);
			rect.x=52;
			rect.y=167;
			rect.width=204;
			rect.height=3;
			this.addChild(rect);
			
			rect=new Rect();
			rect.fill(0xafafaf);
			rect.x=283;
			rect.y=167;
			rect.width=204;
			rect.height=3;
			this.addChild(rect);
			
			rect=new Rect();
			rect.fill(0xafafaf);
			rect.x=52;
			rect.y=227;
			rect.width=204;
			rect.height=1;
			this.addChild(rect);
			
			rect=new Rect();
			rect.fill(0xafafaf);
			rect.x=283;
			rect.y=227;
			rect.width=204;
			rect.height=1;
			this.addChild(rect);
			
			
			this.test=new SlideTest();
			this.test.bounds(52, 228);
			this.test.addEventListener('test_complete', this.onTestComplete);
			this.addChild(this.test);
			
			this.helper=new Helper('Хорошо!', 'Вы правильно определили ключевые продукты Машиностроительного дивизиона.');
			this.helper.bounds(0, 0);
			this.helper.width=WIDTH;
			this.helper.height=HEIGHT;
			this.addChild(this.helper);
			this.helper.addEventListener(Event.CLOSE, this.onHelperClose);
			
			
			this.hint=new hintClass() as Bitmap;
			this.addChild(this.hint);
			this.hint.x=517;
			this.hint.y=224;
			
			this.addEventListener(MouseEvent.MOUSE_DOWN, this.onMouseClick);
		}
		
		private function onMouseClick(event:MouseEvent):void
		{
			this.removeEventListener(MouseEvent.MOUSE_DOWN, this.onMouseClick);
			Tweener.addTween(this.hint, {alpha:0, time:0.5, transition:'linear', onComplete:this.onHintHide});
		}
		
		private function onHintHide():void
		{
			this.hint.visible=false;
		}
		
		private function onHelperClose(event:Event):void
		{
			this.helper.hide();
			this.dispatchEvent(new Event(Event.COMPLETE));
		}
		
		public function center(parentWidht:int, parentHeight:int):void
		{
			this.x=(parentWidht-WIDTH)/2;
			this.y=(parentHeight-HEIGHT)/2;
		}
		
		private function onTestComplete(event:Event):void
		{
			this.helper.show();
		}
	}
}
