package common
{
	import caurina.transitions.Tweener;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.filters.GlowFilter;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import fontmanager.FontManager;
	
	import ru.flexdev.ui.Button;
	import ru.flexdev.ui.Rect;
	
	public class Helper extends Rect
	{
		private var title:TextField;
		private var text:TextField;
		private var button:Button;
		private var window:Rect;
		
		public function Helper(title:String, text:String, dark:Boolean=false)
		{
			super();
			if(dark==false)this.fill(0xffffff, 0.01);
			else this.fill(0x000000, 0.2);
			
			this.visible=false;
			this.alpha=0;
			
			this.window=new Rect();
			this.window.width=387;
			this.window.height=218;
			this.window.verticalCenter=0;
			this.window.horizontalCenter=0;
			this.window.fill(0xffffff).round(12, 12);
			this.addChild(this.window);
			
			
			var glow:GlowFilter=new GlowFilter(0xbababa, 0.25, 8, 8);
			var shadow:DropShadowFilter=new DropShadowFilter(8, 90, 0x000000, 0.5, 4, 8);
			this.window.filters=[glow, shadow];
			
			
			this.title=FontManager.instance.textField;
			var frmt:TextFormat=FontManager.instance.textFormat(20, Common.GRAY, 'regular');
			this.title.defaultTextFormat=frmt;
			this.title.y=33;
			this.title.text=title;
			this.title.width=this.title.textWidth+5;
			this.title.height=this.title.textHeight+5;
			this.title.x=(this.window.width-this.title.width)/2;
			this.window.addChild(this.title);
			
			this.text=FontManager.instance.textField;
			frmt=FontManager.instance.textFormat(17, Common.GRAY, 'light');
			frmt.align='center';
			this.text.defaultTextFormat=frmt;
			this.text.width=this.window.width-70;
			this.text.wordWrap=true;
			this.text.y=76;
			if(title=='')this.text.y=55;
			this.text.text=text;
			this.text.width=this.text.textWidth+5;
			this.text.height=this.text.textHeight+5;
			this.text.x=(this.window.width-this.text.width)/2;
			this.window.addChild(this.text);
			
			this.button=new Button();
			this.button.horizontalCenter=0;
			this.button.bottom=24;
			this.button.width=100;
			this.button.height=28;
			this.button.label='Далее';
			this.button.skinClass=common.ButtonSkin;
			this.window.addChild(this.button);
			this.button.addEventListener(MouseEvent.CLICK, this.onClick);
		}
		
		public function set buttonLabel(value:String):void
		{
			this.button.label=value;
		}
		
		public function set buttonWidth(value:int):void
		{
			this.button.width=value;
		}
		
		public function set windowSize(value:Point):void
		{
			this.window.width=value.x;
			this.window.height=value.y;
		}
		
		public function show():void
		{
			this.parent.setChildIndex(this, this.parent.numChildren-1);
			this.visible=true;
			Tweener.addTween(this, {alpha:1, time:0.25, transition:'linear'});
		}
		
		public function hide(now:Boolean=false):void
		{
			if(now)
			{
				this.alpha=0;
				this.visible=false;
			}
			else Tweener.addTween(this, {alpha:0, time:0.25, transition:'linear', onComplete:this.onHideComplete});
		}
		
		private function onHideComplete():void
		{
			this.visible=false;
		}
		
		private function onClick(event:MouseEvent):void
		{
			this.dispatchEvent(new Event(Event.CLOSE));
		}
	}
}