package common
{
	import flash.display.Sprite;

	public class Common
	{
		public static const GRAY:int=0x4d4d4d;
		public static const ORANGE:int=0xff931e;
		
		public static function removeChildren(value:Sprite):void
		{
			while(value.numChildren>0)value.removeChildAt(0);
		}
	}
}