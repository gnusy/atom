package common
{
	import flash.display.Bitmap;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	import fontmanager.FontManager;
	
	import ru.flexdev.ui.Rect;
	import ru.flexdev.ui.SpriteEx;
	
	public class Background extends SpriteEx
	{
		[Embed(source="assets/cross.png")]
		private var crossClass:Class;
		
		private var title1:TextField;
		private var title2:TextField;
		
		private var toMenu:TextField;
		private var toMap:TextField;
		
		private var map:SpriteEx;
		private var menu:SpriteEx;
		private var cross:SpriteEx;
		private var rect_left:Rect;
		private var rect_right:Rect;
		
		public function Background(width:int, height:int)
		{
			super();
			super.width=width;
			super.height=height;
			
			this.rect_left=new Rect();
			this.rect_left.bounds(0, 0, 0, 0);
			this.addChild(this.rect_left);
			this.rect_left.fill(0xffffff, 0.95);
			
			this.rect_right=new Rect();
			this.rect_right.bounds(this.width, 0, 0, 0);
			this.addChild(this.rect_right);
			this.rect_right.fill(0xf4f4f4, 0.95);
			
			this.title1=FontManager.instance.textField;
			var frmt:TextFormat=FontManager.instance.textFormat(20, Common.GRAY, 'regular');
			this.title1.defaultTextFormat=frmt;
			this.title1.width=740;
			this.title1.wordWrap=true;
			this.title1.x=50;
			this.title1.y=46;
			this.addChild(this.title1);
			
			this.title2=FontManager.instance.textField;
			frmt=FontManager.instance.textFormat(15, Common.GRAY, 'light');
			frmt.align=TextFormatAlign.JUSTIFY;
			this.title2.defaultTextFormat=frmt;
			this.title2.width=Math.min(580, width-100);
			this.title2.wordWrap=true;
			this.title2.x=50;
			this.title2.y=78;
			this.addChild(this.title2);
			
			
			this.map=new SpriteEx();
			this.map.buttonMode=true;
			this.addChild(this.map);
			this.map.x=628;
			this.map.y=18;
			
			frmt=FontManager.instance.textFormat(12, Common.GRAY, 'bold');
			this.toMap=FontManager.instance.textField;
			this.toMap.defaultTextFormat=frmt;
			this.toMap.width=120;
			this.toMap.x=0;
			this.toMap.y=0;
			this.toMap.text='Вернуться на карту';
			this.toMap.height=this.toMap.textHeight+4;
			this.toMap.width=this.toMap.textWidth+4;
			this.map.addChild(this.toMap);
			
			this.map.width=this.toMap.width;
			this.map.height=this.toMap.height;
			this.map.addEventListener(MouseEvent.CLICK, this.onMouseClick);
			
			
			
			
			this.menu=new SpriteEx();
			this.menu.buttonMode=true;
			this.addChild(this.menu);
			this.menu.x=763;
			this.menu.y=18;
			
			this.toMenu=FontManager.instance.textField;
			this.toMenu.defaultTextFormat=frmt;
			this.toMenu.width=60;
			this.toMenu.x=0;
			this.toMenu.y=0;
			this.toMenu.text='Задания';
			this.toMenu.height=this.toMenu.textHeight+4;
			this.toMenu.width=this.toMenu.textWidth+4;
			this.menu.addChild(this.toMenu);
			
			this.menu.width=this.toMenu.width;
			this.menu.height=this.toMenu.height;
			this.menu.addEventListener(MouseEvent.CLICK, this.onMouseClick);
			
			
			
			
			this.cross=new SpriteEx();
			var cross_image:Bitmap=new this.crossClass() as Bitmap;
			cross_image.smoothing=true;
			this.cross.addChild(cross_image);
			this.cross.width=cross_image.width;
			this.cross.height=cross_image.height;
			this.cross.buttonMode=true;
			this.cross.right=22;
			this.cross.top=21;
			
			
			this.addChild(this.cross);
			this.cross.addEventListener(MouseEvent.CLICK, this.onMouseClick);
		}

		public function round(rX:int, rY:int):void
		{
			this.rect_left.round(rX, rY);
		}
		
		public function set split(value:Boolean):void
		{
			if(value==true)
			{
				this.rect_left.bounds(0, 0, 328, 0);
				this.rect_right.bounds(543, 0, 0, 0);
			}
			else
			{
				this.rect_left.bounds(0, 0, 0, 0);
				this.rect_right.bounds(this.width, 0, 0, 0);
			}
			
		}
		
		public function setTitles(title1:String, title2:String=''):Background
		{
			this.title1.text=title1;
			this.title2.text=title2;
			
			this.title1.height=this.title1.textHeight+5;
			this.title2.height=this.title2.textHeight+5;
			return this;
		}
		
		public function setTitleFormat(title:String, frmt:TextFormat):Background
		{
			switch(title)
			{
				case 'title1':
					this.title1.defaultTextFormat=frmt;
					this.title1.setTextFormat(frmt);
					break;
				
				case 'title2':
					this.title2.defaultTextFormat=frmt;
					this.title2.setTextFormat(frmt);
					break;
			}
			this.title1.height=this.title1.textHeight+5;
			this.title2.height=this.title2.textHeight+5;
			return this;
		}
		
		
		public function hideButton(type:String):Background
		{
			switch(type)
			{
				case 'toMenu':
					this.menu.visible=false;
					this.map.x=697;
					break;
				
				case 'toMap':
					this.map.visible=false;
					
					break;
				
				case 'cross':
					this.cross.visible=false;
					break;
				
				case 'all':
					this.menu.visible=false;
					this.map.visible=false;
					this.cross.visible=false;
					break;
			}
			return this;
		}
		
		public function topTitle(type:String, top:int):Background
		{
			switch(type)
			{
				case 'title1':
					this.title1.y=top;
					break;
				
				case 'title2':
					this.title2.y=top;
					break;
			}
			return this;
		}
		
		public function leftTitle(type:String, left:int):Background
		{
			switch(type)
			{
				case 'title1':
					this.title1.x=left;
					break;
				
				case 'title2':
					this.title2.x=left;
					break;
			}
			return this;
		}
		
		public function topButtonCross(top:int):Background
		{
			this.cross.top=top;
			return this;
		}
		
		private function onMouseClick(event:MouseEvent):void
		{
			switch(event.currentTarget)
			{
				case this.map:
					this.dispatchEvent(new Event('return_to_map', true));
					break;
				
				case this.menu:
					this.dispatchEvent(new Event('return_to_menu', true));
					break;
				
				case this.cross:
					this.dispatchEvent(new Event('game_close', true));
					break;
			}
		}
	}
}

