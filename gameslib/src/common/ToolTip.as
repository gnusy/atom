package common
{
	import flash.display.Sprite;
	import flash.filters.DropShadowFilter;
	import flash.filters.GlowFilter;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import fontmanager.FontManager;
	
	public class ToolTip extends Sprite
	{
		public static var WIDTH:int=210;
		public static var HEIGHT:int=94;
		
		private var text:TextField;
		
		public function ToolTip()
		{
			super();
			this.hide();
			this.mouseEnabled=false;
			var frmt:TextFormat=FontManager.instance.textFormat(14, Common.GRAY, 'bold');
			frmt.align='center';
			
			this.text=FontManager.instance.textField;
			this.text.defaultTextFormat=frmt;
			this.text.wordWrap=true;
			this.text.x=8;
			this.text.width=WIDTH-20;
			this.addChild(this.text);
			
			var glow:GlowFilter=new GlowFilter(0xbababa, 0.25, 8, 8);
			var shadow:DropShadowFilter=new DropShadowFilter(8, 90, 0x000000, 0.5, 4, 8);
			this.filters=[glow, shadow];
		}
		
		public function show(text:String, position:Point):void
		{
			this.text.width=WIDTH-30;
			this.text.text=text;
			var bubble_width:int=WIDTH;
			
			
			if(this.text.textWidth+5<WIDTH-20)
			{
				this.text.width=this.text.textWidth+7;
				bubble_width=this.text.width+20;
			}
			
			this.text.height=this.text.textHeight+5;
			var bubble_height:int=this.text.textHeight+5+20;
			
			this.draw(bubble_width, bubble_height);
			
			
			this.x=position.x-bubble_width/2;
			this.y=position.y-bubble_height;
			
			this.text.y=(bubble_height-this.text.textHeight)/2-5;
			
			
			this.visible=true;
			
		}
		
		public function hide():void
		{
			this.visible=false;
		}
		
		public function draw(bubble_width:int, bubble_height:int):void
		{
			this.graphics.clear();
			this.graphics.beginFill(0xffffff, 1);
			this.graphics.drawRoundRect(0, 0, bubble_width, bubble_height-7, 14, 14);
			
			this.graphics.moveTo(bubble_width/2-7, bubble_height-7);
			this.graphics.lineTo(bubble_width/2, bubble_height);
			this.graphics.lineTo(bubble_width/2+7, bubble_height-7);
			
			this.graphics.endFill();
		}
		
	}
}
