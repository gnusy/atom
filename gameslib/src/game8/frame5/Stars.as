package game8.frame5
{
	import flash.geom.Point;
	import flash.utils.clearInterval;
	import flash.utils.setInterval;
	
	import ru.flexdev.ui.Rect;
	
	public class Stars extends Rect
	{
		[Embed(source="assets/star1.png")]private var star1:Class;
		[Embed(source="assets/star2.png")]private var star2:Class;
		[Embed(source="assets/star3.png")]private var star3:Class;
		[Embed(source="assets/star4.png")]private var star4:Class;
		[Embed(source="assets/star5.png")]private var star5:Class;
		[Embed(source="assets/star6.png")]private var star6:Class;
		
		private var stars:Vector.<Star>;
		private var counter:int=0;
		private var interval:uint;
		
		public function Stars()
		{
			super();
			this.width=310;
			this.height=310;
			//this.fill(0xff0000, 0.25);
			
			this.stars=new Vector.<Star>();
			this.stars.push(this.addChild(new Star(this.star1)));
			this.stars.push(this.addChild(new Star(this.star2)));
			this.stars.push(this.addChild(new Star(this.star3)));
			this.stars.push(this.addChild(new Star(this.star4)));
			this.stars.push(this.addChild(new Star(this.star5)));
			this.stars.push(this.addChild(new Star(this.star6)));
			
			this.placeStars();
			
		}
		
		public function start():void
		{
			this.interval=setInterval(this.moveStars, 100);
		}
		
		public function stop():void
		{
			clearInterval(this.interval);
		}
		
		private function placeStars():void
		{
			for(var i:int=0;i<this.stars.length;i++)
			{
				this.stars[i].moveTo(new Point(this.width/2, this.height/2), true);
			}
		}
		
		private function moveStars():void
		{
			this.counter+=5;
			var angle:Number=this.counter*Math.PI/180;
				
			for(var i:int=0;i<this.stars.length;i++)
			{
				var r_float:Number=Math.sin(angle*4)*20;
				
				var x1:Number=Math.sin(angle+2*i*60*Math.PI/360)*(120+r_float)+this.width/2;
				var y1:Number=-Math.cos(angle+2*i*60*Math.PI/360)*(120+r_float)+this.height/2;
				this.stars[i].moveTo(new Point(x1, y1));
			}
		}
	}
}