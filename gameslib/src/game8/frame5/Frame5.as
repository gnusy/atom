package game8.frame5
{
	import common.Background;
	import common.ButtonSkin;
	
	import fireworks.Fireworks;
	
	import flash.events.MouseEvent;
	
	import game8.Frame;
	
	import ru.flexdev.ui.Button;
	
	public class Frame5 extends Frame
	{
		private var background:Background;
		private var stars:Stars;
		private var rotator:Rotator;
		private var fire:Fireworks;
		
		public function Frame5(width:int, height:int)
		{
			super(width, height);
			
			this.background=new Background(862, 446);
			this.addChild(this.background);
			this.background.setTitles('Поздравляем! Вы у финишной прямой!', 'Теперь давайте проверим, что Вы усвоили после\nпутешествия по машиностроительному дивизиону!\n\nВам предстоит пройти тест из 10 вопросов!');
			this.background.hideButton('all');
			
			//this.background.topTitle('title1', 53);
			this.background.topTitle('title2', 88);
			this.background.top=75;
			this.background.horizontalCenter=0;
			
			var button:Button=new Button();
			button.left=50;
			button.top=180;
			button.width=120;
			button.height=28;
			button.label='Пройти тест';
			button.skinClass=common.ButtonSkin;
			this.background.addChild(button);
			button.addEventListener(MouseEvent.CLICK, this.onMouseClick);
			/*
			this.stars=new Stars();
			this.stars.horizontalCenter=0;
			this.stars.verticalCenter=83;
			this.background.addChild(this.stars);
			*/
			this.rotator=new Rotator();
			this.rotator.right=40;
			this.rotator.verticalCenter=0;
			this.background.addChild(this.rotator);
			
			this.fire=new Fireworks();
			this.fire.right=40;
			this.fire.verticalCenter=0;
			this.background.addChild(this.fire);
			this.fire.visible=false;
		}
		
		override public function show(now:Boolean=false):void
		{
			super.show(now)
			//this.stars.start();
			this.rotator.start();
		}
		
		override public function hide():void
		{
			super.hide()
			//this.stars.stop();
		}
		
		private function onMouseClick(event:MouseEvent):void
		{
			super.complete();
		}
	}
}