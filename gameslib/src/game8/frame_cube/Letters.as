package game8.frame_cube
{
	import caurina.transitions.Tweener;
	
	import flash.display.Bitmap;
	
	import ru.flexdev.ui.SpriteEx;
	
	public class Letters extends SpriteEx
	{
		[Embed(source="assets/letter0.png")]private var letter1:Class;
		[Embed(source="assets/letter1.png")]private var letter2:Class;
		[Embed(source="assets/letter2.png")]private var letter3:Class;
		[Embed(source="assets/letter3.png")]private var letter4:Class;
		[Embed(source="assets/letter4.png")]private var letter5:Class;
		[Embed(source="assets/letter5.png")]private var letter6:Class;
		
		
		private var letters:Array;
		
		public function Letters()
		{
			super();
			this.letters=new Array();
			this.letters.push(new letter1() as Bitmap);
			this.letters.push(new letter2() as Bitmap);
			this.letters.push(new letter3() as Bitmap);
			this.letters.push(new letter4() as Bitmap);
			this.letters.push(new letter5() as Bitmap);
			this.letters.push(new letter6() as Bitmap);
			
			for(var i:int=0;i<this.letters.length;i++)
			{
				var letter:Bitmap=this.letters[i];
				letter.x=(88-letter.width)/2+i*131;
				this.addChild(letter);
				letter.alpha=0.6;
			}
			
			this.visible=false;
			this.alpha=0;
		}
		
		public function show():void
		{
			this.visible=true;
			Tweener.addTween(this, {alpha:1, time:1});
		}
		
		public function showLetter(index:int):void
		{
			for(var i:int=0;i<this.letters.length;i++)
			{
				(this.letters[i] as Bitmap).alpha=0.6;
				if(i==index)(this.letters[i] as Bitmap).alpha=1;
			}
		}
	}
}