package game8.frame_cube
{
	import caurina.transitions.Tweener;
	
	import flash.utils.clearInterval;
	import flash.utils.setInterval;
	import flash.utils.setTimeout;
	
	import ru.flexdev.ui.SpriteEx;
	
	public class DrawHolder extends SpriteEx
	{
		private var index:int=0;
		private var timer:uint=0;
		private var counter:int=0;
		
		private var sides:SpriteEx;
		
		public function DrawHolder()
		{
			super();
			this.mouseEnabled=false;
			this.sides=new SpriteEx();
			this.sides.graphics.beginFill(0xc6c6c6);
			this.sides.graphics.moveTo(0, 0);
			this.sides.graphics.lineTo(0, 71);
			this.sides.graphics.lineTo(63, 130);
			this.sides.graphics.lineTo(63, 62);
			this.sides.graphics.lineTo(0, 0);
			this.sides.graphics.endFill();
			this.sides.graphics.beginFill(0xdddddd);
			this.sides.graphics.moveTo(126, 0);
			this.sides.graphics.lineTo(126, 71);
			this.sides.graphics.lineTo(64, 130);
			this.sides.graphics.lineTo(64, 62);
			this.sides.graphics.lineTo(126, 0);
			this.sides.graphics.endFill();
			this.addChild(this.sides);
			this.sides.visible=false;
		}
		
		public function showItem(value:int):void
		{
			this.graphics.clear();
			
			this.index=value;
			var _x:int=73+44-63+131*this.index;
			this.sides.x=_x;
			this.sides.y=210;
			this.sides.visible=false;
			setTimeout(this.showLines, 250);
		}
		
		private function showLines():void
		{
			this.sides.visible=true;
			var _x:int=73+44+131*this.index;
			var _y:int=271;
			
			this.graphics.lineStyle(0, 0xfbb03b);
			
			this.graphics.moveTo(_x, _y);
			this.graphics.lineTo(_x, _y+70);
			this.graphics.lineTo(117, _y+70);
			
			
			this.graphics.lineTo(117, _y+115);
			
			this.graphics.beginFill(0xfbb03b);
			this.graphics.drawCircle(117, _y+115, 5);
			this.graphics.endFill();
		}
	}
}