package game8.frame_cube
{
	import caurina.transitions.Tweener;
	
	import common.Background;
	import common.Common;
	
	import flash.display.Bitmap;
	import flash.events.DataEvent;
	import flash.events.Event;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.utils.clearInterval;
	import flash.utils.setInterval;
	import flash.utils.setTimeout;
	
	import fontmanager.FontManager;
	
	import game8.Frame;
	
	import ru.flexdev.events.EventEx;
	import ru.flexdev.ui.Rect;
	
	public class NeoCube extends Frame
	{
		[Embed(source="assets/hint.png")]private var hintClass:Class;
		
		[Embed(source="assets/item1.png")]private var item01:Class;
		[Embed(source="assets/item2.png")]private var item02:Class;
		[Embed(source="assets/item3.png")]private var item03:Class;
		[Embed(source="assets/item4.png")]private var item04:Class;
		[Embed(source="assets/item5.png")]private var item05:Class;
		[Embed(source="assets/item6.png")]private var item06:Class;
		[Embed(source="assets/item1_gray.png")]private var item01_gray:Class;
		[Embed(source="assets/item2_gray.png")]private var item02_gray:Class;
		[Embed(source="assets/item3_gray.png")]private var item03_gray:Class;
		[Embed(source="assets/item4_gray.png")]private var item04_gray:Class;
		[Embed(source="assets/item5_gray.png")]private var item05_gray:Class;
		[Embed(source="assets/item6_gray.png")]private var item06_gray:Class;
		
		[Embed(source="assets/item7_gray.png")]private var item07_gray:Class;
		[Embed(source="assets/item8_gray.png")]private var item08_gray:Class;
		[Embed(source="assets/item9_gray.png")]private var item09_gray:Class;
		[Embed(source="assets/item10_gray.png")]private var item10_gray:Class;
		[Embed(source="assets/item11_gray.png")]private var item11_gray:Class;
		[Embed(source="assets/item12_gray.png")]private var item12_gray:Class;
		
		
		
		public static const WIDTH:int=893;
		public static const HEIGHT:int=590;
		
		private var background:Background;
		
		private var title:TextField;
		private var text:TextField;
		private var counter:int;
		private var interval:uint;
		private var stopped:Boolean=false;
		
		private var items_data:Array=[
			{name:'На шаг\nвпереди',				image:item01, image_gray:item01_gray, text:'Мы стремимся быть лидером на глобальных рынках. Мы всегда на шаг впереди в технологиях, знаниях и качествах наших сотрудников. Мы предвидим, что будет завтра, и готовы к этому сегодня. Мы постоянно развиваемся и учимся. Каждый день мы стараемся работать лучше, чем вчера.', x:73, y:145, x_to:73, y_to:165, correct:true},
			{name:'Эффективность',					image:item02, image_gray:item02_gray, text:'Мы всегда находим наилучшие варианты решения задач. Мы эффективны во всем, что мы делаем — при выполнении поставленных целей мы максимально рационально используем ресурсы компании и постоянно совершенствуем рабочие процессы. Нет препятствий, которые могут помешать нам находить самые эффективные решения.', x:597, y:342, x_to:204, y_to:165, correct:true},
			{name:'Ответственность\nза результат',	image:item03, image_gray:item03_gray, text:'Каждый из нас несет личную ответственность за результат своей работы и качество своего труда перед государством, отраслью, коллегами и заказчиками. В работе мы предъявляем к себе самые высокие требования. Оцениваются не затраченные усилия, а достигнутый результат. Успешный результат — основа для наших новых достижений.', x:335, y:342, x_to:335, y_to:165, correct:true},
			{name:'Единая\nкоманда',				image:item04, image_gray:item04_gray, text:'Мы все — Росатом. У нас общие цели. Работа в команде единомышленников позволяет достигать уникальных результатов. Вместе мы сильнее и можем добиваться самых высоких целей. Успехи сотрудников — успехи компании.', x:73, y:342, x_to:466, y_to:165, correct:true},
			{name:'Уважение',						image:item05, image_gray:item05_gray, text:'Мы с уважением относимся к нашим заказчикам, партнерам и поставщикам. Мы всегда внимательно слушаем и слышим друг друга вне зависимости от занимаемых должностей и места работы. Мы уважаем историю и традиции отрасли. Достижения прошлого вдохновляют нас на новые победы.', x:204, y:342, x_to:597, y_to:165, correct:true},
			{name:'Безопасность',					image:item06, image_gray:item06_gray, text:'Безопасность — наивысший приоритет. В нашей работе мы в первую очередь обеспечиваем полную безопасность людей и окружающей среды. В безопасности нет мелочей — мы знаем правила безопасности и выполняем их, пресекая нарушения.', x:466, y:145, x_to:728, y_to:165, correct:true},
			
			{name:'Ответственность',				image:item07_gray, image_gray:item07_gray, text:'', x:204, y:145, correct:false},
			{name:'Командный\nрезультат',			image:item08_gray, image_gray:item08_gray, text:'', x:335, y:145, correct:false},
			{name:'Эффективное\nуправление',		image:item09_gray, image_gray:item09_gray, text:'', x:597, y:145, correct:false},
			{name:'Лояльность',						image:item10_gray, image_gray:item10_gray, text:'', x:728, y:145, correct:false},
			{name:'Инновации',						image:item11_gray, image_gray:item11_gray, text:'', x:466, y:342, correct:false},
			{name:'Профессионализм', 				image:item12_gray, image_gray:item12_gray, text:'', x:728, y:342, correct:false}
			
		];
		
		private var items:Vector.<Item>;
		
		private var letters:Letters;
		private var hint:Bitmap;
		private var texHolder:Rect;
		private var drawHolder:DrawHolder;
		
		public function NeoCube(width:int, height:int)
		{
			super(width, height);
			

			this.background=new Background(WIDTH, HEIGHT);
			this.addChild(this.background);
			this.background.setTitles('Реализацию стратегии Госкорпорации во многом направляют корпоративные ценности.', 'Выберите 6 ключевых ценностей «Росатома»');
			this.background.setTitleFormat('title1', FontManager.instance.textFormat(21, 0x4d4d4d, 'bold')); 
			this.background.setTitleFormat('title2', FontManager.instance.textFormat(17, 0x4d4d4d, 'regular'));
			this.background.topTitle('title1', 40);
			this.background.topTitle('title2', 103);
			this.background.hideButton('toMenu');
			this.background.hideButton('toMap');
			this.background.x=52;
			this.background.y=71;
			
			this.items=new Vector.<Item>();
			
			for(var i:int=0;i<this.items_data.length;i++)
			{
				var item:Item=new Item(this.items_data[i]);
				item.x=this.items_data[i]['x'];
				item.y=this.items_data[i]['y'];
				this.background.addChild(item);
				this.items.push(item);
			}
			
			this.title=FontManager.instance.textField;
			var frmt:TextFormat=FontManager.instance.textFormat(24, 0x4d4d4d, 'bold');
			frmt.align=TextFormatAlign.JUSTIFY;
			this.title.defaultTextFormat=frmt;
			this.title.width=590;
			this.title.x=67;
			this.title.y=94;
			
			this.text=FontManager.instance.textField;
			frmt=FontManager.instance.textFormat(17, 0x4d4d4d, 'regular');
			frmt.align=TextFormatAlign.LEFT;
			this.text.defaultTextFormat=frmt;
			this.text.wordWrap=true;
			this.text.width=590;
			//this.text.height=100;
			this.text.x=67;
			this.text.y=134;
			
			
			this.letters=new Letters();
			this.background.addChild(this.letters);
			this.letters.x=73;
			this.letters.y=100;
			
			
			this.hint=new hintClass() as Bitmap;
			this.background.addChild(this.hint);
			this.hint.x=73;
			this.hint.y=269;
			this.hint.visible=false;
			this.hint.alpha=0;
			
			
			this.texHolder=new Rect();
			this.texHolder.width=730;
			this.texHolder.height=280;
			this.texHolder.bottom=30;
			this.texHolder.horizontalCenter=0;
			this.texHolder.fill();
			this.background.addChild(this.texHolder);
			this.texHolder.visible=false;
			this.texHolder.addChild(this.text);
			this.texHolder.addChild(this.title);
			
			this.drawHolder=new DrawHolder();
			this.background.addChild(this.drawHolder);
			
			this.addEventListener('item_selected', this.onItemSelectTest);
			this.addEventListener('game_close', this.onGameClose);
			
		}
		
		private function onGameClose(event:Event):void
		{
			event.stopImmediatePropagation();
			clearInterval(this.interval);
			super.complete();
			this.dispatchEvent(new DataEvent(Event.COMPLETE, true, false, '1'));
		}
		
		private function onItemSelectTest(event:EventEx):void
		{
			for(var i:int=0;i<this.items.length;i++)
			{
				if(this.items[i].correct==false)return;
			}
			
			this.removeEventListener('item_selected', this.onItemSelectTest);
			
			for(i=0;i<this.items.length;i++)
			{
				this.items[i].postTest();
				if(this.items[i].isCorrect==false)
				{
					this.items.splice(i, 1);
					i--;
				}
			}
			
			for(i=0;i<this.items_data.length;i++)
			{
				if(this.items_data[i]['correct']==false)
				{
					this.items_data.splice(i, 1);
					i--;
				}
			}
			
			this.letters.show();
			
			this.background.setTitles('Единые корпоративные ценности', '');
			setTimeout(this.showHint, 1000);
			this.addEventListener('item_selected', this.onItemSelect);
			
		}

		public function showHint():void
		{
			this.hint.visible=true;
			Tweener.addTween(this.hint, {alpha:1, time:0.5, transition:'linear'});
		}
		
		private function onItemSelect(event:EventEx):void
		{
			this.hint.visible=false;
			
			var index:int=this.items.indexOf(event.data);
			for(var i:int=0;i<this.items.length;i++)
			{
				if(this.items[i]!=event.data)this.items[i].selected=false;
				if(this.items[i]==event.data)
				{
					this.letters.showLetter(i);
				}
			}
			
			this.text.text=this.items_data[index]['text'];
			
			var name:String=this.items_data[index]['name'];
			name=name.split('\n').join(' ');
			
			this.title.text=name;
			this.texHolder.visible=true;
			
			this.drawHolder.showItem(index);
			
		}


		public function center(parentWidht:int, parentHeight:int):void
		{
			this.x=(parentWidht-WIDTH)/2;
			this.y=(parentHeight-HEIGHT)/2;
			
		}
	}
}