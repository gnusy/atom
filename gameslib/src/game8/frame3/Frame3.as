package game8.frame3
{
	import common.Background;
	import common.ButtonSkin;
	import common.Common;
	
	import flash.display.Bitmap;
	import flash.events.MouseEvent;
	
	import fontmanager.FontManager;
	
	import game8.Frame;
	
	import ru.flexdev.ui.Button;
	import ru.flexdev.ui.Image;
	
	public class Frame3 extends Frame
	{
		[Embed(source="assets/pers.png")]private var persClass:Class;
		
		private var background:Background;
		private var pers:Image;
		
		public function Frame3(width:int, height:int)
		{
			super(width, height);
			
			this.background=new Background(580, 370);
			this.addChild(this.background);
			this.background.setTitles('Отлично!', 'Теперь Вы узнали, что такое ценности Росатома. Ценности – это опора при принятии решений в ситуации неопределенности. Руководствуясь ими, мы быстрее  и эффективнее достигаем результата.\n\nПоступая в духе ценностей, во всех рабочих ситуациях можно быть уверенным, что  всегда примешь верное решение. Ценности помогают дивизиону формировать  культуру результата.');
			this.background.hideButton('all');
			this.background.topTitle('title1', 53);
			this.background.topTitle('title2', 108);
			this.background.setTitleFormat('title2', FontManager.instance.textFormat(17, Common.GRAY, 'light'));
			this.background.top=75;
			this.background.left=368;
			
			var button:Button=new Button();
			button.left=54;
			button.top=310;
			button.width=69;
			button.height=28;
			button.label='Далее';
			button.skinClass=common.ButtonSkin;
			this.background.addChild(button);
			button.addEventListener(MouseEvent.CLICK, this.onMouseClick);
			
			var image:Bitmap=new persClass() as Bitmap;
			image.smoothing=true;
			this.pers=new Image();
			this.pers.source=image.bitmapData;
			this.pers.left=8;
			this.pers.bottom=0;
			this.pers.mouseEnabled=false;
			this.addChild(this.pers);
		}
		
		private function onMouseClick(event:MouseEvent):void
		{
			super.complete();
		}
	}
}