package game8.frame4
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.filters.GlowFilter;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import caurina.transitions.Tweener;
	
	import common.Common;
	import common.TryButtonSkin;
	
	import fontmanager.FontManager;
	
	import ru.flexdev.ui.Button;
	import ru.flexdev.ui.Rect;
	
	public class Reflection extends Rect
	{
		public function Reflection()
		{
			super();
			this.fill(0x000000, 0.25);
			
			this.visible=false;
			this.alpha=0;
			
			var window:Rect=new Rect();
			window.width=387;
			window.height=139;
			window.verticalCenter=0;
			window.horizontalCenter=0;
			window.fill(0xffffff).round(12, 12);
			this.addChild(window);
			
			
			var glow:GlowFilter=new GlowFilter(0xbababa, 0.25, 8, 8);
			var shadow:DropShadowFilter=new DropShadowFilter(8, 90, 0x000000, 0.5, 4, 8);
			window.filters=[glow, shadow];
			
			
			var title:TextField=FontManager.instance.textField;
			var frmt:TextFormat=FontManager.instance.textFormat(20, Common.GRAY, 'regular');
			title.defaultTextFormat=frmt;
			title.y=25;
			title.text='Неправильно!';
			title.width=title.textWidth+5;
			title.height=title.textHeight+5;
			title.x=(window.width-title.width)/2;
			window.addChild(title);
			
			var button:Button=new Button();
			button.horizontalCenter=0;
			button.bottom=32;
			button.width=140;
			button.height=20;
			button.label='Попробуйте ещё раз';
			button.skinClass=common.TryButtonSkin;
			window.addChild(button);
			button.addEventListener(MouseEvent.CLICK, this.onClick);
		}
		
		public function show():void
		{
			this.visible=true;
			Tweener.addTween(this, {alpha:1, time:0.25, transition:'linear'});
		}
		
		public function hide():void
		{
			Tweener.addTween(this, {alpha:0, time:0.25, transition:'linear', onComplete:this.onHideComplete});
		}
		
		private function onHideComplete():void
		{
			this.visible=false;
		}
		
		private function onClick(event:MouseEvent):void
		{
			this.dispatchEvent(new Event(Event.CLOSE));
		}
	}
}