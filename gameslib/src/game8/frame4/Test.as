package game8.frame4
{
	import common.Common;
	
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import fontmanager.FontManager;
	
	import ru.flexdev.ui.Rect;
	
	public class Test extends Rect
	{
		private var answers:Array=	[
										{text:'достигать поставленных результатов в срок', correct:true},
										{text:'попасть в кадровый резерв', correct:true},
										{text:'номинироваться на конкурс «Человек года»', correct:true},
										{text:'участвовать в интересных проектах', correct:true},
										{text:'получить высокую оценку по итогам года', correct:true},
										{text:'сделать карьеру за месяц', correct:false},
										{text:'получить сертификат по программе обучения ценностям', correct:false}
									];
		
		private var items:Vector.<TestItem>;
		
		public function Test()
		{
			super();
			this.width=460;
			this.height=210;
			
			var title:TextField=FontManager.instance.textField;
			var frmt:TextFormat=FontManager.instance.textFormat(15, Common.GRAY, 'bold');
			title.defaultTextFormat=frmt;
			title.width=460;
			title.x=-3;
			title.y=-4;
			title.text='Выберите из списка 5 правильных вариантов';
			this.addChild(title);
			
			this.items=new Vector.<TestItem>();
			
			this.answers.sort(this.shuffleAnswers);
			
			for(var i:int=0;i<this.answers.length;i++)
			{
				var item:TestItem=new TestItem(this.answers[i].text, this.answers[i].correct);
				item.left=0;
				item.top=32+27*i;
				this.addChild(item);
				this.items.push(item);
			}
		}
		
		private function shuffleAnswers(obj1:Object, obj2:Object):Number
		{
			return Math.random()<0.5?-1:1;
		}
		
		public function get complete():Boolean
		{
			for(var i:int=0;i<this.items.length;i++)
			{
				if(this.items[i].correct==false)return false;
			}
			
			return true;
		}
	}
}