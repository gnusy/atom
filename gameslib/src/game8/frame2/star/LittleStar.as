package game8.frame2.star
{
	import flash.display.Bitmap;
	import flash.filters.GlowFilter;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import ru.flexdev.ui.SpriteEx;
	
	public class LittleStar extends SpriteEx
	{
		[Embed(source="assets/cursor.png")]private var little_starClass:Class;
		
		public static var DRAGGING:Boolean=false;
		private var _glow:GlowFilter;
		
		public function LittleStar()
		{
			super();
			this.visible=false;
			this.mouseEnabled=false;
			var img:Bitmap=new this.little_starClass() as Bitmap;
			img.smoothing=true;
			this.width=img.width;
			this.height=img.height;
			this.addChild(img);
			
			this._glow=new GlowFilter(0x000000, 1, 1.5, 1.5);
			this._glow.strength=16;
			
		}
		
		public function show():void
		{
			LittleStar.DRAGGING=true;
			this.visible=true;
		}
		
		public function hide():void
		{
			LittleStar.DRAGGING=false;
			this.visible=false;
		}
		
		public function set position(center:Point):void
		{
			this.x=center.x-this.width/2;
			this.y=center.y-this.height/2;
		}
		
		public function get rect():Rectangle
		{
			return new Rectangle(this.x, this.y, this.width, this.height);
		}
		
		public function set glow(value:Boolean):void
		{
			return;
			if(value)this.filters=[this._glow];
			else this.filters=[];
		}
	}
}