package game8.frame2.list
{
	import ru.flexdev.ui.ScrollerEx;
	import ru.flexdev.ui.SpriteEx;
	
	public class List extends SpriteEx
	{
		private var data:Array;
		private var scroller:ScrollerEx;
		private var items:Vector.<ListItem>;
		
		public function List(data:Array)
		{
			super();
			this.data=data;
			this.data.sort(this.shuffle);
			this.width=463;
			this.height=346;
			
			this.scroller=new ScrollerEx();
			this.scroller.height=this.height;
			this.scroller.width=this.width;			
			
			this.addChild(this.scroller);
			
			var itemsHolder:SpriteEx=new SpriteEx();
			
			var prevItem:ListItem=null;
			
			this.items=new Vector.<ListItem>();
			
			for(var i:int=0;i<this.data.length;i++)
			{
				var item_data:Object=this.data[i];
				var listItem:ListItem=new ListItem(item_data);
				this.items.push(listItem);
				itemsHolder.addChild(listItem);
				
				if(prevItem!=null)listItem.y=prevItem.y+prevItem.height+13;
				else listItem.y=5;
				prevItem=listItem;
				
				itemsHolder.height=prevItem.y+prevItem.height;
			}
			itemsHolder.y=0;
			this.scroller.addChild(itemsHolder);
			
			this.scroller.scrollTo(0);
		}
		
		public function shuffle(val1:Object, val2:Object):Number
		{
			return Math.random()>0.5?1:-1;
		}
		
		public function get score():int
		{
			var res:int=0;
			for(var i:int=0;i<this.items.length;i++)
			{
				res+=this.items[i].score;
			}
			return res;
		}
	}
}