package game4.frame_cube
{
	import caurina.transitions.Tweener;
	
	import common.Common;
	
	import flash.display.Bitmap;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import fontmanager.FontManager;
	
	import ru.flexdev.events.EventEx;
	import ru.flexdev.ui.Rect;
	import ru.flexdev.ui.SpriteEx;
	
	public class Item extends SpriteEx
	{
		private var _selected:Boolean=false;
		private var _correct:Boolean=false;
		
		private var title:TextField;
		public var mode:String='test';
		private var _data:Object;
		
		private var rect:Rect;
		private var rect_holder:SpriteEx;
		private var image:Bitmap;
		private var image_gray:Bitmap;
	
		public var errors:int=0;
		
		public function Item(data:Object)
		{
			super();
			this.width=88;
			this.height=88;
			
			this.rect_holder=new SpriteEx();
			this.rect_holder.x=this.rect_holder.y=44;
			this.addChild(this.rect_holder);
			
			this.rect=new Rect();
			this.rect.x=this.rect.y=-44;
			this.rect_holder.addChild(this.rect);
			
			this.rect.width=this.width;
			this.rect.height=this.height;
			this.rect.stroke(0xcccccc).fill();
			
			this.buttonMode=true;
			this._data=data;
			this._correct=data['correct'];
			
			this.title=FontManager.instance.textField;
			var frmt:TextFormat=FontManager.instance.textFormat(15, Common.GRAY, 'light');
			frmt.align='center';
			this.title.defaultTextFormat=frmt;
			this.title.wordWrap=true;
			this.title.width=150;
			this.title.x=-31;
			this.title.y=105;
			this.title.text=data['name'];
			this.title.height=this.title.textHeight+4;
			//this.title.width=this.title.textWidth+5;
			this.addChild(this.title);
			
			this.image=new this._data['image']() as Bitmap;
			this.image_gray=new this._data['image_gray']() as Bitmap;
			
			this.addChild(this.image);
			this.addChild(this.image_gray);
			
			this.image.x=(this.width-this.image.width)/2;
			this.image.y=(this.height-this.image.height)/2;
			
			this.image_gray.x=(this.width-this.image_gray.width)/2;
			this.image_gray.y=(this.height-this.image_gray.height)/2;
			
			this.update();	
			this.addEventListener(MouseEvent.CLICK, this.onClick);
			
		}
		
		private function onClick(event:MouseEvent):void
		{
			if(this.buttonMode==false)return;
			
			if(this.mode=='test')this._selected=!this._selected;
			else this._selected=true;
			this.update();
			
			
			
			if(this._correct==false && this._selected==true)this.errors++;
			this.dispatchEvent(new EventEx('item_selected', this, true));
			
		}
		
		private function update():void
		{
			var frmt:TextFormat=this.title.getTextFormat();
			
			
			if(this.mode=='test')
			{
				if(this._selected && this._correct)
				{
					frmt.color=0x000000;
					this.rect.stroke(0xfbb03b);
					
					this.image.visible=true;
					this.image_gray.visible=false;
					
				}
				else if(this._selected && !this._correct)
				{
					frmt.color=Common.GRAY;
					this.rect.stroke(0xff0000);
					
					this.image.visible=false;
					this.image_gray.visible=true;
				}
				else				
				{
					frmt.color=Common.GRAY;
					this.rect.stroke(0xcccccc);
					
					this.image.visible=false;
					this.image_gray.visible=true;
				}
			}
			else
			{
				if(this._selected && this._correct)
				{
					Tweener.addTween(this.rect_holder, {rotation:45, time:0.25});
					Tweener.addTween(this.image, {alpha:1, time:0.25});
				}
				else				
				{
					Tweener.addTween(this.rect_holder, {rotation:0, time:0.25});
					Tweener.addTween(this.image, {alpha:0.5, time:0.25});
				}
			}
			
			this.title.setTextFormat(frmt);
		}
		
		public function set selected(value:Boolean):void
		{
			 this._selected=value;
			 this.update();	
		}
		
		public function get correct():Boolean
		{
			return this._selected==this._correct;
		}
		
		public function get isCorrect():Boolean
		{
			return this._correct;
		}
		
		public function postTest():void
		{
			if(this._correct==false)Tweener.addTween(this, {alpha:0, time:0.5, transition:'linear', onComplete:this.onHide});
			else
			{
				Tweener.addTween(this, {x:this._data['x_to'], y:this._data['y_to'], time:1, transition:'linear', onComplete:this.onSlide});
				Tweener.addTween(this.image, {alpha:0.5, time:1, transition:'linear'});
				Tweener.addTween(this.title, {alpha:0, time:1, transition:'linear'});
				
				this.mode='normal';
				this.buttonMode=false;
				this._selected=false;
				this.update();
			}
		}
		
		private function onHide():void
		{
			this.visible=false;
		}
		
		private function onSlide():void
		{
			this.title.visible=false;
			this.buttonMode=true;
			this.mode='info';
		}
	}
}