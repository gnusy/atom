package game4.frame5
{
	import caurina.transitions.Tweener;
	
	import flash.display.Bitmap;
	import flash.geom.Point;
	import flash.utils.clearInterval;
	import flash.utils.setInterval;
	
	import ru.flexdev.ui.Rect;
	import ru.flexdev.ui.SpriteEx;
	
	public class Rotator extends Rect
	{
		[Embed(source="assets/rosatom.png")]private var star:Class;
		
		private var holder:SpriteEx;
		
		
		public function Rotator()
		{
			super();
			this.width=346;
			this.height=346;
			this.holder=new SpriteEx();
			this.holder.x=this.width/2;
			this.holder.y=this.height/2;
			this.addChild(this.holder);
			
			var image:Bitmap=new star() as Bitmap;
			image.smoothing=true;
			this.holder.addChild(image)
			image.x=-this.width/2;
			image.y=-this.height/2;
				
		}
		
		public function start():void
		{
			Tweener.addTween(this.holder, {rotation:360, time:5, transition:'linear', onComplete:this.onRotateComplete});
		}
	
		public function set centerPosition(point:Point):void
		{
			this.centerX=point.x;
			this.centerY=point.y;
		}
		
		public function set centerX(value:int):void
		{
			this.x=value-(this.scaleX*this.width)/2;
		}

		public function get centerX():int
		{
			return this.x+(this.scaleX*this.width)/2;
		}
		
		public function set centerY(value:int):void
		{
			this.y=value-(this.scaleY*this.height)/2;
		}
		
		public function get centerY():int
		{
			return this.y+(this.scaleY*this.height)/2;
		}
		
		private function onRotateComplete():void
		{
			this.start();
		}
	}
}