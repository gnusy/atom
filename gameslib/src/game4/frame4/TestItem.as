package game4.frame4
{
	import common.Common;
	
	import flash.display.Bitmap;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import fontmanager.FontManager;
	
	import ru.flexdev.ui.SpriteEx;
	
	public class TestItem extends SpriteEx
	{
		[Embed(source="assets/arrow_gray.png")]private var arrow_grayClass:Class;
		[Embed(source="assets/arrow_orange.png")]private var arrow_orangeClass:Class;
		[Embed(source="assets/arrow_red.png")]private var arrow_redClass:Class;
		
		private var _correct:Boolean;
		private var selected:Boolean=false;
		
		private var title:TextField;
		private var arrow_gray:Bitmap;
		private var arrow_orange:Bitmap;
		private var arrow_red:Bitmap;
		
		public var errors:int=0;
		
		public function TestItem(text:String, correct:Boolean)
		{
			super();
			this.width=425;
			this.height=8;
			this.buttonMode=true;
			this._correct=correct;
			
			this.arrow_gray=new this.arrow_grayClass() as Bitmap;
			this.arrow_gray.smoothing=true;
			this.arrow_gray.y=2;
			this.addChild(this.arrow_gray);
			
			this.arrow_orange=new this.arrow_orangeClass() as Bitmap;
			this.arrow_orange.smoothing=true;
			this.arrow_orange.y=2;
			this.addChild(this.arrow_orange);
			
			this.arrow_red=new this.arrow_redClass() as Bitmap;
			this.arrow_red.smoothing=true;
			this.arrow_red.y=2;
			this.addChild(this.arrow_red);
			
			
			this.title=FontManager.instance.textField;
			var frmt:TextFormat=FontManager.instance.textFormat(17, Common.GRAY, 'regular');
			this.title.defaultTextFormat=frmt;
			this.title.wordWrap=true;
			this.title.width=460;
			this.title.height=16;
			this.title.x=12;
			this.title.y=-6;
			this.title.text=text;
			this.title.height=this.title.textHeight+4;
			this.addChild(this.title);
			this.height=this.title.height;
			this.update();	
			this.addEventListener(MouseEvent.CLICK, this.onClick);
			
		}
		
		private function onClick(event:MouseEvent):void
		{
			this.selected=!this.selected;
			this.update();
			
			if(this._correct==false && this.selected==true)this.errors++;
			this.dispatchEvent(new Event('item_select', true));
		}
		
		private function update():void
		{
			var frmt:TextFormat=this.title.getTextFormat();
			if(this.selected && this._correct)
			{
				this.arrow_gray.visible=false;
				this.arrow_orange.visible=true;
				this.arrow_red.visible=false;
				frmt.color=Common.ORANGE;
			}
			else if(this.selected && !this._correct)
			{
				this.arrow_gray.visible=false;
				this.arrow_orange.visible=false;
				this.arrow_red.visible=true;
				frmt.color=0xff0000;
			}
			else
			{
				this.arrow_gray.visible=true;
				this.arrow_orange.visible=false;
				this.arrow_red.visible=false;
				frmt.color=Common.GRAY;
			}
			
			this.title.setTextFormat(frmt);
		}
		
		public function get correct():Boolean
		{
			return this._correct==this.selected;
		}
	}
}
