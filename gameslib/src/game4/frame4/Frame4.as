package game4.frame4
{
	import caurina.transitions.Tweener;
	
	import common.Background;
	import common.ButtonSkin;
	import common.Common;
	
	import flash.display.Bitmap;
	import flash.events.DataEvent;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.utils.setTimeout;
	
	import fontmanager.FontManager;
	
	import game4.Frame;
	
	import ru.flexdev.events.EventEx;
	import ru.flexdev.ui.Button;
	import ru.flexdev.ui.Image;
	import ru.flexdev.ui.SpriteEx;

	
	public class Frame4 extends Frame
	{
		[Embed(source="assets/pers.png")]private var persClass:Class;
		[Embed(source="assets/pers_success.png")]private var pers_successClass:Class;
		
		private var background:Background;
		private var pers:Image;
		private var persSuccess:Image;
		
		private var button:Button;
		private var test:Test;
		private var reflection:Reflection;
		
		public function Frame4(width:int, height:int)
		{
			super(width, height);
			
			this.background=new Background(580, 500);
			this.addChild(this.background);
			this.background.setTitles('Не так просто сразу понять практическое значение\nкорпоративных ценностей. Это — новый сотрудник.\nПомогите ему разобраться, какие возможности\nдает ему лично и его предприятию следование в\nсвоей работе ценностям.', '');
			this.background.hideButton('all');
			this.background.topTitle('title1', 53);
			this.background.topTitle('title2', 53);
			this.background.top=75;
			this.background.left=368;
			this.background.round(24, 24);
			
			var kluv:SpriteEx=new SpriteEx();
			kluv.graphics.beginFill(0xffffff, 0.95);
			kluv.graphics.moveTo(0, 0);
			kluv.graphics.lineTo(-50, 20);
			kluv.graphics.lineTo(0, 40);
			kluv.graphics.lineTo(0, 0);
			kluv.graphics.endFill();
			this.background.addChild(kluv);
			kluv.y=150;
			
			this.button=new Button();
			this.button.left=54;
			this.button.bottom=58;
			this.button.width=80;
			this.button.height=28;
			this.button.label='Проверить';
			this.button.skinClass=common.ButtonSkin;
			this.background.addChild(this.button);
			this.button.addEventListener(MouseEvent.CLICK, this.onCheckTest);
			this.button.visible=false;
			
			var image:Bitmap=new persClass() as Bitmap;
			image.smoothing=true;
			this.pers=new Image();
			this.pers.source=image.bitmapData;
			this.pers.left=-20;
			this.pers.bottom=0;
			this.pers.mouseEnabled=false;
			this.addChild(this.pers);
			
			image=new pers_successClass() as Bitmap;
			image.smoothing=true;
			this.persSuccess=new Image();
			this.persSuccess.source=image.bitmapData;
			this.persSuccess.left=-20;
			this.persSuccess.bottom=0;
			this.persSuccess.mouseEnabled=false;
			this.addChild(this.persSuccess);
			this.persSuccess.alpha=0;

			
			this.test=new Test();
			this.background.addChild(this.test);
			this.test.bounds(55, 190);
			
			this.reflection=new Reflection();
			this.reflection.bounds(0, 0, 0, 0);
			this.reflection.addEventListener(Event.CLOSE, this.onReflectionClose);
			this.addChild(this.reflection);
			
			this.addEventListener('item_select', this.onCheckTest2);
		}
		
		override public function show(now:Boolean=false):void
		{
			super.show(now)
		
		}
		
		override public function hide():void
		{
			super.hide()
		}
		
		private function onCheckTest(event:MouseEvent):void
		{
			if(this.test.complete==false)
			{
				this.reflection.show();
				return;
			}
			else
			{
				this.testComplete();
			}
		}
		
		private function onCheckTest2(event:Event):void
		{
			if(this.test.complete==true)
			{
				this.testComplete();
			}
		}
		
		private function onReflectionClose(event:Event):void
		{
			this.reflection.hide();
			this.testComplete();
		}
		
		private function onComplete(event:MouseEvent):void
		{
			super.complete();
		}
		
		private function testComplete():void
		{
			this.button.removeEventListener(MouseEvent.CLICK, this.onCheckTest);
			
			Tweener.addTween(this.background, {height:210, time:0.25, transition:'linear'});
			Tweener.addTween(this.test, {alpha:0, time:0.125, transition:'linear'});
			Tweener.addTween(this.test, {alpha:0, time:0.125, transition:'linear'});
			Tweener.addTween(this.pers, {alpha:0, time:0.25, transition:'linear'});
			Tweener.addTween(this.persSuccess, {alpha:1, time:0.25, transition:'linear'});
			//Tweener.addTween(this.button, {bottom:109, time:0.25, transition:'linear'});
			
			setTimeout(this.finalize, 250);
			
			
			this.dispatchEvent(new EventEx('game_score', this.test.score, true));
			
		}
		
		private function finalize():void
		{
			this.button.addEventListener(MouseEvent.CLICK, this.onComplete);
			this.background.setTitles('', 'Эти возможности,  действительно, важны для меня. Я хочу быть успешным и эффективным. Спасибо за помощь!');
			this.background.setTitleFormat('title2', FontManager.instance.textFormat(17, Common.GRAY, 'light'));
			this.button.label='Далее';
			this.button.visible=true;
		}
	}
}