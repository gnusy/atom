package game4.frame3
{
	import common.Background;
	import common.ButtonSkin;
	import common.Common;
	
	import flash.display.Bitmap;
	import flash.events.MouseEvent;
	
	import fontmanager.FontManager;
	
	import game4.Frame;
	
	import ru.flexdev.ui.Button;
	import ru.flexdev.ui.Image;
	import ru.flexdev.ui.SpriteEx;
	
	public class Frame3 extends Frame
	{
		[Embed(source="assets/pers.png")]private var persClass:Class;
		
		private var background:Background;
		private var pers:Image;
		
		public function Frame3(width:int, height:int)
		{
			super(width, height);
			
			this.background=new Background(580, 390);
			this.addChild(this.background);
			this.background.setTitles('Мария:\n\nОтлично!', 'Теперь Вы узнали, что такое ценности Росатома. Ценности – это опора при принятии решений в ситуации неопределенности. Руководствуясь ими, мы быстрее  и эффективнее достигаем результата.\n\nДействуя в соответствии с духом ценностей, во всех рабочих ситуациях можно быть уверенным в правильности принимаемого решения. Ценности помогают дивизиону формировать  культуру результата.');
			this.background.hideButton('all');
			this.background.topTitle('title1', 36);
			this.background.topTitle('title2', 124);
			this.background.setTitleFormat('title2', FontManager.instance.textFormat(17, Common.GRAY, 'light'));
			this.background.top=75;
			this.background.left=375;
			this.background.round(60, 60);
			
			var kluv:SpriteEx=new SpriteEx();
			kluv.graphics.beginFill(0xffffff, 0.95);
			kluv.graphics.moveTo(0, 0);
			kluv.graphics.lineTo(-50, 20);
			kluv.graphics.lineTo(0, 40);
			kluv.graphics.lineTo(0, 0);
			kluv.graphics.endFill();
			this.background.addChild(kluv);
			kluv.y=180;
			
			var button:Button=new Button();
			button.left=54;
			button.bottom=40;
			button.width=69;
			button.height=28;
			button.label='Далее';
			button.skinClass=common.ButtonSkin;
			this.background.addChild(button);
			button.addEventListener(MouseEvent.CLICK, this.onMouseClick);
			
			var image:Bitmap=new persClass() as Bitmap;
			image.smoothing=true;
			this.pers=new Image();
			this.pers.source=image.bitmapData;
			this.pers.left=8;
			this.pers.bottom=0;
			this.pers.mouseEnabled=false;
			this.addChild(this.pers);
		}
		
		private function onMouseClick(event:MouseEvent):void
		{
			super.complete();
		}
	}
}