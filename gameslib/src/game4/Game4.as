package game4
{
	import flash.display.Sprite;
	import flash.events.Event;
	
	import game4.frame1.Frame1;
	import game4.frame2.Frame2;
	import game4.frame3.Frame3;
	import game4.frame4.Frame4;
	import game4.frame5.Frame5;
	import game4.frame_cube.NeoCube;
	import game4.memory.GameMemory;
	
	import ru.flexdev.ui.SpriteEx;
	
	public class Game4 extends Sprite
	{
		public static const WIDTH:int=1024;
		public static const HEIGHT:int=680;
		
		private var holder:SpriteEx;

		private var frames:Vector.<Frame>;
		private var currentFrame:Frame;
		
		private var counter:int=-1;
		
		public function Game4()
		{
			super();
			this.counter=-1;
			
			this.holder=new SpriteEx();
			this.holder.width=WIDTH;
			this.holder.height=HEIGHT;
			this.addChild(this.holder);
			this.holder.clipped=true;
			
			this.frames=new Vector.<Frame>();
			
			var frame:Frame;
			
			frame=new Frame1(WIDTH, HEIGHT);
			frame.bottom=0;
			this.frames.push(frame);
			
			frame=new NeoCube(WIDTH, HEIGHT);
			frame.bottom=0;
			this.frames.push(frame);
			
			frame=new Frame2(WIDTH, HEIGHT);
			frame.bottom=0;
			this.frames.push(frame);
			
			frame=new Frame3(WIDTH, HEIGHT);
			frame.bottom=0;
			this.frames.push(frame);
			
			frame=new Frame4(WIDTH, HEIGHT);
			frame.bottom=0;
			this.frames.push(frame);
			
			frame=new GameMemory(WIDTH, HEIGHT);
			frame.bottom=0;
			this.frames.push(frame);
			
			frame=new Frame5(WIDTH, HEIGHT);
			frame.bottom=0;
			this.frames.push(frame);
			
			this.nextFrame();
			this.addEventListener('frame_complete', this.onFrameComplete);
		}
		
		private function nextFrame():void
		{
			this.counter++;
			if(this.counter>=this.frames.length)
			{
				this.gameComplete();
				return;
			}
			if(this.currentFrame!=null)this.currentFrame.hide();
			
			this.currentFrame=this.frames[this.counter];
			this.holder.addChild(this.currentFrame);
			this.currentFrame.show(this.counter==0);
		}
		
		private function onFrameComplete(event:Event):void
		{
			this.nextFrame();
		}
		
		private function gameComplete():void
		{
			this.dispatchEvent(new Event(Event.COMPLETE));
		}
	}
}

