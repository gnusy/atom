package game4.frame2.star
{
	import ru.flexdev.ui.SpriteEx;
	
	public class Bars extends SpriteEx
	{
		private var maxCount:int;

		public function Bars(maxCount:int)
		{
			super();
			this.maxCount=maxCount;
			this.width=maxCount*6-1;
			
			this.height=3;
			this.count=0;
		}
		
		public function set count(value:int):void
		{
			this.graphics.clear();
			for(var i:int=0;i<value;i++)
			{
				this.graphics.beginFill(0x6661a1);
				this.graphics.drawRect(6*i, 0, 5, 3);
				this.graphics.endFill();
			}
			
			for(i=value;i<maxCount;i++)
			{
				this.graphics.beginFill(0xcdcccb);
				this.graphics.drawRect(6*i, 0, 5, 3);
				this.graphics.endFill();
			}
		}
	}
}