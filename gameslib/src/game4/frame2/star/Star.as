package game4.frame2.star
{
	import caurina.transitions.Tweener;
	
	import common.Common;
	import common.ToolTip;
	
	import flash.display.Bitmap;
	import flash.events.MouseEvent;
	import flash.filters.GlowFilter;
	import flash.geom.Rectangle;
	
	import ru.flexdev.ui.Rect;
	import ru.flexdev.ui.SpriteEx;
	
	public class Star extends Rect
	{
		private var starsHolder:SpriteEx;
		private var image_gray:Bitmap;
		private var image_orange:Bitmap;
		private var image_half:Bitmap;
		private var maxCount:int;
		private var tip:String;
		public var pack:String;
		private var count:int=0;
		private var bars:Bars;
		private var toolTip:ToolTip;
		private var glow:GlowFilter;
		
		public function Star(gray:Class, orange:Class, half:Class, maxCount:int, tip:String, pack:String)
		{
			super();
			this.stroke(0x8c8c8c);
			this.maxCount=maxCount;
			this.tip=tip;
			this.pack=pack;
			
			this.starsHolder=new SpriteEx();
			this.addChild(this.starsHolder);
			this.starsHolder.top=10;
			this.starsHolder.horizontalCenter=0;
			
			this.image_gray=new gray() as Bitmap;
			this.image_gray.smoothing=true;
			this.starsHolder.addChild(this.image_gray);
			
			this.image_orange=new orange() as Bitmap;
			this.image_orange.smoothing=true;
			this.starsHolder.addChild(this.image_orange);
			this.image_orange.alpha=0;

			this.image_half=new half() as Bitmap;
			this.image_half.smoothing=true;
			this.starsHolder.addChild(this.image_half);
			this.image_half.alpha=0;
			
			this.starsHolder.width=this.image_gray.width;
			this.starsHolder.height=this.image_gray.height;
			
			this.width=150;
			this.height=150;
			
			
			this.bars=new Bars(maxCount);
			this.bars.horizontalCenter=0;
			this.bars.bottom=7;
			this.addChild(this.bars);
			
			
			this.toolTip=new ToolTip();
			this.addChild(this.toolTip);
			
			this.glow=new GlowFilter(Common.ORANGE, 0.25, 16, 16);
			
			this.addEventListener(MouseEvent.MOUSE_MOVE, this.onRoll);
			this.addEventListener(MouseEvent.MOUSE_OUT, this.onRoll);
		}
		
		public function addItem():Boolean
		{
			if(this.count==this.maxCount)return false;
			
			this.count++;
			this.bars.count=this.count;
			if(this.count==1)
			{
				Tweener.addTween(this.image_gray, {alpha:0, time:3});
				Tweener.addTween(this.image_half, {alpha:1, time:3});
			}
			
			if(this.count==this.maxCount)
			{
				Tweener.addTween(this.image_half, {alpha:0, time:3});
				Tweener.addTween(this.image_orange, {alpha:1, time:3});
			}
			
			return true;
		}
		
		public function onRoll(event:MouseEvent):void
		{
			switch(event.type)
			{
				case MouseEvent.MOUSE_MOVE:
					//if(LittleStar.DRAGGING==false)this.toolTip.show(this.tip, new Point(this.width/2, 21));
					//else this.toolTip.hide();
					this.starsHolder.filters=[this.glow];
					break;
				
				case MouseEvent.MOUSE_OUT:
					this.starsHolder.filters=[];
					//this.toolTip.hide();
					break;
			}
		}
		
		private function get rect():Rectangle
		{
			return new Rectangle(this.x, this.y, this.width, this.height);
		}
		
		public function intersect(rect:Rectangle):Boolean
		{
			return this.rect.intersects(rect);
		}
		
		public function get correct():Boolean
		{
			return this.count==this.maxCount;
		}
		
	}
}