package game4.frame2.list
{
	import flash.display.Bitmap;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import common.Common;
	
	import fontmanager.FontManager;
	
	import ru.flexdev.events.EventEx;
	import ru.flexdev.ui.SpriteEx;
	
	public class ListItem extends SpriteEx
	{
		[Embed(source="assets/arrow_gray.png")]private var arrow_grayClass:Class;
		[Embed(source="assets/arrow_orange.png")]private var arrow_orangeClass:Class;
		
		public var pack:String='';
		private var selected:Boolean=false;
		
		private var title:TextField;
		private var arrow_gray:Bitmap;
		private var arrow_orange:Bitmap;
		
		private var _status:String='normal';
		private var _errors:int=0;
		
		public function ListItem(data:Object)
		{
			super();
			this.width=425;
			this.height=8;
			this.buttonMode=true;
			this.pack=data.pack;
			
			this.arrow_gray=new this.arrow_grayClass() as Bitmap;
			this.arrow_gray.smoothing=true;
			this.addChild(this.arrow_gray);
			
			this.arrow_orange=new this.arrow_orangeClass() as Bitmap;
			this.arrow_orange.smoothing=true;
			this.addChild(this.arrow_orange);
			
			this.title=FontManager.instance.textField;
			var frmt:TextFormat=FontManager.instance.textFormat(17, Common.GRAY, 'regular');
			this.title.defaultTextFormat=frmt;
			this.title.wordWrap=true;
			this.title.width=430;
			this.title.x=12;
			this.title.y=-8;
			this.title.text=data.text;
			this.title.height=this.title.textHeight+4;
			this.addChild(this.title);
			
			this.height=this.title.height;
			this.status='normal';
	
			this.addEventListener(MouseEvent.ROLL_OVER, this.onRoll);
			this.addEventListener(MouseEvent.ROLL_OUT, this.onRoll);
			this.addEventListener(MouseEvent.MOUSE_DOWN, this.onStartDrag);
		}
		
		private function onRoll(event:MouseEvent):void
		{
			if(this.status=='passed')return;
			
			switch(event.type)
			{
				case MouseEvent.ROLL_OVER:
					this.status='over';
					break;
				
				case MouseEvent.ROLL_OUT:
					this.status='normal';
					break;
			}
		}
		
		private function onStartDrag(event:MouseEvent):void
		{
			if(this.status=='passed')return;
			this.status='passed';
			
			this.dispatchEvent(new EventEx('start_drag', this, true));
		}
		
		private function update():void
		{
			var frmt:TextFormat=this.title.getTextFormat();
			if(this.status=='over')
			{
				this.arrow_gray.visible=false;
				this.arrow_orange.visible=true;
				frmt.color=Common.ORANGE;
				this.buttonMode=true;
			}
			else if(this.status=='normal')
			{
				this.arrow_gray.visible=true;
				this.arrow_orange.visible=false;
				frmt.color=Common.GRAY;
				this.buttonMode=true;
			}
			else if(this.status=='passed')
			{
				this.arrow_gray.visible=true;
				this.arrow_orange.visible=false;
				frmt.color=0xcccccc;
				this.buttonMode=false;
			}
			
			this.title.setTextFormat(frmt);
		}
		
		public function set status(value:String):void
		{
			this._status=value;
			this.update();
		}
		
		public function get status():String
		{
			return this._status;
		}
		
		public function get correct():Boolean
		{
			return true;
		}

		public function set errors(value:int):void
		{
			this._errors=value;
			if(this._errors>2)this._errors=2;
		}
	
		public function get errors():int
		{
			return this._errors;
		}
		
		public function get score():int
		{
			return 2-this._errors;
		}
	}
}