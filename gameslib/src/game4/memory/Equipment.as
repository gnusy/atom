package game4.memory
{
	import by.blooddy.crypto.serialization.JSON;
	
	import caurina.transitions.Tweener;
	
	import common.Common;
	
	import flash.display.Bitmap;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.text.TextField;
	import flash.utils.ByteArray;
	
	import fontmanager.FontManager;
	
	import ru.flexdev.events.EventEx;
	import ru.flexdev.ui.Button;
	import ru.flexdev.ui.Rect;
	import ru.flexdev.ui.ScrollerEx;
	import ru.flexdev.ui.SpriteEx;
	
	public class Equipment extends Rect
	{
		[Embed(source="assets/equipment.json", mimeType="application/octet-stream")]private var data_json:Class;
		
		private var exit:Button;
		
		private var wnd:Rect;
		private var data:Array;
		
		public function Equipment(width:int, height:int)
		{
			super();
			this.visible=false;
			this.alpha=0;
						
			this.width=width;
			this.height=height;
			this.fill(0x5e5e5d, 0.34);
			
			this.wnd=new Rect();
			this.addChild(this.wnd);
			this.wnd.width=640;
			this.wnd.height=445;
			this.wnd.verticalCenter=0;
			this.wnd.horizontalCenter=0;
			this.wnd.fill(0xffffff).round(14, 16);
			
			var shadow:DropShadowFilter=new DropShadowFilter();
			shadow.color=0x3e3e3e;
			shadow.alpha=0.5;
			shadow.distance=1;
			shadow.blurX=shadow.blurY=16;
			this.wnd.filters=[shadow];
			
			var title:TextField=FontManager.instance.textField;
			title.defaultTextFormat=FontManager.instance.textFormat(21, Common.GRAY, 'bold');
			title.text='Описание оборудования';
			title.y=26;
			title.width=title.textWidth+5;
			title.x=(this.wnd.width-title.width)/2;
			this.wnd.addChild(title);
			
			
			this.exit=new Button();
			this.exit.label='Вернуться в игру';
			this.exit.width=175;
			this.exit.height=35;
			this.exit.skinClass=ButtonSkin;
			this.exit.bottom=16;
			this.exit.horizontalCenter=0;
			
			
			this.wnd.addChild(this.exit);
			this.exit.addEventListener(MouseEvent.CLICK, this.onMouseClick);
			
			var scroller:ScrollerEx=new ScrollerEx();
			scroller.x=33;
			scroller.y=70;
			scroller.width=580;
			scroller.height=300;
			this.wnd.addChild(scroller);
			
			var ba:ByteArray=new this.data_json() as ByteArray;
			ba.position=0;
			var json:String=ba.readUTFBytes(ba.length);
			this.data=JSON.decode(json)['texts'] as Array;
			
			var last_item:EquipmentItem=null;
			for(var i:int=0;i<this.data.length;i++)
			{
				var ei:EquipmentItem=new EquipmentItem(i+1, this.data[i]);
				scroller.addChild(ei);
				if(last_item!=null)ei.y=last_item.y+last_item.height+24;
				last_item=ei;
			}
		}
		
		public function onMouseClick(event:MouseEvent):void
		{
			this.dispatchEvent(new EventEx(Event.COMPLETE));
		}
		
		public function show(now:Boolean=false):void
		{
			this.visible=true;
			if(now)this.alpha=1;
			else Tweener.addTween(this, {alpha:1, time:0.5, transition:'linear'});
		}
		
		public function hide():void
		{
			Tweener.addTween(this, {alpha:0, time:0.5, transition:'linear', onComplete:this.onHideComplete});
		}
		
		private function onHideComplete():void
		{
			this.visible=false;
		}
	}
}