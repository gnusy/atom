package game4.memory
{
	import common.Common;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	import fontmanager.FontManager;
	
	import mx.states.OverrideBase;
	
	import ru.flexdev.events.EventEx;
	import ru.flexdev.ui.SpriteEx;
	
	public class RadioButton extends SpriteEx
	{
		private var _label:TextField;
		private var _selected:Boolean;
		
		public function RadioButton()
		{
			super();
			this._label=FontManager.instance.textField;
			var frmt:TextFormat=FontManager.instance.textFormat(21, Common.GRAY, 'regular');
			this._label.defaultTextFormat=frmt;
			this.addChild(this._label);
			this.buttonMode=true;
			
			if(this.parent==null)this.addEventListener(Event.ADDED_TO_STAGE, this.onAddedToStage);
			else this.parent.addEventListener('unselect', this.onUnSelect);
			
			this.addEventListener(MouseEvent.CLICK, this.onClick);
			
		}
		
		private function onAddedToStage(event:Event):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, this.onAddedToStage);
			this.parent.addEventListener('unselect', this.onUnSelect);
		}
		
		
		public function set label(value:String):void
		{
			this._label.text=value;
			this._label.height=this._label.textHeight+5;
			this._label.width=this._label.textWidth+5;
			this.resize();
		}
		
		override protected function resize():void
		{
			super.resize();
			this._label.x=(this.width-this._label.width)/2;
			this._label.y=(this.height-this._label.height)/2;
		}
		
		private function onClick(event:MouseEvent):void
		{
			this._selected=true;
			this.update();
			this.parent.dispatchEvent(new EventEx('unselect', this));
		
		}
		
		private function onUnSelect(event:EventEx):void
		{
			if(event.data==this)return;
			this._selected=false;
			this.update();
		}
		
		private function update():void
		{
			var frmt:TextFormat=FontManager.instance.textFormat(21, this._selected?Common.ORANGE:Common.GRAY, 'regular');
			this._label.setTextFormat(frmt);
		}
		
		public function get selected():Boolean
		{
			return this._selected;
		}
		
		public function set selected(value:Boolean):void
		{
			this._selected=value;
			this.update();
		}
	}
}