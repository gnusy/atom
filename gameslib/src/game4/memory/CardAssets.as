package game4.memory
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;

	public class CardAssets
	{
		[Embed(source="assets/memo-01.png")]private var memo01:Class;
		[Embed(source="assets/memo-02.png")]private var memo02:Class;
		[Embed(source="assets/memo-03.png")]private var memo03:Class;
		[Embed(source="assets/memo-04.png")]private var memo04:Class;
		[Embed(source="assets/memo-05.png")]private var memo05:Class;
		[Embed(source="assets/memo-06.png")]private var memo06:Class;
		[Embed(source="assets/memo-07.png")]private var memo07:Class;
		[Embed(source="assets/memo-08.png")]private var memo08:Class;
		[Embed(source="assets/memo-09.png")]private var memo09:Class;
		[Embed(source="assets/memo-10.png")]private var memo10:Class;
		[Embed(source="assets/memo-11.png")]private var memo11:Class;
		[Embed(source="assets/memo-12.png")]private var memo12:Class;
		[Embed(source="assets/memo-13.png")]private var memo13:Class;
		[Embed(source="assets/memo-14.png")]private var memo14:Class;
		[Embed(source="assets/memo-15.png")]private var memo15:Class;
		[Embed(source="assets/memo-16.png")]private var memo16:Class;
		[Embed(source="assets/memo-17.png")]private var memo17:Class;
		[Embed(source="assets/memo-18.png")]private var memo18:Class;
		[Embed(source="assets/memo-19.png")]private var memo19:Class;
		[Embed(source="assets/memo-20.png")]private var memo20:Class;
		
		[Embed(source="assets/memo-default.png")]private var memo_default:Class;
		
		private static var _instance:CardAssets;
		private var cards:Array=[memo_default, memo01, memo02, memo03, memo04, memo05, memo06, memo07, memo08, memo09, memo10, memo11, memo12, memo13, memo14, memo15, memo16, memo17, memo18, memo19, memo20];
		
		public static function get instance():CardAssets
		{
			if(_instance==null)_instance=new CardAssets();
			return _instance;
		}
		
		public function CardAssets()
		{
			for(var i:int=0;i<this.cards.length;i++)
			{
				var img:Bitmap=new this.cards[i]() as Bitmap;
				this.cards[i]=img.bitmapData;
			}
		}
		
		public function getCard(index:int):BitmapData
		{
			return this.cards[index];
		}
	}
}