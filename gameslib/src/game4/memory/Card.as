package game4.memory
{
	import caurina.transitions.Tweener;
	
	import flash.display.Bitmap;
	import flash.events.MouseEvent;
	import flash.geom.PerspectiveProjection;
	import flash.geom.Point;
	
	import ru.flexdev.events.EventEx;
	import ru.flexdev.ui.SpriteEx;
	
	public class Card extends SpriteEx
	{
		private var front:Bitmap;
		private var back:Bitmap;
		private var holder:SpriteEx;
		public var flipped:Boolean=false;
		public var number:int;
		
		private var _fixed:Boolean=false;
		
		public function Card(index:int, level:int=1)
		{
			super();
			this.number=index;
			this.width=153;
			this.height=154;
			
			if(level==1)this.scaleX=this.scaleY=85/153;
			else this.scaleX=this.scaleY=70/153;
			
			var pp:PerspectiveProjection=new PerspectiveProjection();
			pp.projectionCenter=new Point(this.width/2, this.height/2);
			this.transform.perspectiveProjection=pp;
			
			this.front=new Bitmap(CardAssets.instance.getCard(index).clone());
			this.back=new Bitmap(CardAssets.instance.getCard(0).clone());
			this.front.smoothing=true;
			this.back.smoothing=true;
			
			this.front.scaleX=-1;
			
			
			this.holder=new SpriteEx();
			this.addChild(this.holder);
			this.holder.x=this.width/2;
			this.holder.rotationY=0.01;
			this.holder.rotationY=0;
			
			
			this.holder.addChild(this.front);
			this.holder.addChild(this.back);
			
			this.front.x=this.width/2;
			this.back.x=-this.width/2;
			
			this.front.visible=false;
			this.addEventListener(MouseEvent.CLICK, this.onClick);
			this.buttonMode=true;
		}
		
		private function onClick(event:MouseEvent):void
		{
			if(this.mouseEnabled==false)return;
			if(this.flipped==true)return;
			this.mouseEnabled=false;
			if(this.flipped==false)this.flip();
			else this.unflip();
		}
		
		private function flip():void
		{
			Tweener.addTween(this.holder, {rotationY:180, time:0.25, transition:'linear', rounded:false, onUpdate:this.onFlipUpdate, onComplete:this.onFlipComplete});
		}

		public function unflip():void
		{
			Tweener.addTween(this.holder, {rotationY:0, time:0.25, transition:'linear', rounded:false, onUpdate:this.onFlipUpdate, onComplete:this.onFlipComplete});
		}
	
		private function onFlipUpdate():void
		{
			if(this.holder.rotationY>=90 && this.flipped==false)
			{
				this.front.visible=true;
				this.back.visible=false;
			}
			
			if(this.holder.rotationY<=90 && this.flipped==true)
			{
				this.front.visible=false;
				this.back.visible=true;
			}
		}
		
		private function onFlipComplete():void
		{
			this.flipped=!this.flipped;
			this.mouseEnabled=true;
			
			if(this.flipped==true)this.dispatchEvent(new EventEx('flip', this, true));
			
		}
		
		public function get scaledWidth():Number
		{
			return this.width*this.scaleX;
		}
		
		public function get scaledHeight():Number
		{
			return this.height*this.scaleY;
		}
		
		public function get fixed():Boolean
		{
			return this._fixed;
		}
		
		public function set fixed(value:Boolean):void
		{
			this._fixed=value;
			if(value==true)
			{
				this.mouseEnabled=false;
				this.buttonMode=false;
			}
		}
	}
}