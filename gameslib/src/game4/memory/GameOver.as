package game4.memory
{
	import caurina.transitions.Tweener;
	
	import common.Common;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.text.TextField;
	
	import fontmanager.FontManager;
	
	import ru.flexdev.events.EventEx;
	import ru.flexdev.ui.Button;
	import ru.flexdev.ui.Rect;
	import ru.flexdev.ui.SpriteEx;
	
	public class GameOver extends Rect
	{
		private var btnBegin:Button;
		private var btnExit:Button;
		private var currentTime:TextField;
		private var bestTime:TextField;
		private var wnd:Rect;
		
		public function GameOver(width:int, height:int)
		{
			super();
			this.visible=false;
			this.alpha=0;
			
			this.width=width;
			this.height=height;
			this.fill(0x5e5e5d, 0.34);
			
			this.wnd=new Rect();
			this.addChild(this.wnd);
			this.wnd.width=380;
			this.wnd.height=430;
			this.wnd.top=160;
			this.wnd.horizontalCenter=0;
			this.wnd.fill(0xffffff).round(14, 16);
			
			var shadow:DropShadowFilter=new DropShadowFilter();
			shadow.color=0x3e3e3e;
			shadow.alpha=0.5;
			shadow.distance=1;
			shadow.blurX=shadow.blurY=16;
			this.wnd.filters=[shadow];
			
			var title:TextField=FontManager.instance.textField;
			title.defaultTextFormat=FontManager.instance.textFormat(21, Common.GRAY, 'bold');
			title.text='Конец игры';
			title.y=34;
			title.width=title.textWidth+5;
			title.x=(this.wnd.width-title.width)/2;
			this.wnd.addChild(title);
			
			
			var text:TextField=FontManager.instance.textField;
			text.defaultTextFormat=FontManager.instance.textFormat(17, Common.GRAY, 'light');
			text.text='Время игры';
			text.y=76;
			text.width=text.textWidth+5;
			text.x=(this.wnd.width-text.width)/2;
			this.wnd.addChild(text);
			
			text=FontManager.instance.textField;
			text.defaultTextFormat=FontManager.instance.textFormat(17, Common.GRAY, 'light');
			text.text='Лучшее время';
			text.y=130;
			text.width=text.textWidth+5;
			text.x=(this.wnd.width-text.width)/2;
			this.wnd.addChild(text);
			
			text=FontManager.instance.textField;
			text.defaultTextFormat=FontManager.instance.textFormat(17, Common.GRAY, 'light');
			text.text='Испытай себя и побей свой рекорд';
			text.y=232;
			text.width=text.textWidth+5;
			text.x=(this.wnd.width-text.width)/2;
			this.wnd.addChild(text);
			
			this.currentTime=FontManager.instance.textField;
			this.currentTime.defaultTextFormat=FontManager.instance.textFormat(17, Common.GRAY, 'light');
			this.currentTime.text='00:00';
			this.currentTime.y=99;
			this.currentTime.width=this.currentTime.textWidth+5;
			this.currentTime.x=(this.wnd.width-this.currentTime.width)/2;
			this.wnd.addChild(this.currentTime);
			
			this.bestTime=FontManager.instance.textField;
			this.bestTime.defaultTextFormat=FontManager.instance.textFormat(44, Common.ORANGE, 'bold');
			this.bestTime.text='00:00';
			this.bestTime.y=157;
			this.bestTime.width=this.bestTime.textWidth+5;
			this.bestTime.x=(this.wnd.width-this.bestTime.width)/2;
			this.wnd.addChild(this.bestTime);
			
			
			
			
			this.btnBegin=new Button();
			this.wnd.addChild(this.btnBegin);
			this.btnBegin.horizontalCenter=0;
			this.btnBegin.bottom=85;
			this.btnBegin.label='Попробовать ещё раз';
			this.btnBegin.width=225;
			this.btnBegin.height=40;
			this.btnBegin.skinClass=ButtonSkin;
			this.btnBegin.addEventListener(MouseEvent.CLICK, this.onBeginClick);
			
			this.btnExit=new Button();
			this.wnd.addChild(this.btnExit);
			this.btnExit.horizontalCenter=0;
			this.btnExit.bottom=35;
			this.btnExit.label='Выйти из игры';
			this.btnExit.width=225;
			this.btnExit.height=40;
			this.btnExit.skinClass=ButtonSkin;
			this.btnExit.addEventListener(MouseEvent.CLICK, this.onExitClick);
			
		}
		
		public function times(curent:String, best:String):void
		{
			this.currentTime.text=curent;
			this.currentTime.width=this.currentTime.textWidth+5;
			this.currentTime.x=(this.wnd.width-this.currentTime.width)/2;
			
			this.bestTime.text=best;
			this.bestTime.width=this.bestTime.textWidth+5;
			this.bestTime.x=(this.wnd.width-this.bestTime.width)/2;
		}
		
		
		private function onBeginClick(event:MouseEvent):void
		{
			this.dispatchEvent(new EventEx(Event.COMPLETE, false));
		}
		
		private function onExitClick(event:MouseEvent):void
		{
			this.dispatchEvent(new EventEx(Event.COMPLETE, true));
		}
		
		public function show(now:Boolean=false):void
		{
			this.visible=true;
			if(now)this.alpha=1;
			else Tweener.addTween(this, {alpha:1, time:0.5, transition:'linear'});
		}
		
		public function hide():void
		{
			Tweener.addTween(this, {alpha:0, time:0.5, transition:'linear', onComplete:this.onHideComplete});
		}
		
		private function onHideComplete():void
		{
			this.visible=false;
		}
	}
}