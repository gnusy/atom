package content
{
	import common.Common;
	
	import flash.display.Bitmap;
	import flash.events.DataEvent;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import fontmanager.FontManager;
	
	import ru.flexdev.ui.SpriteEx;
	
	public class ContentItem extends SpriteEx
	{
		[Embed(source="assets/arrow_gray.png")]private var arrow_grayClass:Class;
		
		private var sub:Boolean;
		
		private var title:TextField;
		private var index:String;
		private var arrow_gray:Bitmap;
		
		public function ContentItem(data:Object)
		{
			super();
			this.width=425;
			this.height=8;
			
			this.sub=data['sub'];
			this.index=data['index'];
			
			this.arrow_gray=new this.arrow_grayClass() as Bitmap;
			this.arrow_gray.smoothing=true;
			this.addChild(this.arrow_gray);
			this.arrow_gray.visible=false;

			
			this.title=FontManager.instance.textField;
			var frmt:TextFormat=FontManager.instance.textFormat(15, 0xc5c5c6, sub?'light':'bold');
			this.title.defaultTextFormat=frmt;
			this.title.width=700;
			this.title.x=sub?17:0;
			this.title.y=-6;
			this.title.text=data['name'];
			this.title.height=this.title.textHeight+4;
			this.title.width=this.title.textWidth+4;
			this.height=this.title.height;
			this.addChild(this.title);
			
			this.arrow_gray.x=this.title.x-17;
		}
		
		private function onClick(event:MouseEvent):void
		{
			this.dispatchEvent(new DataEvent('selectItem', true, false, this.index));
		}
		
		public function set active(value:Boolean):void
		{
			var frmt:TextFormat=this.title.getTextFormat();
			if(this.index=='-1')
			{
				if(value)frmt.color=Common.GRAY;
				else frmt.color=0xc5c5c6;
				
				this.title.setTextFormat(frmt);
				return;
			}
			
			
			this.buttonMode=value;
			
			if(value)
			{
				frmt.color=Common.GRAY;
				this.addEventListener(MouseEvent.CLICK, this.onClick);
			}
			else
			{
				frmt.color=0xc5c5c6;
			}
			
			this.title.setTextFormat(frmt);
		}

		public function set current(value:Boolean):void
		{
			this.arrow_gray.visible=value;
		}

	}
	
}