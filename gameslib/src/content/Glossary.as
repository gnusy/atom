package content
{
	import by.blooddy.crypto.serialization.JSON;
	
	import caurina.transitions.Tweener;
	
	import common.Common;
	
	import flash.display.Bitmap;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.utils.ByteArray;
	
	import fontmanager.FontManager;
	
	import ru.flexdev.ui.Rect;
	import ru.flexdev.ui.ScrollerEx;
	import ru.flexdev.ui.SpriteEx;
	
	public class Glossary extends Rect
	{
		[Embed(source="assets/cross.png")]private var crossClass:Class;
		[Embed(source="assets/data.json", mimeType="application/octet-stream")]private var data_json:Class;
		
		private var data:Array;
		private var cross:SpriteEx;
		
		public function Glossary()
		{
			super();
			this.visible=false;
			this.alpha=0;
			this.bounds(0, 0, 0, 0);
			this.fill(0xffffff);
			
			var ba:ByteArray=new this.data_json() as ByteArray;
			ba.position=0;
			var json:String=ba.readUTFBytes(ba.length);
			this.data=JSON.decode(json)['items'] as Array;
			
			var title:TextField=FontManager.instance.textField;
			var frmt:TextFormat=FontManager.instance.textFormat(20, Common.GRAY, 'bold');
			title.defaultTextFormat=frmt;
			title.width=740;
			title.wordWrap=true;
			title.x=50;
			title.y=46;
			this.addChild(title);
			title.text='Глоссарий';
			
			var scroller:ScrollerEx=new ScrollerEx();
			scroller.width=845;
			scroller.height=430;
			scroller.y=95;
			this.addChild(scroller);
			
			var lastItem:GlossaryItem;
			for(var i:int=0;i<this.data.length;i++)
			{
				var item:GlossaryItem=new GlossaryItem(this.data[i]);
				scroller.addChild(item);
				if(lastItem!=null)item.y=lastItem.y+lastItem.height+16;
				lastItem=item;
			}
			
			this.cross=new SpriteEx();
			var cross_image:Bitmap=new this.crossClass() as Bitmap;
			cross_image.smoothing=true;
			this.cross.addChild(cross_image);
			this.cross.width=cross_image.width;
			this.cross.height=cross_image.height;
			this.cross.buttonMode=true;
			this.cross.right=22;
			this.cross.top=21;
			
			
			this.addChild(this.cross);
			this.cross.addEventListener(MouseEvent.CLICK, this.onCrossClick);
		}
		
		private function onCrossClick(event:MouseEvent):void
		{
			this.hide();
		}
		
		public function show():void
		{
			this.visible=true;
			this.parent.setChildIndex(this, this.parent.numChildren-1);
			Tweener.addTween(this, {alpha:1, time:0.5, transition:'linear'});

		}
		
		public function hide():void
		{
			Tweener.addTween(this, {alpha:0, time:0.5, transition:'linear', onComplete:this.onHideComplete});
		}
		
		private function onHideComplete():void
		{
			this.visible=false;
		}
	}
}