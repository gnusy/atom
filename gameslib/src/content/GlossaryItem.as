package content
{
	import common.Common;
	
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import fontmanager.FontManager;
	
	import ru.flexdev.ui.SpriteEx;
	
	public class GlossaryItem extends SpriteEx
	{
		private var txtName:TextField;
		private var txtTitle:TextField;
		
		
		public function GlossaryItem(data:Object)
		{
			super();
			
			this.txtName=FontManager.instance.textField;
			var frmt:TextFormat;
			if(data.hasOwnProperty('type')==true)frmt=FontManager.instance.textFormat(21, Common.ORANGE, 'bold');
			else frmt=FontManager.instance.textFormat(17, Common.GRAY, 'light');
			this.txtName.defaultTextFormat=frmt;
			this.txtName.wordWrap=true;
			this.txtName.width=640;
			this.txtName.x=50;
			this.txtName.y=-5;
			this.addChild(this.txtName);
			
			if(data.hasOwnProperty('type')==true)
			{
				this.txtName.text=data['name'];
			}
			else
			{
				this.txtName.text=data['name']+' — '+data['title'];
				this.txtName.setTextFormat(FontManager.instance.textFormat(17, Common.GRAY, 'regular'), 0, (data['name'] as String).length+2);
			}
			
			this.txtName.height=this.txtName.textHeight+4;
			//this.txtName.width=this.txtName.textWidth+4;

			this.height=this.txtName.height;

		}
	}
}