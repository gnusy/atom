package content
{
	import common.Background;
	import common.ButtonSkin;
	import common.Helper;
	
	import flash.display.Sprite;
	import flash.events.DataEvent;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.utils.setTimeout;
	
	import ru.flexdev.ui.Button;
	
	public class Content extends Sprite
	{
		public static const WIDTH:int=871;
		public static const HEIGHT:int=570;
		
		private var background:Background;
		
		private var items_data:Array=[
			{name:'Приветствие генерального директора', sub:false, index:'0'},
			{name:'Атомэнергомаш. География. Продукция и направления деятельности', sub:false, index:'1'},
			{name:'Предприятия:', sub:false, index:'-1'},
			{name:'АО НПО «ЦНИИТМАШ»', sub:true, index:'2'},
			{name:'АО «СНИИП»', sub:true, index:'3'},
			{name:'ПАО «ЗиО-Подольск»', sub:true, index:'4'},
			{name:'АО ОКБ «ГИДРОПРЕСС»', sub:true, index:'5'},
			{name:'АО «ЦКБМ»', sub:true, index:'6'},
			{name:'Филиал АО «АЭМ-технологии» «Петрозаводскмаш»', sub:true, index:'7'},
			{name:'Филиал АО «АЭМ-технологии» «Атоммаш» в г. Волгодонске', sub:true, index:'8'},
			{name:'АО «ОКБМ Африкантов»', sub:true, index:'9'},
			{name:'АО «СвердНИИХиммаш»', sub:true, index:'10'},
			{name:'Стратегия дивизиона и Госкорпорации', sub:false, index:'11'},
			{name:'Ценности', sub:false, index:'12'},
			{name:'Сотрудники дивизиона', sub:false, index:'13'},
			{name:'Итоговое задание', sub:false, index:'14'}
		];
		private var items:Vector.<ContentItem>;
		private var special_item:ContentItem;
		
		private var btn:Button;
		private var glossary:Glossary;
		
		public function Content()
		{
			super();
			this.background=new Background(WIDTH, HEIGHT);
			this.addChild(this.background);
			this.background.setTitles('Содержание курса', '').hideButton('toMenu').hideButton('toMap');
		
			this.items=new Vector.<ContentItem>();
			
			for(var i:int=0;i<this.items_data.length;i++)
			{
				var item:ContentItem=new ContentItem(this.items_data[i]);
				this.background.addChild(item);
				
				if(i!=2)this.items.push(item);
				else this.special_item=item;
				
				item.x=50;
				item.y=94+28*i;
			}
			
			this.btn=new Button();
			this.background.addChild(this.btn);
			this.btn.right=40;
			this.btn.bottom=27;
			this.btn.label='Глоссарий';
			this.btn.width=125;
			this.btn.height=30;
			this.btn.skinClass=common.ButtonSkin;
			this.btn.addEventListener(MouseEvent.CLICK, this.onGlossaryClick);
				
			this.glossary=new Glossary();
			this.background.addChild(this.glossary);
		}
		
		public function onGlossaryClick(event:MouseEvent):void
		{
			this.glossary.show();
		}
		
		
		private var _current:int=-1;
		public function set current(index:int):void
		{
			if(index==-1)return;
			for(var i:int=0;i<this.items.length;i++)
			{
				//this.items[i].active=false;
				this.items[i].current=false;
			}
			
			if(index>14)index=14;
			
			for(i=0;i<=index;i++)
			{
				this.items[i].active=true;
			}
			if(index>=2)this.special_item.active=true;
			
			this.items[index].current=true;
			this._current=index;
		}
		
		public function get current():int
		{
			return this._current;
		}
		
		
		public function center(parentWidht:int, parentHeight:int):void
		{
			this.x=(parentWidht-WIDTH)/2;
			this.y=(parentHeight-HEIGHT)/2;
		}

	}
}

