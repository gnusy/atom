package tutorial
{
	import caurina.transitions.Tweener;
	
	import common.Common;
	
	import flash.display.Bitmap;
	import flash.events.MouseEvent;
	
	import ru.flexdev.ui.SpriteEx;
	
	public class Screen extends SpriteEx
	{
		[Embed(source="assets/screen1.png")]private var screen1:Class;
		[Embed(source="assets/screen2.png")]private var screen2:Class;
		[Embed(source="assets/screen3.png")]private var screen3:Class;
		[Embed(source="assets/screen4.png")]private var screen4:Class;
		[Embed(source="assets/screen5.png")]private var screen5:Class;
		[Embed(source="assets/screen6.png")]private var screen6:Class;
		[Embed(source="assets/screen7.png")]private var screen7:Class;
		[Embed(source="assets/screen8.png")]private var screen8:Class;
		[Embed(source="assets/screen9.png")]private var screen9:Class;
		
		[Embed(source="assets/screen10.png")]private var screen10:Class;
		[Embed(source="assets/screen11.png")]private var screen11:Class;
		[Embed(source="assets/screen12.png")]private var screen12:Class;
		[Embed(source="assets/screen13.png")]private var screen13:Class;
		[Embed(source="assets/screen14.png")]private var screen14:Class;

		
		private var screens:Array=[screen1, screen2, screen3, screen4, screen5, screen6, screen7, screen8, screen9, screen10, screen11, screen12, screen13, screen14];
		
		public function Screen()
		{
			super();
			this.width=1024;
			this.height=768;
			
			this.alpha=0;
			this.visible=false;
		}
		
		public function show(index:int):void
		{
			Common.removeChildren(this);
			var img:Bitmap=new this.screens[index]() as Bitmap;
			this.addChild(img);
			
			this.visible=true;
			Tweener.addTween(this, {alpha:1, time:0.25, transition:'linear', onComplete:this.onShow});
		}
		
		public function hide():void
		{
			this.removeEventListener(MouseEvent.CLICK, this.onMouseClick);
			Tweener.addTween(this, {alpha:0, time:0.25, transition:'linear', onComplete:this.onHide});
		}
		
		private function onShow():void
		{
			this.addEventListener(MouseEvent.CLICK, this.onMouseClick);
		}
	
		private function onHide():void
		{
			
			this.visible=false;
		}
		
		private function onMouseClick(event:MouseEvent):void
		{
			this.hide();
		}
	}
}