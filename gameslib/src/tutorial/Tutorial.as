package tutorial
{
	import common.ButtonSkin;
	
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import ru.flexdev.ui.Button;
	import ru.flexdev.ui.SpriteEx;
	
	public class Tutorial extends Sprite
	{
		[Embed(source="assets/cross.png")]private var crossClass:Class;
		[Embed(source="assets/background.png")]private var backClass:Class;
		public static const WIDTH:int=1024;
		public static const HEIGHT:int=768;
		
		private var items:Array=[
			new Rectangle(126, 268, 300, 24),
			new Rectangle(126, 313, 350, 21),
			new Rectangle(532, 313, 210, 18),
			new Rectangle(532, 266, 240, 24),
			new Rectangle(120, 568, 260, 54),
			new Rectangle(120, 634, 235, 21),
			new Rectangle(534, 633, 245, 25),
			new Rectangle(534, 598, 230, 26),
			new Rectangle(534, 567, 180, 23),
			
			new Rectangle(121, 454, 240, 45),
			new Rectangle(502, 459, 330, 24),
			new Rectangle(530, 490, 240, 28),
			new Rectangle(504, 391, 150, 36),
			new Rectangle(121, 392, 355, 44)
		];
		
		private var holder:Sprite;
		
		private var screen:Screen;
		
		private var btnClose:Button;
		private var cross:SpriteEx;
		
		public function Tutorial()
		{
			super();

			var img:Bitmap=new backClass() as Bitmap;
			img.smoothing=true;
			this.addChild(img);
			
			
			
			
			this.holder=new Sprite();
			this.holder.graphics.beginFill(0xffffff, 0.01);
			this.holder.graphics.drawRect(0, 0, WIDTH, HEIGHT);
			this.holder.graphics.endFill();
			this.addChild(this.holder);
			
			this.holder.addEventListener(MouseEvent.CLICK, this.onMouseClick);
			this.holder.addEventListener(MouseEvent.MOUSE_MOVE, this.onMouseMove);
			
			/*
			for(var i:int=0;i<this.items.length;i++)
			{
				var rect:Rectangle=this.items[i] as Rectangle;
				this.holder.graphics.lineStyle(0, 0xff0000);
				this.holder.graphics.drawRect(rect.x, rect.y, rect.width, rect.height);
				
			}
			*/

			
			
			
			this.btnClose=new Button();
			this.btnClose.skinClass=common.ButtonSkin;
			this.btnClose.label='Далее';
			this.btnClose.x=827;
			this.btnClose.y=670;
			this.btnClose.width=83;
			this.btnClose.height=29;
			this.btnClose.addEventListener(MouseEvent.CLICK, this.onCloseClick);
			this.btnClose.visible=false;
			this.addChild(this.btnClose);
			
			this.cross=new SpriteEx();
			var cross_image:Bitmap=new this.crossClass() as Bitmap;
			cross_image.smoothing=true;
			this.cross.addChild(cross_image);
			this.cross.width=cross_image.width;
			this.cross.height=cross_image.height;
			this.cross.buttonMode=true;
			this.cross.x=914;
			this.cross.y=171;
			this.addChild(this.cross);
			this.cross.addEventListener(MouseEvent.CLICK, this.onCloseClick);
			this.cross.visible=false;
			
			this.screen=new Screen();
			this.addChild(this.screen);
		}
		
		public function onCloseClick(event:MouseEvent):void
		{
			this.gameComplete();
		}
		
		public function onMouseClick(event:MouseEvent):void
		{
			for(var i:int=0;i<this.items.length;i++)
			{
				var rect:Rectangle=this.items[i] as Rectangle;
				if(rect.containsPoint(new Point(event.localX, event.localY)))
				{
					this.screen.show(i);
					this.btnClose.visible=true;
					this.cross.visible=true;
					return;
				}
			}
			
		}
		
		public function onMouseMove(event:MouseEvent):void
		{
			for(var i:int=0;i<this.items.length;i++)
			{
				var rect:Rectangle=this.items[i] as Rectangle;
				if(rect.containsPoint(new Point(event.localX, event.localY)))
				{
					this.holder.buttonMode=true;
					return;
				}
			}
			
			
			this.holder.buttonMode=false;
		}
		
		public function center(parentWidht:int, parentHeight:int):void
		{
			this.x=(parentWidht-WIDTH)/2;
			this.y=(parentHeight-HEIGHT)/2;
		}
		
		private function gameComplete():void
		{
			this.dispatchEvent(new Event('game_close'));
		}
	}
}

