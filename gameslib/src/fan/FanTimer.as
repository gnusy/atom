package fan
{
	import caurina.transitions.Tweener;
	
	import flash.filters.GlowFilter;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import fontmanager.FontManager;
	
	import ru.flexdev.ui.Rect;
	
	public class FanTimer extends Rect
	{
		private var tf:TextField;
		
		private var days:int;
		private var months:int;
		private var years:int;
		
		public var days_from:int=126;
		public var months_from:int=103;
		public var years_from:int=2026;
		
		public var days_to:int;
		public var months_to:int;
		public var years_to:int;
		
		public function FanTimer()
		{
			super();
			this.width=250;
			this.height=70;
			this.stroke(0xffffff, 1, 3).round(40, 40).fill(0x000000, 0.5);
			this.visible=false;
			this.alpha=0;
			
			this.tf=FontManager.instance.textField;
			var frmt:TextFormat=FontManager.instance.textFormat(46, 0xffffff, 'digital');
			this.tf.defaultTextFormat=frmt;
			this.addChild(this.tf);
			
			var dt:Date=new Date();
			this.days_to=dt.date;
			this.months_to=dt.month+1;
			this.years_to=dt.fullYear;
			
			
			var glow:GlowFilter=new GlowFilter(0xffffff, 0.25, 16, 16);
			this.filters=[glow];
		}
		
		public function set center(value:Point):void
		{
			this.x=value.x-this.width/2;
			this.y=value.y-this.height/2;
		}
		
		public function show():void
		{
			this.visible=true;
			Tweener.addTween(this, {alpha:1, time:0.25, transition:'linear'});
			start();
		}
		
		public function start():void
		{
			Tweener.addTween(this, {days_from:days_to, months_from:months_to, years_from:years_to, time:2.5, transition:'linear', onUpdate:this.onDateUpdate});
		}

		private function onDateUpdate():void
		{
			this.days=this.days_from-Math.floor(this.days_from/32)*32;
			this.months=this.months_from-Math.floor(this.months_from/13)*13;
			this.years=this.years_from;
			
			
			var ds:String=this.days.toString();
			if(ds.length==1)ds='0'+ds;
			var ms:String=this.months.toString();
			if(ms.length==1)ms='0'+ms;
			var ys:String=this.years.toString();
			
			
			this.time=ds+'/'+ms+'/'+ys;
		}
		
		private function set time(value:String):void
		{
			this.tf.text=value;
			this.tf.width=this.tf.textWidth+4;
			this.tf.height=this.tf.textHeight+2;
			this.tf.x=(this.width-this.tf.width)/2;
			this.tf.y=(this.height-this.tf.height)/2;
		}
	}
}