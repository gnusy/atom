package game9
{
	import common.Common;
	
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	import fontmanager.FontManager;
	
	import ru.flexdev.skins.button.BaseButtonSkin;
	import ru.flexdev.ui.Rect;
	
	public class ButtonSkin extends BaseButtonSkin
	{
		private var rect:Rect;
		private var tf:TextField;
		
		public function ButtonSkin()
		{
			super();
			
			this.rect=new Rect();
			this.rect.stroke(Common.GRAY).round(6, 6).bounds(0, 0, 0, 0);
			this.addChild(this.rect);
			
			this.tf=FontManager.instance.textField;
			this.tf.selectable=false;
			this.tf.mouseEnabled=false;
			var frmt:TextFormat=FontManager.instance.textFormat(17, Common.GRAY, 'bold');
			frmt.align=TextFormatAlign.CENTER;
			frmt.bold=true;
			this.tf.defaultTextFormat=frmt;
			this.tf.wordWrap=true;
			this.tf.width=this.width;
			this.tf.x=0;
			this.addChild(this.tf); 
			
			this.state='up';
		}
		
		override protected function updateState():void
		{
			switch(super.state)
			{
				case 'up':
					this.rect.fill(0xffffff, 0.01).stroke(Common.GRAY);
					this.updateLabelFont(true);
					break;
				
				case 'over':
					this.rect.fill(0xffffff, 0.01).stroke(Common.ORANGE);
					this.updateLabelFont(true);
					break;
				
				case 'down':
					this.rect.fill(0xffffff, 0.01).stroke(Common.ORANGE);
					this.updateLabelFont(true);
					break;
				
				case 'disabled':
					this.rect.fill(0xeeeeee, 1).stroke(0xaaaaaa);
					this.updateLabelFont(false);
					break;
			}
		}
		
		private function updateLabelFont(enabled:Boolean):void
		{
			var frmt:TextFormat=this.tf.getTextFormat();
			if(enabled==true)
			{
				if(super.state=='up')frmt.color=Common.GRAY;
				else frmt.color=Common.ORANGE;
					
			}
			else
			{
				frmt.color=0xaaaaaa;
			}
			this.tf.setTextFormat(frmt);
			this.tf.defaultTextFormat=frmt;
		}
		
		override protected function updateLabel():void
		{
			this.tf.text=super.label;
			this.tf.height=this.tf.textHeight+5;
			this.tf.y=(this.height-this.tf.height)/2+1;
		}
		
		override protected function resize():void
		{
			if(this.tf==null)return;
			this.tf.width=this.width;
			this.tf.height=this.tf.textHeight+5;
			this.tf.y=(this.height-this.tf.height)/2+1;
		}
		
		override public function dispose():void
		{
		
		}
	}
}