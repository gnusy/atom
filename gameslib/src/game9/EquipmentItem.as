package game9
{
	import common.Common;
	
	import flash.display.Bitmap;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import fontmanager.FontManager;
	
	import ru.flexdev.ui.SpriteEx;
	
	public class EquipmentItem extends SpriteEx
	{
		public function EquipmentItem(index:int, text:String)
		{
			super();
			this.width=100;
			this.height=95;
			
			var img:Bitmap=new Bitmap(CardAssets.instance.getCard(index).clone());
			img.smoothing=true;
			img.scaleX=img.scaleY=this.height/img.height;
			this.addChild(img);
			
			
			var txt:TextField=FontManager.instance.textField;
			var frmt:TextFormat=FontManager.instance.textFormat(17, 0x222222, 'light');

			txt.defaultTextFormat=frmt;
			txt.wordWrap=true;
			txt.width=440;
			txt.x=113;
			txt.text=text;
			txt.height=txt.textHeight+4;
			txt.y=(this.height-txt.height)/2;
			
			this.addChild(txt);
		}
	}
}