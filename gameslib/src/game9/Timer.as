package game9
{
	import common.Common;
	
	import flash.events.TimerEvent;
	import flash.text.TextField;
	import flash.utils.Timer;
	import flash.utils.clearInterval;
	import flash.utils.getTimer;
	import flash.utils.setInterval;
	
	import fontmanager.FontManager;
	
	import ru.flexdev.ui.SpriteEx;
	
	public class Timer extends SpriteEx
	{
		private var label:TextField;
		
		private var timer:flash.utils.Timer;
		private var counter:int=0;
		
		public function Timer()
		{
			super();
			this.width=80;
			this.height=25;
			
			this.label=FontManager.instance.textField;
			this.label.defaultTextFormat=FontManager.instance.textFormat(28, Common.ORANGE, 'bold');
			this.label.text='00:00';
			this.label.y=-4;
			this.label.width=this.label.textWidth+5;
			this.label.height=this.label.textHeight+5;
			this.label.x=(this.width-this.label.width)/2;
			this.addChild(this.label);
			
			this.timer=new flash.utils.Timer(1000);
			this.timer.addEventListener(TimerEvent.TIMER, this.onTimer)
		}
		
		private function onTimer(event:TimerEvent):void
		{
			this.counter++;
			this.formatTime(this.counter);
		}
		
		public function start():void
		{
			this.counter=0;
			this.timer.start();
		}
		
		public function pause():void
		{
			this.timer.stop();
		}
		
		public function resume():void
		{
			this.timer.start();
		}
		
		public function stop():void
		{
			this.timer.stop();
		}
		
		private function formatTime(value:int):void
		{
			var minutes:int=Math.floor(value/60);
			var seconds:int=value-minutes*60;
			
			var mm:String=minutes.toString(); 
			if(mm.length==1)mm='0'+mm;
			
			var ss:String=seconds.toString(); 
			if(ss.length==1)ss='0'+ss;
			
			this.label.text=mm+':'+ss;
		}
		
		public function get time():String
		{
			return this.label.text;
		}
	}
}