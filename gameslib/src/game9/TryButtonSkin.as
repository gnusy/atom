package game9
{
	import common.Common;
	
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	import fontmanager.FontManager;
	
	import ru.flexdev.skins.button.BaseButtonSkin;
	import ru.flexdev.ui.Rect;
	
	public class TryButtonSkin extends BaseButtonSkin
	{
		private var rect:Rect;
		private var tf:TextField;
		
		public function TryButtonSkin()
		{
			super();
			
			this.rect=new Rect();
			this.rect.stroke(Common.GRAY).round(6, 6).bounds(0, 0, 0, 0);
			this.addChild(this.rect);
			
			this.tf=FontManager.instance.textField;
			this.tf.selectable=false;
			this.tf.mouseEnabled=false;
			var frmt:TextFormat=FontManager.instance.textFormat(17, Common.GRAY, 'regular');
			frmt.align=TextFormatAlign.CENTER;
			frmt.bold=true;
			this.tf.defaultTextFormat=frmt;
			this.tf.wordWrap=true;
			this.tf.width=this.width;
			this.tf.x=0;
			this.addChild(this.tf); 
			
			this.state='up';
			
			this.rect.visible=false;
			
			
		}
		
		override protected function updateState():void
		{
			switch(super.state)
			{
				case 'up':
					this.rect.gradientFill(0xffffff, 0xffffff, 90).stroke(Common.GRAY);
					this.updateLabelFont(true);
					break;
				
				case 'over':
					this.rect.gradientFill(0xfafafa, 0xe0e0e0, 90).stroke(Common.GRAY);
					this.updateLabelFont(true);
					break;
				
				case 'down':
					this.rect.gradientFill(0xe0e0e0, 0xe0e0e0, 90).stroke(Common.GRAY);
					this.updateLabelFont(true);
					break;
				
				case 'disabled':
					this.rect.fill(0xeeeeee, 1).stroke(0xaaaaaa);
					this.updateLabelFont(false);
					break;
			}
		}
		
		private function updateLabelFont(enabled:Boolean):void
		{
			var frmt:TextFormat=this.tf.getTextFormat();
			if(enabled==true)
			{
				if(super.state=='over')frmt.color=Common.ORANGE;
				else frmt.color=Common.GRAY;
			}
			else
			{
				frmt.color=0xaaaaaa;
			}
			this.tf.setTextFormat(frmt);
			this.tf.defaultTextFormat=frmt;
			
			this.updateLine();
		}
		
		override protected function updateLabel():void
		{
			this.tf.text=super.label;
			this.tf.height=this.tf.textHeight+5;
			this.tf.y=(this.height-this.tf.height)/2+1;
			
			
		}
		
		override protected function resize():void
		{
			if(this.tf==null)return;
			this.tf.width=this.width;
			this.tf.height=this.tf.textHeight+5;
			this.tf.y=(this.height-this.tf.height)/2+1;
			
			
			this.updateLine();
			
		}
		
		public function updateLine():void
		{
			this.graphics.clear();
			if(this.state=='over')this.graphics.lineStyle(0, Common.ORANGE);
			else this.graphics.lineStyle(0, Common.GRAY);
			this.graphics.moveTo(0, this.height);
			this.graphics.lineTo(this.width, this.height);
		}	
			
		
		override public function dispose():void
		{
			
		}
	}
}