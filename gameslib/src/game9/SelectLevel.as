package game9
{
	import caurina.transitions.Tweener;
	
	import common.ButtonSkin;
	import common.Common;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.text.TextField;
	
	import fontmanager.FontManager;
	
	import ru.flexdev.events.EventEx;
	import ru.flexdev.ui.Button;
	import ru.flexdev.ui.Rect;
	
	public class SelectLevel extends Rect
	{
		private var btnEasy:RadioButton;
		private var btnNormal:RadioButton;
		private var btnHard:RadioButton;
		private var btnBegin:Button;
		
		public function SelectLevel(width:int, height:int)
		{
			super();
			this.visible=false;
			this.alpha=0;
			
			this.width=width;
			this.height=height;
			this.fill(0x5e5e5d, 0.34);
			
			var wnd:Rect=new Rect();
			this.addChild(wnd);
			wnd.width=410;
			wnd.height=295;
			wnd.verticalCenter=0;
			wnd.horizontalCenter=0;
			wnd.fill(0xffffff).round(14, 16);
			
			var shadow:DropShadowFilter=new DropShadowFilter();
			shadow.color=0x3e3e3e;
			shadow.alpha=0.5;
			shadow.distance=1;
			shadow.blurX=shadow.blurY=16;
			wnd.filters=[shadow];
			
			var title:TextField=FontManager.instance.textField;
			title.defaultTextFormat=FontManager.instance.textFormat(21, Common.GRAY, 'bold');
			title.text='Выберите уровень сложности';
			title.y=34;
			title.width=title.textWidth+5;
			title.x=(wnd.width-title.width)/2;
			wnd.addChild(title);
			
			this.btnEasy=new RadioButton();
			wnd.addChild(this.btnEasy);
			this.btnEasy.horizontalCenter=0;
			this.btnEasy.top=85;
			this.btnEasy.label='Легко';
			this.btnEasy.width=125;
			this.btnEasy.height=40;
			this.btnEasy.selected=true;
			
			this.btnNormal=new RadioButton();
			wnd.addChild(this.btnNormal);
			this.btnNormal.horizontalCenter=0;
			this.btnNormal.top=120;
			this.btnNormal.label='Средне';
			this.btnNormal.width=125;
			this.btnNormal.height=40;
			
			this.btnHard=new RadioButton();
			wnd.addChild(this.btnHard);
			this.btnHard.horizontalCenter=0;
			this.btnHard.top=155;
			this.btnHard.label='Сложно';
			this.btnHard.width=125;
			this.btnHard.height=40;
			
			this.btnBegin=new Button();
			wnd.addChild(this.btnBegin);
			this.btnBegin.horizontalCenter=0;
			this.btnBegin.bottom=35;
			this.btnBegin.label='Начать игру';
			this.btnBegin.width=125;
			this.btnBegin.height=40;
			this.btnBegin.skinClass=game9.ButtonSkin;
			this.btnBegin.addEventListener(MouseEvent.CLICK, this.onBeginClick);
			
		}
		
		private function onBeginClick(event:MouseEvent):void
		{
			var res:int=1;
			if(this.btnEasy.selected)res=1; 
			else if(this.btnNormal.selected)res=2;
			else if(this.btnHard.selected)res=3;	
			
			this.dispatchEvent(new EventEx(Event.SELECT, res));
		}
		
		public function show(now:Boolean=false):void
		{
			this.visible=true;
			if(now)this.alpha=1;
			else Tweener.addTween(this, {alpha:1, time:0.5, transition:'linear'});
			
		}
		
		public function hide():void
		{
			Tweener.addTween(this, {alpha:0, time:0.5, transition:'linear', onComplete:this.onHideComplete});
		}
		
		private function onHideComplete():void
		{
			this.visible=false;
		}
	}
}