package game9
{
	import common.Common;
	
	import flash.events.Event;
	
	import ru.flexdev.events.EventEx;
	import ru.flexdev.ui.SpriteEx;

	public class Memory extends SpriteEx
	{
		private var cards:Vector.<Card>;
		private var _level:int;
		
		public function Memory()
		{
		}
		
		public function set level(value:int):void
		{
			if(value<1)value=1;
			if(value>3)value=3;
			this._level=value;
			
			this.width=this.height=20;
			
			Common.removeChildren(this);
			this.cards=new Vector.<Card>();
			
			var max_card_number:int=5+this._level*5;
			var data:Array=new Array();
			for(var i:int=0;i<max_card_number;i++)
			{
				data.push(i+1);
				data.push(i+1);
			}
			data.sort(this.shuffleCards);
			
			var cols:int=5;
			if(this._level==2)cols=6;
			if(this._level==3)cols=8;
			
			var gap:int=92;
			if(this._level==2)gap=76;
			if(this._level==3)gap=78;
			
			for(i=0;i<data.length;i++)
			{
				var card:Card=new Card(data[i], this._level);
				
				var x_modificator:int=i-cols*Math.floor(i/cols);
				var y_modificator:int=Math.floor(i/cols);
				card.x=x_modificator*gap;
				card.y=y_modificator*gap;
				
				this.addChild(card);
				
				this.width=Math.max(this.width, card.x+card.scaledWidth);
				this.height=Math.max(this.height, card.y+card.scaledHeight);
				
				this.cards.push(card);
			}
			
			this.addEventListener('flip', this.onFlipCard);
		}
		
		private function shuffleCards(val1:int, val2:int):Number
		{
			return Math.random()<0.5?1:-1;
		}
		
		private function onFlipCard(event:EventEx):void
		{
			var flipped_card:Card=event.data as Card;
			
			for(var i:int=0;i<this.cards.length;i++)
			{
				if(this.cards[i]==flipped_card)continue;
				if(this.cards[i].fixed)continue;
				if(this.cards[i].flipped)
				{
					if(this.cards[i].number!=flipped_card.number)
					{
						this.cards[i].unflip();
					}
					else
					{
						flipped_card.fixed=true;
						this.cards[i].fixed=true;
					}
				}
			}
			if(this.correct)this.dispatchEvent(new EventEx(Event.COMPLETE));
		}
		
		private function get correct():Boolean
		{
			for(var i:int=0;i<this.cards.length;i++)
			{
				if(this.cards[i].fixed==false)return false;
			}
			return true;
		}
	}
}