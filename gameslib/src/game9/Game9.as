package game9
{
	import common.Background;
	import common.TryButtonSkin;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import ru.flexdev.events.EventEx;
	import ru.flexdev.ui.Button;
	import ru.flexdev.ui.SpriteEx;
	
	public class Game9 extends Sprite
	{
		public static const WIDTH:int=1024;
		public static const HEIGHT:int=675;
		
		private var holder:SpriteEx;
		private var background:Background;
		private var selectLevel:SelectLevel;
		private var gameOver:GameOver;
		private var equipment:Equipment;
		
		private var memory:Memory;
		
		private var btnNewGame:Button;
		private var btnExit:Button;
		private var btnEquipment:Button;
		
		private var timer:Timer;
		private var bestTime:String='99:99';
		
		public function Game9()
		{
			super();
			this.background=new Background(872, 580);
			this.addChild(this.background);
			this.background.setTitles('', '');
			this.background.hideButton('toMenu').hideButton('toMap').hideButton('cross');
			this.background.x=(WIDTH-this.background.width)/2;
			this.background.y=(HEIGHT-this.background.height)/2;
			
			this.btnNewGame=new Button();
			this.background.addChild(this.btnNewGame);
			this.btnNewGame.left=275;
			this.btnNewGame.bottom=63;
			this.btnNewGame.label='Новая игра';
			this.btnNewGame.width=150;
			this.btnNewGame.height=40;
			this.btnNewGame.skinClass=game9.ButtonSkin;
			this.btnNewGame.addEventListener(MouseEvent.CLICK, this.onNewGameClick);
			
			this.btnExit=new Button();
			this.background.addChild(this.btnExit);
			this.btnExit.right=275;
			this.btnExit.bottom=63;
			this.btnExit.label='Выйти из игры';
			this.btnExit.width=150;
			this.btnExit.height=40;
			this.btnExit.skinClass=game9.ButtonSkin;
			this.btnExit.addEventListener(MouseEvent.CLICK, this.onExitClick);
			
			this.btnEquipment=new Button();
			this.background.addChild(this.btnEquipment);
			this.btnEquipment.horizontalCenter=0;
			this.btnEquipment.bottom=20;
			this.btnEquipment.label='Узнать про оборудование';
			this.btnEquipment.width=215;
			this.btnEquipment.height=25;
			this.btnEquipment.skinClass=game9.TryButtonSkin;
			this.btnEquipment.addEventListener(MouseEvent.CLICK, this.onEquipmentClick);
			
			
			this.timer=new Timer();
			this.background.addChild(this.timer);
			this.timer.top=34;
			this.timer.horizontalCenter=0;
			
			this.memory=new Memory();
			this.memory.horizontalCenter=0;
			this.memory.top=80;
			this.memory.level=1;
			this.background.addChild(this.memory);
			this.memory.addEventListener(Event.COMPLETE, this.onMemoryComplete);
			
			this.selectLevel=new SelectLevel(WIDTH, HEIGHT);
			this.addChild(this.selectLevel);
			this.selectLevel.show();
			this.selectLevel.addEventListener(Event.SELECT, this.onSelectLevel);
			
			this.gameOver=new GameOver(WIDTH, HEIGHT);
			this.addChild(this.gameOver);
			this.gameOver.addEventListener(Event.COMPLETE, this.onGameOver);
			
			
			this.equipment=new Equipment(WIDTH, HEIGHT);
			this.addChild(this.equipment);
			this.equipment.addEventListener(Event.COMPLETE, this.onEquipmentClose);
		}
		
		private function onMemoryComplete(event:EventEx):void
		{
			this.timer.stop();
			
			if(this.bestTime>this.timer.time)this.bestTime=this.timer.time;
			trace('complete', this.timer.time, this.bestTime);
			this.gameOver.show();
			this.gameOver.times(this.timer.time, this.bestTime);
			
		}
		
		private function onNewGameClick(event:MouseEvent):void
		{
			this.selectLevel.show();
			this.timer.stop();
		}
		
		private function onExitClick(event:MouseEvent):void
		{
			this.dispatchEvent(new Event(Event.COMPLETE));
		}
		
		private function onEquipmentClick(event:MouseEvent):void
		{
			this.timer.pause();
			this.equipment.show();
		}
		
		private function onSelectLevel(event:EventEx):void
		{
			this.selectLevel.hide();
			this.timer.start();
			this.memory.level=event.data;
		}
		
		private function onGameOver(event:EventEx):void
		{
			this.gameOver.hide();
		//	this.selectLevel.show();
		}
		
		private function onEquipmentClose(event:EventEx):void
		{
			this.timer.resume();
			this.equipment.hide();
		}
		
		public function center(parentWidht:int, parentHeight:int):void
		{
			this.x=0;
			this.y=parentHeight-HEIGHT;
		}
		
		private function gameComplete():void
		{
			this.dispatchEvent(new Event(Event.COMPLETE));
		}
	
	}
}