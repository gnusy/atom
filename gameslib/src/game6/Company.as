package game6
{
	import flash.display.Bitmap;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import ru.flexdev.events.EventEx;
	import ru.flexdev.ui.SpriteEx;
	
	public class Company extends SpriteEx
	{
		private var data:Object;
		public function Company(data:Object)
		{
			super();
			this.data=data;
			var img:Bitmap=new data['image']() as Bitmap;
			this.addChild(img);
			
			var position:Point=data['position'];
			this.x=position.x;
			this.y=position.y;
			
			this.addEventListener(MouseEvent.MOUSE_DOWN, this.onMouseDown);
		}
		
		private function onMouseDown(event:MouseEvent):void
		{
			this.dispatchEvent(new EventEx('company_select', this.data, true));
		}
	}
}
