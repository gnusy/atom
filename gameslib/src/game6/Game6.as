package game6
{
	import common.Background;
	import common.Common;
	import common.Helper;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.ui.Mouse;
	
	import fontmanager.FontManager;
	
	import ru.flexdev.events.EventEx;
	
	public class Game6 extends Sprite
	{
		public static const WIDTH:int=932;
		public static const HEIGHT:int=635;
	
		[Embed(source="assets/company0.png")]private var company0:Class;
		[Embed(source="assets/company1.png")]private var company1:Class;
		[Embed(source="assets/company2.png")]private var company2:Class;
		[Embed(source="assets/company3.png")]private var company3:Class;
		
		
		private var background:Background;
		private var helper:Helper;
		private var agregat:Agregat;
		
		private var targets_data:Array=[
			{company_id:3, position:new Point(436, 507), image:company3, image_position:new Point(110, 420)},
			{company_id:2, position:new Point(482, 404), image:company2, image_position:new Point(60, 10)},
			{company_id:1, position:new Point(577, 502), image:company1, image_position:new Point(245, 440)},
			{company_id:3, position:new Point(616, 358), image:company3, image_position:new Point(285, 85)},
			{company_id:1, position:new Point(687, 340), image:company1, image_position:new Point(365, 135)},
			{company_id:0, position:new Point(704, 185), image:company0, image_position:new Point(370, -15)},
			{company_id:3, position:new Point(709, 480), image:company3, image_position:new Point(380, 440)}
		];
		
		private var companies_data:Array=[
			{company_id:0, image:company0, position:new Point(45, 168)},
			{company_id:1, image:company1, position:new Point(45, 252)},
			{company_id:2, image:company2, position:new Point(45, 353)},
			{company_id:3, image:company3, position:new Point(45, 460)}
		];
		
		private var targets:Vector.<DropTarget>;
		private var companies:Array;
		
		private var dragItem:DragItem;
		public var errors:int=0;
		
		public function Game6()
		{
			super();
			this.background=new Background(WIDTH, HEIGHT);
			this.addChild(this.background);
			this.background.setTitles('Ядерная паропроизводящая установка (ЯППУ)', 'Определите, какую продукцию для ЯППУ производит предприятие\n(переместите логотип к соответствующей части установки)');
			this.background.hideButton('toMenu').hideButton('toMap');
			this.background.setTitleFormat('title1', FontManager.instance.textFormat(21, Common.GRAY, 'regular'));
			this.background.setTitleFormat('title2', FontManager.instance.textFormat(17, Common.GRAY, 'light'));
			this.background.topTitle('title1', 40);
			this.background.topTitle('title2', 73);
			this.background.leftTitle('title1', 35);
			this.background.leftTitle('title2', 35);
			
			
			this.agregat=new Agregat();
			this.agregat.bounds(313, 133);
			this.addChild(this.agregat);
			
			
			
			this.targets=new Vector.<DropTarget>();
			
			for(var i:int=0;i<this.targets_data.length;i++)
			{
				var dt:DropTarget=new DropTarget(this.targets_data[i]);
				dt.position=this.targets_data[i]['position'];
				this.addChild(dt);
				this.targets.push(dt);
			}
			
			
			this.companies=new Array();
			
			for(i=0;i<this.companies_data.length;i++)
			{
				var company:Company=new Company(this.companies_data[i]);
				this.addChild(company);
				this.companies.push(company);
			}
			
			this.addEventListener('company_select', this.onCompanySelect);
			
			this.dragItem=new DragItem();
			this.addChild(this.dragItem);
			
			this.helper=new Helper('Прекрасно!', 'Вы разобрались в том, какой вклад вносят предприятия дивизиона в создание ядерной паропроизводящей установки.\n\nНастало время узнать, как функционирует эта установка.');
			this.helper.buttonLabel='Смотреть видео';
			this.helper.windowSize=new Point(400, 275);
			this.helper.buttonWidth=150;
			this.helper.bounds(0, 0);
			this.helper.width=WIDTH;
			this.helper.height=HEIGHT;
			this.addChild(this.helper);
			this.helper.addEventListener(Event.CLOSE, this.onHelperClose);
			
		}
		
		private function onCompanySelect(event:EventEx):void
		{
			var company_data:Object=event.data;
			
			this.dragItem.show(company_data);
			
			this.dragItem.position=this.globalToLocal(new Point(this.stage.mouseX, this.stage.mouseY));
			this.addEventListener(MouseEvent.MOUSE_MOVE, this.onMouseEvent);
			this.addEventListener(MouseEvent.MOUSE_UP, this.onMouseEvent);
		}
		
		private function onMouseEvent(event:MouseEvent):void
		{
			switch(event.type)
			{
				case MouseEvent.MOUSE_MOVE:
					this.dragItem.position=this.globalToLocal(new Point(event.stageX, event.stageY));
					break;
				
				case MouseEvent.MOUSE_UP:
					this.removeEventListener(MouseEvent.MOUSE_MOVE, this.onMouseEvent);
					this.removeEventListener(MouseEvent.MOUSE_UP, this.onMouseEvent);
					this.dragItem.hide();
					this.checkDropTargets();
					break;
			}
			
		}
		
		private function checkDropTargets():void
		{
			for(var i:int=0;i<this.targets.length;i++)
			{
				if(this.targets[i].intersects(this.dragItem.rect)==false)continue;
				
				var res:int=this.targets[i].checkCompany(this.dragItem.data['company_id']);
				if(res==1)
				{
					this.agregat.addSuccess(this.targets[i]);
					if(this.agregat.correct==true)this.testComplete();
				}
				else if(res==-1)
				{
					this.errors++;
				}
			}
		}
		
		private function testComplete():void
		{
			this.helper.show();
		}
		
		private function onHelperClose(event:Event):void
		{
			trace('this.errors', this.errors);
			this.helper.hide();
			this.dispatchEvent(new Event(Event.COMPLETE));
			this.dispatchEvent(new EventEx('game_score', 14-this.errors, true));
		}
		
		public function center(parentWidht:int, parentHeight:int):void
		{
			this.x=(parentWidht-WIDTH)/2;
			this.y=(parentHeight-HEIGHT)/2;
		}
	}
}
