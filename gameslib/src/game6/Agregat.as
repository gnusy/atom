package game6
{
	import common.Common;
	
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.filters.ColorMatrixFilter;
	import flash.geom.Matrix;
	import flash.geom.Point;
	
	import ru.flexdev.ui.SpriteEx;
	
	public class Agregat extends SpriteEx
	{
		[Embed(source="assets/agregat.png")]private var agregatClass:Class;
		[Embed(source="assets/mark_right.png")]private var markClass:Class;
		
		private var count_successed:int=0;
		private var marksHolder:Sprite;
		
		public function Agregat()
		{
			super();
			var img:Bitmap=new agregatClass() as Bitmap;
			this.addChild(img);
			
			
			this.marksHolder=new Sprite();
			this.addChild(this.marksHolder);
			
			this.update();
		}
		
		public function addSuccess(target:DropTarget):void
		{
			this.count_successed++;
			if(this.count_successed>7)this.count_successed=7;
			
			this.update();
			
			
			trace(target.company.image);
			
			var img:Bitmap=new (target.company.image)();
			this.addChild(img);
			img.smoothing=true;
			img.scaleX=img.scaleY=0.5;
			img.x=(target.company.image_position as Point).x;
			img.y=(target.company.image_position as Point).y;
			img.alpha=0.5;
			var matrix:Array = new Array();
			matrix = matrix.concat([0.9, 0, 0, 0, 0]); // red
			matrix = matrix.concat([0.9, 0, 0, 0, 0]); // green
			matrix = matrix.concat([0.9, 0, 0, 0, 0]); // blue
			matrix = matrix.concat([0, 0, 0, 0.9, 0]); // alpha
			
			

			
			var cmf:ColorMatrixFilter=new ColorMatrixFilter(matrix);
			
			img.filters=[cmf];
		}
		
		private function update():void
		{
			Common.removeChildren(this.marksHolder);
			this.graphics.clear();
			this.graphics.lineStyle(0, 0x898989);
			this.graphics.moveTo(479, 479/2);
			this.graphics.lineTo(479+47, 479/2);
			
			this.graphics.moveTo(479+47, 479/2-318/2);
			this.graphics.lineTo(479+47, 479/2+318/2);
			
			this.graphics.beginFill(0xffffff);
			for(var i:int=0;i<7;i++)
			{
				this.graphics.moveTo(479+47, 479/2-318/2+i*53);
				this.graphics.lineTo(479+47+20, 479/2-318/2+i*53);
				this.graphics.drawCircle(479+47+20+41/2, 479/2-318/2+i*53, 41/2);
			}
			this.graphics.endFill();
			
			this.graphics.lineStyle(0, 0x32ce1b);
			
			
			for(i=0;i<this.count_successed;i++)
			{
				this.graphics.beginFill(0x32ce1b);
				this.graphics.drawCircle(479+47+20+41/2, 479/2-318/2+i*53, 41/2);
				this.graphics.endFill();	
				
				var img:Bitmap=new markClass() as Bitmap;
				this.marksHolder.addChild(img);
				
				img.x=479+47+20+41/2-img.width/2;
				img.y=479/2-318/2+i*53-img.height/2;

			}
			
		}
		
		public function get correct():Boolean
		{
			return this.count_successed==7;
		}
	}
}