package game6
{
	import flash.display.Bitmap;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import ru.flexdev.ui.Rect;
	
	public class DropTarget extends Rect
	{
		[Embed(source="assets/mark_right.png")]private var mark_right:Class;
		[Embed(source="assets/mark_wrong.png")]private var mark_wrong:Class;
		
		private var mark:Bitmap;
		public var company:Object;
		private var company_id:int=-1;
		private var _type:String;
		public function DropTarget(company:Object)
		{
			super();
			this.company=company;
			this.company_id=company['company_id'];
			
			this.width=41;
			this.height=41;
			this.type='normal';
		}
		
		private function set type(value:String):void
		{
			this._type=value;
			if(this.mark!=null)this.removeChild(this.mark);
			switch(value)
			{
				case 'normal':
					this.fill(0xd3d3d3).stroke(0xffffff).round(41, 41);
					break;
				
				case 'right':
					this.fill(0x32ce1b).stroke(0xffffff).round(41, 41);
					this.mark=new mark_right() as Bitmap;
					this.addChild(this.mark);
					this.mark.x=(this.width-this.mark.width)/2;
					this.mark.y=(this.height-this.mark.height)/2;
					break;
				
				case 'wrong':
					this.fill(0xffffff).stroke(0xd3d3d3).round(41, 41);
					this.mark=new mark_wrong() as Bitmap;
					this.addChild(this.mark);
					this.mark.x=(this.width-this.mark.width)/2;
					this.mark.y=(this.height-this.mark.height)/2;
					break;
			}
		}
		
		public function set position(value:Point):void
		{
			this.x=value.x-this.width/2;
			this.y=value.y-this.height/2;
		}
		
		public function intersects(value:Rectangle):Boolean
		{
			return new Rectangle(this.x, this.y, this.width, this.height).intersects(value);
		}
		
		public function checkCompany(company_id:int):int
		{
			if(this.company_id==company_id)
			{
				if(this._type=='right')return 0;
				this.type='right';
				return 1;
			}
			else if(this.company_id!=company_id)
			{
				if(this._type!='normal')return 0;
				this.type='wrong';
				return -1;
			}
			
			return 0;
		}
	}
}