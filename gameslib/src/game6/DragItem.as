package game6
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.filters.GlowFilter;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import ru.flexdev.ui.SpriteEx;
	
	public class DragItem extends SpriteEx
	{
		[Embed(source="assets/drag_mark.png")]private var drag_mark:Class;
		
		public var data:Object;
		private var logo:Bitmap;
		private var scale:Number=0.4166666666666667;
		
		public function DragItem()
		{
			super();
			var img:Bitmap=new drag_mark() as Bitmap;
			this.addChild(img);
			this.width=img.width;
			this.height=img.height;
			this.mouseEnabled=false;
			this.visible=false;
			
			var glow:GlowFilter=new GlowFilter(0xffffff, 1, 16, 16);
			this.filters=[glow];
		}
		
		public function set position(value:Point):void
		{
			this.x=value.x-this.width/2;
			this.y=value.y-this.height/2;
		}
		
		public function show(value:Object):void
		{
			this.data=value;
			this.visible=true;
			
			if(this.logo!=null)this.removeChild(this.logo);
			this.logo=new value['image']() as Bitmap;
			this.logo.smoothing=true;
			
			this.scale=this.height/this.logo.height;
			
			this.logo.scaleX=this.logo.scaleY=this.scale;
			this.logo.x=33;
			this.logo.y=(this.height-this.logo.height)/2;
			this.addChild(this.logo);
		}
		
		public function hide():void
		{
			this.visible=false;
		}
		
		public function get rect():Rectangle
		{
			return new Rectangle(this.x, this.y, this.width, this.height);
		}
	}
}
