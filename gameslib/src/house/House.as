package house
{
	import caurina.transitions.Tweener;
	
	import common.ButtonSkin;
	import common.Helper;
	
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import ru.flexdev.ui.Button;
	import ru.flexdev.ui.Image;
	import ru.flexdev.ui.Rect;
	
	public class House extends Sprite
	{
		[Embed(source="assets/background.png")]private var backgroundClass:Class;
		
		[Embed(source="assets/c1.png")]private var c1Class:Class;
		[Embed(source="assets/c2.png")]private var c2Class:Class;
		[Embed(source="assets/c3.png")]private var c3Class:Class;
		[Embed(source="assets/c4.png")]private var c4Class:Class;
		[Embed(source="assets/c5.png")]private var c5Class:Class;
		[Embed(source="assets/c6.png")]private var c6Class:Class;
		[Embed(source="assets/c7.png")]private var c7Class:Class;
		
				
		public static const WIDTH:int=1024;
		public static const HEIGHT:int=675;
		private var background:Rect;
		private var button:Button;
		private var index:int;
		
		
		private var cardsData:Array=[
			{position:new Point(300, 211), image:c1Class},
			{position:new Point(442, 211), image:c2Class},
			{position:new Point(584, 211), image:c3Class},
			{position:new Point(300, 321), image:c4Class},
			{position:new Point(442, 321), image:c5Class},
			{position:new Point(584, 321), image:c6Class},
			{position:new Point(300, 431), image:c7Class}
		];
		/*
		private var pack:Array=[
			{old:[], current:[]},
			{old:[], current:[0, 1, 2, 3, 4, 5, 6]},
			{old:[], current:[2]},
			{old:[2], current:[0]},
			{old:[0, 2], current:[4]},
			{old:[0, 2, 4], current:[5]},
			{old:[0, 2, 4, 5], current:[1, 6]},
			{old:[0, 2, 4, 5, 1, 6], current:[3]}
		];
		*/
		private var pack:Array=[
		{old:[], current:[]},
		{old:[], current:[0, 1, 2, 3, 4, 5, 6]},
		{old:[], current:[2]},
		{old:[], current:[0]},
		{old:[], current:[4]},
		{old:[], current:[5]},
		{old:[], current:[1, 6]},
		{old:[], current:[3]}
		];
		
		
		private var cards:Array;
		
		private var xxx:int=0;
		
		private var helper:Helper;
		
		public function House()
		{
			super();
			this.background=new Rect();
			this.background.width=WIDTH;
			this.background.height=HEIGHT;
			this.background.fill(0xffffff);
			this.addChild(this.background);
			var img:Image=new Image();
			img.source=(new backgroundClass() as Bitmap).bitmapData;
			img.top=0;
			img.horizontalCenter=0;
			img.alpha=0;
			this.background.addChild(img);
			
			Tweener.addTween(img, {alpha:1, time:1, transition:'linear'});
			
			
			
			this.cards=new Array();
			for(var i:int=0;i<this.cardsData.length;i++)
			{
				var card:Bitmap=new this.cardsData[i]['image']() as Bitmap;
				card.x=this.cardsData[i]['position'].x;
				card.y=this.cardsData[i]['position'].y;
				this.cards.push(card);
				this.background.addChild(card);
				card.alpha=0.4;
			}
			
			this.addEventListener(MouseEvent.CLICK, this.qqq);
			
			
			this.button=new Button();
			this.button.skinClass=common.ButtonSkin;
			this.button.label='Далее';
			this.button.horizontalCenter=0;
			this.button.bottom=30;
			this.button.width=83;
			this.button.height=29;
			this.button.addEventListener(MouseEvent.CLICK, this.onClick);
			this.addChild(this.button);
			
			this.helper=new Helper('', '100%-й результат машиностроительного дивизиона складывается из нескольких элементов.\n\nВ рамках курса Вы познакомитесь с ключевыми составляющими успеха дивизиона.');
			this.helper.bounds(0, 0);
			this.helper.width=WIDTH;
			this.helper.height=HEIGHT;
			this.helper.windowSize=new Point(390, 280);
			this.addChild(this.helper);
			this.helper.addEventListener(Event.CLOSE, this.onHelperClose);
		}
		
		private function onHelperClose(event:Event):void
		{
			this.helper.hide();
			
		}
		
		public function onClick(event:MouseEvent):void
		{
			//if(this.index==1)this.helper.show();
			//else this.dispatchEvent(new Event('game_close'));
			this.dispatchEvent(new Event('game_close'));
		}
		
		
		public function qqq(event:MouseEvent):void
		{
			if(!event.altKey || !event.ctrlKey)return;
			for(var i:int=0;i<this.cards.length;i++)
			{
				(this.cards[i] as Bitmap).alpha=0.4;
			}
			
			xxx++;
			this.show(xxx);
		
		}
		
		public function center(parentWidht:int, parentHeight:int):void
		{
			this.x=(parentWidht-WIDTH)/2;
			this.y=(parentHeight-HEIGHT);

		}
		
		public function show(index:int):void
		{
			this.index=index;
			if(index<1)index=1;
			if(index>7)index=7;
			var tmp:Object=this.pack[index];
			
			for(var i:int=0;i<tmp['old'].length;i++)
			{
				var card:Bitmap=(this.cards[tmp['old'][i]] as Bitmap);
				card.alpha=1;
			}
			
			for(i=0;i<tmp['current'].length;i++)
			{
				card=(this.cards[tmp['current'][i]] as Bitmap);
				
				var rect:Rect=new Rect();
				rect.width=card.width;
				rect.height=card.height;
				rect.x=card.x;
				rect.y=card.y;
				rect.fill(0x978aaa);
				card.parent.addChildAt(rect, 0);
				
				
				Tweener.addTween(card, {alpha:1, time:1, transition:'linear'});
				Tweener.addTween(rect, {alpha:0, time:1, transition:'linear'});
			}
			
			if(index==1)this.helper.show();
		}
	}
}