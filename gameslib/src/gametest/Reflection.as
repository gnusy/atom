package gametest
{
	import caurina.transitions.Tweener;
	
	import common.Common;
	import common.TryButtonSkin;
	
	import flash.display.Bitmap;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.filters.GlowFilter;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import fontmanager.FontManager;
	
	import ru.flexdev.events.EventEx;
	import ru.flexdev.ui.Button;
	import ru.flexdev.ui.Rect;
	
	public class Reflection extends Rect
	{
		[Embed(source="assets/star.png")]private var star:Class;
		
		private var title:TextField;
		private var text:TextField;
		private var button:Button;
		private var window:Rect;
		
		public function Reflection()
		{
			super();
			this.fill(0xffffff, 0.01);
			
			this.visible=false;
			this.alpha=0;
			
			this.window=new Rect();
			this.window.width=387;
			this.window.height=218;
			this.window.verticalCenter=0;
			this.window.horizontalCenter=0;
			this.window.fill(0xffffff).round(12, 12);
			this.addChild(this.window);
			
			
			var glow:GlowFilter=new GlowFilter(0xbababa, 0.25, 8, 8);
			var shadow:DropShadowFilter=new DropShadowFilter(8, 90, 0x000000, 0.5, 4, 8);
			this.window.filters=[glow, shadow];
			
			
			this.title=FontManager.instance.textField;
			var frmt:TextFormat=FontManager.instance.textFormat(20, Common.GRAY, 'regular');
			this.title.defaultTextFormat=frmt;
			this.title.y=66;
			this.title.text='Поздравляем!';
			this.title.width=this.title.textWidth+5;
			this.title.height=this.title.textHeight+5;
			this.title.x=(this.window.width-this.title.width)/2;
			this.window.addChild(this.title);
			
			this.text=FontManager.instance.textField;
			frmt=FontManager.instance.textFormat(15, Common.GRAY, 'light');
			frmt.align='center';
			this.text.defaultTextFormat=frmt;
			this.text.y=97;
			this.text.text='Вы ответили на __ из 10 вопросов\nи получаете __ элементов.';
			this.text.width=this.text.textWidth+5;
			this.text.height=this.text.textHeight+5;
			this.text.x=(this.window.width-this.text.width)/2;
			this.window.addChild(this.text);
			
			this.button=new Button();
			this.button.horizontalCenter=0;
			this.button.bottom=30;
			this.button.width=90;
			this.button.height=28;
			this.button.label='Далее';
			//this.button.skinClass=common.TryButtonSkin;
			this.button.skinClass=common.ButtonSkin;
			this.window.addChild(this.button);
			this.button.addEventListener(MouseEvent.CLICK, this.onClick);

			var image:Bitmap=new this.star() as Bitmap;
			this.window.addChild(image);
			image.y=23;
			image.x=(window.width-image.width)/2;
			
		}
		
		public function show(stars:int):void
		{
			var ending:String='ов';
			switch(stars)
			{
				case 1:
				case 21:
					ending='';
					break;
				
				case 2:
				case 3:
				case 4:
				case 22:
				case 23:
				case 24:
					ending='а';
					break;
			}
			
			
			var txt:String='Вы ответили на все вопросы\nи получаете '+ stars.toString() +' балл'+ending+'.'
			this.text.text=txt;
			this.text.width=this.text.textWidth+5;
			this.text.height=this.text.textHeight+5;
			this.text.x=(this.window.width-this.text.width)/2;
			
			
			if(stars>5)this.title.text='Поздравляем!';
			else this.title.text='Вам удалось набрать немного!';
			this.title.width=this.title.textWidth+5;
			this.title.x=(this.window.width-this.title.width)/2;
			
			
			this.visible=true;
			Tweener.addTween(this, {alpha:1, time:0.25, transition:'linear'});
		}
		
		public function hide(now:Boolean=false):void
		{
			if(now)
			{
				this.alpha=0;
				this.visible=false;
			}
			else Tweener.addTween(this, {alpha:0, time:0.25, transition:'linear', onComplete:this.onHideComplete});
		}
		
		private function onHideComplete():void
		{
			this.visible=false;
		}
		
		private function onClick(event:MouseEvent):void
		{
			this.dispatchEvent(new EventEx(Event.CLOSE));
		}
	}
}