package gametest.tests
{
	import caurina.transitions.Tweener;
	
	import common.Common;
	
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import fontmanager.FontManager;
	
	import gametest.GameTest;
	
	import ru.flexdev.events.EventEx;
	import ru.flexdev.ui.Button;
	import ru.flexdev.ui.SpriteEx;
	
	public class Test extends SpriteEx
	{
		private var data:Object;
		private var last:Boolean;
		private var answers:Vector.<TestItem>;
		public var score:int=3;
		private var testResultWindow:TestResultWindow;
		private var correct:Boolean=false;
		private var button:Button;
		private var question:TextField;
		
		public function Test(data:Object, last:Boolean=false)
		{
			super();
			this.width=GameTest.WIDTH;
			this.height=GameTest.HEIGHT;
			this.visible=false;
			this.alpha=0;
			this.data=data;
			this.last=last;
			
			var text:TextField=FontManager.instance.textField;
			var frmt:TextFormat=FontManager.instance.textFormat(20, Common.GRAY, 'regular');
			text.x=51;
			text.y=86;//53
			text.defaultTextFormat=frmt;
			text.wordWrap=true;
			text.width=770;
			text.text=this.data.text;
			text.height=text.textHeight+4;
			this.addChild(text);
			
			this.question=FontManager.instance.textField;
			frmt=FontManager.instance.textFormat(15, Common.GRAY, 'bold');
			this.question.x=51;
			this.question.y=Math.max(text.y+text.height+34, 163);
			this.question.defaultTextFormat=frmt;
			this.question.wordWrap=true;
			this.question.width=770;
			this.question.text=this.data.question;
			this.question.height=question.textHeight+4;
			this.addChild(this.question);
			
			this.answers=new Vector.<TestItem>();
			var a_y:int=this.question.y+this.question.height+31;
			var prevAnswer:TestItem=null;
				
			for(var i:int=0;i<this.data['answers'].length;i++)
			{
				var answer_data:Object=this.data['answers'][i];
				var answer:TestItem=new TestItem(answer_data.text, answer_data.correct);
				answer.x=60;
				if(prevAnswer==null)answer.y=a_y;
				else answer.y=prevAnswer.y+prevAnswer.height+15;
				this.answers.push(answer);
				this.addChild(answer);
				prevAnswer=answer;
			}
			
			
			this.button=new Button();
			this.button.skinClass=common.ButtonSkin;
			this.button.width=200;
			this.button.height=40;
			this.button.label='Принять ответ';
			//if(last)this.button.label='Завершить тест';
			this.button.bottom=50;
			this.button.left=59;
			this.addChild(button);
			this.button.addEventListener(MouseEvent.CLICK, this.onButtonClick);
		
			this.addEventListener('answer_selected', this.onAnswerSelected);
			
			
			this.testResultWindow=new TestResultWindow();
			this.addChild(this.testResultWindow);
			this.testResultWindow.addEventListener('close', this.onTestResultWindowClose);
			
		}
		
		private function onAnswerSelected(event:EventEx):void
		{
			if(this.isMultiSelect==true)return;
			for(var i:int=0;i<this.answers.length;i++)
			{
				if(this.answers[i]!=event.data)this.answers[i].unselect();
			}
		}
		
		private function get isMultiSelect():Boolean
		{
			var counter:int=0;
			for(var i:int=0;i<this.data['answers'].length;i++)
			{
				if(this.data['answers'][i]['correct']==true)counter++;
			}
			if(counter>1)return true;
			return false;
		}
		
		private function onButtonClick(event:MouseEvent):void
		{
			if(this.score==0)
			{
				this.dispatchEvent(new EventEx('next_test', this.score, true));
				return;
			}
			
			
			this.correct=true;
			for(var i:int=0;i<this.answers.length;i++)
			{
				if(this.answers[i].correct==false)
				{
					this.correct=false;
					break;
				}
				
			}

			
			if(this.correct==true)
			{
				if(this.last==false)this.testResultWindow.show(1);
				else this.testResultWindow.show(4);
			}
			
			if(this.correct==false)
			{
				this.score--;
				if(this.score==0)this.testResultWindow.show(3);
				else this.testResultWindow.show(2);
			}

			
		}
		
		private function onTestResultWindowClose(event:EventEx):void
		{
			this.testResultWindow.hide();
			var type:int=event.data;
			if(type==1)
			{
				this.dispatchEvent(new EventEx('next_test', this.score, true));
			}
			if(type==2)
			{
				//if(this.correct==true)this.dispatchEvent(new EventEx('next_test', this.score, true));
			}
			if(type==3)
			{
				this.showRightAnswers();
				this.button.label='Следующий вопрос';
			}
			if(type==4)
			{
				this.dispatchEvent(new EventEx('next_test', this.score, true));
			}
			
			//this.dispatchEvent(new EventEx('next_test', 1, true));
		}
		
		private function showRightAnswers():void
		{
			this.question.text='Правильный ответ:';
			
			for(var i:int=0;i<this.answers.length;i++)
			{
				this.answers[i].setCorrect();
			}
			
		}
		
		public function show(now:Boolean=false):void
		{
			this.visible=true;
			if(now)this.alpha=1;
			else Tweener.addTween(this, {alpha:1, time:0.5, transition:'linear'});
		}
		
		public function hide():void
		{
			Tweener.addTween(this, {alpha:0, time:0.5, transition:'linear', onComplete:this.onHideComplete});
		}
		
		private function onHideComplete():void
		{
			this.visible=false;
			this.parent.removeChild(this);
		}
	}
}