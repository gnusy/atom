package gametest.tests
{
	import caurina.transitions.Tweener;
	
	import common.Common;
	import common.TryButtonSkin;
	
	import flash.display.Bitmap;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.filters.GlowFilter;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import fontmanager.FontManager;
	
	import gametest.GameTest;
	
	import ru.flexdev.events.EventEx;
	import ru.flexdev.ui.Button;
	import ru.flexdev.ui.Rect;
	
	public class TestResultWindow extends Rect
	{

		
		private var title:TextField;
		private var text:TextField;
		private var button:Button;
		private var window:Rect;
		private var type:int=-1;
		
		public function TestResultWindow()
		{
			super();
			this.width=GameTest.WIDTH;
			this.height=GameTest.HEIGHT;
			this.fill(0xffffff, 0.01);
			
			this.visible=false;
			this.alpha=0;
			
			this.window=new Rect();
			this.window.width=400;
			this.window.height=218;
			this.window.verticalCenter=0;
			this.window.horizontalCenter=0;
			this.window.fill(0xffffff).round(12, 12);
			this.addChild(this.window);
			
			
			var glow:GlowFilter=new GlowFilter(0xbababa, 0.25, 8, 8);
			var shadow:DropShadowFilter=new DropShadowFilter(8, 90, 0x000000, 0.5, 4, 8);
			this.window.filters=[glow, shadow];
			
			
			this.title=FontManager.instance.textField;
			var frmt:TextFormat=FontManager.instance.textFormat(20, Common.GRAY, 'regular');
			this.title.defaultTextFormat=frmt;
			this.title.y=66;
			this.title.text='Поздравляем!';
			this.title.width=this.title.textWidth+5;
			this.title.height=this.title.textHeight+5;
			this.title.x=(this.window.width-this.title.width)/2;
			this.window.addChild(this.title);
			
			this.text=FontManager.instance.textField;
			frmt=FontManager.instance.textFormat(17, Common.GRAY, 'light');
			frmt.align='center';
			this.text.defaultTextFormat=frmt;
			this.text.y=97;
			this.text.text='Обратная связь';
			this.text.width=this.text.textWidth+5;
			this.text.height=this.text.textHeight+5;
			this.text.x=(this.window.width-this.text.width)/2;
			this.window.addChild(this.text);
			
			this.button=new Button();
			this.button.horizontalCenter=0;
			this.button.bottom=30;
			this.button.width=200;
			this.button.height=40;
			this.button.label='Далее';
			this.button.skinClass=common.ButtonSkin;
			this.window.addChild(this.button);
			this.button.addEventListener(MouseEvent.CLICK, this.onClick);
			
		}
		
		public function show(type:int):void
		{
			this.type=type;
			switch(type)
			{
				case 1:
					this.title.text='Поздравляем!';
					this.text.text='Вы ответили верно и можете двигаться дальше.';
					this.button.label='Следующий вопрос';
					break;
			
				case 2:
					this.title.text='Неверно!';
					this.text.text='Обдумайте еще раз ваш ответ.';
					this.button.label='Попробовать ещё раз';
					break;

				case 3:
					this.title.text='Попытки исчерпаны!';
					this.text.text='К сожалению, ваш ответ неверен.';
					this.button.label='Узнать правильный ответ';
					break;
				
				case 4:
					this.title.text='Поздравляем!';
					this.text.text='Вы ответили на все вопросы.';
					this.button.label='Завершить тест';
					break;

			}
			
			
			//var txt:String='Вы ответили на '+stars.toString()+' из 10 вопросов\nи получаете '+ stars.toString() +' элемент'+ending+'.'
			//this.text.text=txt;
			this.text.width=this.text.textWidth+5;
			this.text.height=this.text.textHeight+5;
			this.text.x=(this.window.width-this.text.width)/2;
			
			
			//if(stars>5)this.title.text='Поздравляем!';
			//else this.title.text='Вам удалось набрать немного!';
			this.title.width=this.title.textWidth+5;
			this.title.x=(this.window.width-this.title.width)/2;
			
			
			this.visible=true;
			Tweener.addTween(this, {alpha:1, time:0.25, transition:'linear'});
		}
		
		public function hide(now:Boolean=false):void
		{
			if(now)
			{
				this.alpha=0;
				this.visible=false;
			}
			else Tweener.addTween(this, {alpha:0, time:0.25, transition:'linear', onComplete:this.onHideComplete});
		}
		
		private function onHideComplete():void
		{
			this.visible=false;
		}
		
		private function onClick(event:MouseEvent):void
		{
			this.dispatchEvent(new EventEx(Event.CLOSE, this.type));
		}
	}
}