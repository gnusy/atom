package gametest.tests
{
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import fontmanager.FontManager;
	
	import ru.flexdev.ui.Rect;
	
	public class Counter extends Rect
	{
		private var text:TextField;
		
		public function Counter()
		{
			super();
			this.width=90;
			this.height=22;
			this.fill(0xfbb03b).round(22, 22);
			
			this.text=FontManager.instance.textField;
			var frmt:TextFormat=FontManager.instance.textFormat(9.79, 0xffffff, 'bold');
			frmt.align='center';
			this.text.x=0;
			this.text.width=this.width;
			this.text.defaultTextFormat=frmt;
			this.text.text='Вопрос 0/10';
			this.text.height=text.textHeight+4;
			this.text.y=(this.height-this.text.height)/2
			this.addChild(this.text);
		}
		
		public function set count(value:int):void
		{
			this.text.text='Вопрос '+value.toString()+'/10';
		}
	}
}