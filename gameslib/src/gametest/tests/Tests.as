package gametest.tests
{
	import gametest.tests.Test;
	
	import gametest.GameTest;
	
	import ru.flexdev.events.EventEx;
	import ru.flexdev.ui.SpriteEx;
	
	public class Tests extends SpriteEx
	{
		private var data:Array;
		private var tests:Vector.<Test>;
		private var counter:int=-1;
		private var currentTest:Test;
		private var stars:int=0;
		private var testCounter:Counter;
		
		public function Tests(data:Array)
		{
			super();
			this.data=data;
			this.width=GameTest.WIDTH;
			this.height=GameTest.HEIGHT;
			
			this.testCounter=new Counter();
			this.addChild(this.testCounter);
			this.testCounter.bounds(50, 52);
			
			this.tests=new Vector.<Test>();
			for(var i:int=0;i<this.data.length;i++)
			{
				var test:Test=new Test(this.data[i], i==this.data.length-1);
				this.tests.push(test);
			}
			
			this.nextTest();
			this.addEventListener('next_test', this.onNextTest);
		}
		
		public function nextTest():void
		{
			this.counter++;
			if(this.counter>this.tests.length)return;
			if(this.counter==this.tests.length)
			{
				this.complete();
				return;
			}
			if(this.currentTest!=null)this.currentTest.hide();
			this.currentTest=this.tests[this.counter];
			this.addChild(this.currentTest);
			this.currentTest.show(this.counter==0);
			this.testCounter.count=this.counter+1;
		}
	
		private function onNextTest(event:EventEx):void
		{
			this.stars=this.stars+event.data;
			this.nextTest();
		}
		
		private function complete():void
		{
			this.dispatchEvent(new EventEx('test_complete', this.stars));
		}
	}
}