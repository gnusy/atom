package gametest
{
	import by.blooddy.crypto.serialization.JSON;
	
	import common.Background;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.utils.ByteArray;
	
	import gametest.tests.Tests;
	
	import ru.flexdev.events.EventEx;
	
	public class GameTest extends Sprite
	{
		public static const WIDTH:int=870;
		public static const HEIGHT:int=528;
		
		private var background:Background;
		
		[Embed(source="assets/data.json", mimeType="application/octet-stream")]private var data_json:Class;
		
		private var test_data:Array;
		
		private var tests:Tests;
		private var _stars:int=0;
		private var reflection:Reflection;
		
		public function GameTest()
		{
			super();
			this.background=new Background(WIDTH, HEIGHT);
			this.addChild(this.background);
			this.background.setTitles('', '');
			this.background.hideButton('toMap');
			this.background.hideButton('toMenu');
		
			this.createTests();
			
			this.tests=new Tests(test_data);
			this.addChild(this.tests);
			
			this.reflection=new Reflection();
			this.addChild(this.reflection);
			this.reflection.bounds(0, 0);
			this.reflection.width=GameTest.WIDTH;
			this.reflection.height=GameTest.HEIGHT;
				
			this.reflection.addEventListener(Event.CLOSE, this.onReflectionClose);
			
			
			this.tests.addEventListener('test_complete', this.onTestComplete);
		}
		
		private function createTests():void
		{
			var ba:ByteArray=new this.data_json() as ByteArray;
			ba.position=0;
			var json:String=ba.readUTFBytes(ba.length);
			this.test_data=JSON.decode(json)['tests'] as Array;
		}
		
		public function center(parentWidht:int, parentHeight:int):void
		{
			this.x=(parentWidht-WIDTH)/2;
			this.y=(parentHeight-HEIGHT)/2;
		}
		
		private function onTestComplete(event:EventEx):void
		{
			this._stars=event.data;
			this.reflection.show(this._stars);
		}
		
		private function onReflectionClose(event:EventEx):void
		{
			this.reflection.hide();
			this.dispatchEvent(new EventEx('game_score', this._stars, true));
		}
		
		public function get stars():int
		{
			return this._stars;
		}
	}
}