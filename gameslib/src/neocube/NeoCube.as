package neocube
{
	import caurina.transitions.Tweener;
	
	import common.Background;
	import common.Common;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.utils.clearInterval;
	import flash.utils.setInterval;
	import flash.utils.setTimeout;
	
	import fontmanager.FontManager;
	
	import ru.flexdev.events.EventEx;
	
	public class NeoCube extends Sprite
	{
		public static const WIDTH:int=872;
		public static const HEIGHT:int=580;
		
		private var background:Background;
		private var cube:Cube;
		private var text:TextField;
		private var subtext:TextField;
		private var counter:int;
		private var interval:uint;
		
		private var items_data:Array=[
			{name:'На шаг\nвпереди', text:'Мы стремимся быть лидером на глобальных рынках. Мы всегда на шаг впереди в технологиях, знаниях и качествах наших сотрудников. Мы предвидим, что будет завтра, и готовы к этому сегодня. Мы постоянно развиваемся и учимся. Каждый день мы стараемся работать лучше, чем вчера.', y:98, x:50, correct:true},
			{name:'Эффективность', text:'Мы всегда находим наилучшие варианты решения задач. Мы эффективны во всем, что мы делаем — при выполнении поставленных целей мы максимально рационально используем ресурсы компании и постоянно совершенствуем рабочие процессы. Нет препятствий, которые могут помешать нам находить самые эффективные решения.', y:158, x:280, correct:true},
			{name:'Ответственность\nза результат', text:'Каждый из нас несет личную ответственность за результат своей работы и качество своего труда перед государством, отраслью, коллегами и заказчиками. В работе мы предъявляем к себе самые высокие требования. Оцениваются не затраченные усилия, а достигнутый результат. Успешный результат — основа для наших новых достижений.', y:200, x:50, correct:true},
			{name:'Единая \nкоманда', text:'Мы все — Росатом. У нас общие цели. Работа в команде единомышленников позволяет достигать уникальных результатов. Вместе мы сильнее и можем добиваться самых высоких целей. Успехи сотрудников — успехи компании.', y:261, x:50, correct:true},
			{name:'Уважение', text:'Мы с уважением относимся к нашим заказчикам, партнерам и поставщикам. Мы всегда внимательно слушаем и слышим друг друга вне зависимости от занимаемых должностей и места работы. Мы уважаем историю и традиции отрасли. Достижения прошлого вдохновляют нас на новые победы.', y:329, x:280, correct:true},
			{name:'Безопасность', text:'Безопасность — наивысший приоритет. В нашей работе мы в первую очередь обеспечиваем полную безопасность людей и окружающей среды. В безопасности нет мелочей — мы знаем правила безопасности и выполняем их, пресекая нарушения.', y:375, x:50, correct:true},
			{name:'Качество', text:'', y:98, x:280, correct:false},
			{name:'Вместе сильнее', text:'', y:158, x:50, correct:false},
			{name:'Результативность', text:'', y:200, x:280, correct:false},
			{name:'Технологическое\nлидерство', text:'', y:261, x:280, correct:false},
			{name:'Инновации', text:'', y:329, x:50, correct:false},
			{name:'Ответственность', text:'', y:375, x:280, correct:false}
		];
		
		private var items:Vector.<Item>;
		
		public function NeoCube()
		{
			super();

			this.background=new Background(WIDTH, HEIGHT);
			this.addChild(this.background);
			this.background.setTitles('Реализацию стратегии Госкорпорации во многом направляют корпоративные ценности.', '');
			this.background.setTitleFormat('title1', FontManager.instance.textFormat(21, Common.GRAY, 'bold')); 
			this.background.hideButton('toMenu');
			this.background.hideButton('toMap');
			//this.background.topButtonCross(31);
			
			
			this.subtext=FontManager.instance.textField;
			var frmt:TextFormat=FontManager.instance.textFormat(21, Common.GRAY, 'regular');
			this.subtext.defaultTextFormat=frmt;
			this.subtext.wordWrap=true;
			this.subtext.width=750;
			this.subtext.x=50;
			this.subtext.y=115;
			this.subtext.text='Выберите 6 ключевых ценностей «Росатома»';
			this.addChild(this.subtext);
			
			
			
			this.cube=new Cube();
			this.cube.x=343;
			this.cube.y=110;
			this.addChild(this.cube);
			this.cube.visible=false;
			this.cube.alpha=0;
			
			this.items=new Vector.<Item>();
			
			for(var i:int=0;i<this.items_data.length;i++)
			{
				var item:Item=new Item(this.items_data[i]);
				item.x=this.items_data[i]['x'];
				item.y=this.items_data[i]['y']+88;
				this.addChild(item);
				this.items.push(item);
			}
			
			this.text=FontManager.instance.textField;
			frmt=FontManager.instance.textFormat(17, Common.GRAY, 'light');
			frmt.align=TextFormatAlign.JUSTIFY;
			this.text.defaultTextFormat=frmt;
			this.text.wordWrap=true;
			this.text.width=750;
			this.text.height=100;
			this.text.x=50;
			this.text.y=450;
			this.addChild(this.text);
			
			
			this.addEventListener('item_selected', this.onItemSelectTest);
			
			this.addEventListener(MouseEvent.CLICK, this.onMouseClick);
			
		}
		
		private function onItemSelectTest(event:EventEx):void
		{
			for(var i:int=0;i<this.items.length;i++)
			{
				if(this.items[i].correct==false)return;
			}
			
			this.removeEventListener('item_selected', this.onItemSelectTest);
			
			for(i=0;i<this.items.length;i++)
			{
				this.items[i].postTest();
				if(this.items[i].isCorrect==false)
				{
					this.items.splice(i, 1);
					i--;
				}
			}
			
			for(i=0;i<this.items_data.length;i++)
			{
				if(this.items_data[i]['correct']==false)
				{
					this.items_data.splice(i, 1);
					i--;
				}
			}
			
			this.background.setTitles('Единые корпоративные ценности', '');
			this.subtext.visible=false;
			setTimeout(this.start, 1000);
		}
		
		private function onMouseClick(event:MouseEvent):void
		{
			if(event.ctrlKey && event.altKey)this.start();
		}
		
		public function start():void
		{
			this.counter=-1;
			this.cube.visible=true;
			Tweener.addTween(this.cube, {alpha:1, time:0.5, transition:'linear'});
			this.interval=setInterval(this.autoShowItem, 1000);
		}
		
		public function autoShowItem():void
		{
			this.counter++;
			if(this.counter==this.items_data.length)
			{
				clearInterval(this.interval);
				for(var i:int=0;i<this.items.length;i++)
				{
					this.items[i].selected=false;
					this.items[i].buttonMode=true;
				}
				this.text.text='Кликните на названии ценности, и ниже НЭОКУБ появится утверждение, лежащее в его основе.';
				this.cube.hideAll(0.6);
				this.addEventListener('item_selected', this.onItemSelect);
				return;
			}
			var item_data:Object=this.items_data[this.counter];
			this.items[this.counter].selected=true;
			this.cube.show(this.counter);
			this.text.text=item_data['text'];
		}
		
		private function onItemSelect(event:EventEx):void
		{
			var index:int=this.items.indexOf(event.data);
			for(var i:int=0;i<this.items.length;i++)
			{
				if(this.items[i]!=event.data)this.items[i].selected=false;
			}
			
			this.cube.showOnly(index);
			this.text.text=this.items_data[index]['text'];
			
		}
		
		public function center(parentWidht:int, parentHeight:int):void
		{
			this.x=(parentWidht-WIDTH)/2;
			this.y=(parentHeight-HEIGHT)/2;
			
		}
	}
}