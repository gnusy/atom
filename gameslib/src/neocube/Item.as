package neocube
{
	import caurina.transitions.Tweener;
	
	import common.Common;
	
	import flash.display.Bitmap;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import fontmanager.FontManager;
	
	import ru.flexdev.events.EventEx;
	import ru.flexdev.ui.SpriteEx;
	
	public class Item extends SpriteEx
	{
		private var _selected:Boolean=false;
		private var _correct:Boolean=false;
		
		private var title:TextField;
		public var mode:String='test';
		private var _data:Object;
		
		public function Item(data:Object)
		{
			super();
			this.width=692;
			this.height=8;
			this.buttonMode=true;
			this._data=data;
			this._correct=data['correct'];
			
			this.title=FontManager.instance.textField;
			var frmt:TextFormat=FontManager.instance.textFormat(18, Common.GRAY, 'regular');
			this.title.defaultTextFormat=frmt;
			this.title.wordWrap=true;
			this.title.width=680;
			this.title.height=16;
			this.title.x=12;
			this.title.y=-6;
			this.title.text=data['name'];
			this.title.height=this.title.textHeight+4;
			this.title.width=this.title.textWidth+5;
			this.addChild(this.title);
			
			this.height=this.title.height;
			this.update();	
			this.addEventListener(MouseEvent.CLICK, this.onClick);
			
		}
		
		private function onClick(event:MouseEvent):void
		{
			if(this.buttonMode==false)return;
			
			if(this.mode=='test')this._selected=!this._selected;
			else this._selected=true;
			this.update();
			this.dispatchEvent(new EventEx('item_selected', this, true));
		}
		
		private function update():void
		{
			var frmt:TextFormat=this.title.getTextFormat();
			
			
			if(this.mode=='test')
			{
				if(this._selected && this._correct)
				{
					frmt.color=Common.ORANGE;
				}
				else if(this._selected && !this._correct)
				{
					frmt.color=0xff0000;
				}
				else				
				{
					frmt.color=Common.GRAY;
				}
			}
			else
			{
				if(this._selected && this._correct)
				{
					frmt.color=Common.ORANGE;
				}
				else				
				{
					frmt.color=Common.GRAY;
				}
			}
			
			this.title.setTextFormat(frmt);
		}
		
		public function set selected(value:Boolean):void
		{
			 this._selected=value;
			 this.update();	
		}
		
		public function get correct():Boolean
		{
			return this._selected==this._correct;
		}
		
		public function get isCorrect():Boolean
		{
			return this._correct;
		}
		
		public function postTest():void
		{
			if(this._correct==false)Tweener.addTween(this, {alpha:0, time:0.5, transition:'linear', onComplete:this.onHide});
			else
			{
				Tweener.addTween(this, {x:50, y:y-88, time:1, transition:'linear'});
				this.mode='normal';
				this.buttonMode=false;
				this._selected=false;
				this.update();
			}
		}
		
		private function onHide():void
		{
			this.visible=false;
		}
	}
}