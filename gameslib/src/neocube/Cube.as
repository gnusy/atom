package neocube
{
	import caurina.transitions.Tweener;
	
	import common.Common;
	
	import flash.display.Bitmap;
	import flash.text.TextField;
	
	import fontmanager.FontManager;
	
	import ru.flexdev.ui.Image;
	import ru.flexdev.ui.SpriteEx;
	
	public class Cube extends SpriteEx
	{
		[Embed(source="assets/cube.png")]private var cube:Class;
		[Embed(source="assets/item0.png")]private var item0Class:Class;
		[Embed(source="assets/item1.png")]private var item1Class:Class;
		[Embed(source="assets/item2.png")]private var item2Class:Class;
		[Embed(source="assets/item3.png")]private var item3Class:Class;
		[Embed(source="assets/item4.png")]private var item4Class:Class;
		[Embed(source="assets/item5.png")]private var item5Class:Class;
		
		private var items:Array;
		
		public function Cube()
		{
			super();
			this.width=493;
			this.height=306;
			
			var img:Bitmap=new cube() as Bitmap;
			var image:Image=new Image();
			image.source=img.bitmapData;
			image.left=143;
			image.top=72;
			this.addChild(image);
			
			this.items=new Array();
			this.createItems();
		}
		
		public function createItems():void
		{
			var tf:TextField;
			var img:Bitmap;
			
			img=new item0Class() as Bitmap;
			img.x=97;
			img.y=93;
			this.addChild(img);
			tf=FontManager.instance.textField;
			tf.defaultTextFormat=FontManager.instance.textFormat(18, Common.GRAY);
			tf.x=-4;
			tf.y=83;
			tf.width=150;
			tf.text='На шаг\nвпереди';
			this.addChild(tf);
			this.items.push([img, tf]);
			
			img=new item1Class() as Bitmap;
			img.x=202;
			img.y=29;
			this.addChild(img);
			tf=FontManager.instance.textField;
			tf.defaultTextFormat=FontManager.instance.textFormat(18, Common.GRAY);
			tf.x=156;
			tf.y=-3;
			tf.width=150;
			tf.text='Эффективность';
			this.addChild(tf);
			this.items.push([img, tf]);
			
			img=new item2Class() as Bitmap;
			img.x=297;
			img.y=93;
			this.addChild(img);
			tf=FontManager.instance.textField;
			tf.defaultTextFormat=FontManager.instance.textFormat(18, Common.GRAY);
			tf.x=337;
			tf.y=83;
			tf.width=150;
			tf.text='Ответственность\nза результат';
			this.addChild(tf);
			this.items.push([img, tf]);
			
			img=new item3Class() as Bitmap;
			img.x=97;
			img.y=188;
			this.addChild(img);
			tf=FontManager.instance.textField;
			tf.defaultTextFormat=FontManager.instance.textFormat(18, Common.GRAY);
			tf.x=-4;
			tf.y=181;
			tf.width=150;
			tf.text='Единая\nкоманда';
			this.addChild(tf);
			this.items.push([img, tf]);
			
			img=new item4Class() as Bitmap;
			img.x=202;
			img.y=252;
			this.addChild(img);
			tf=FontManager.instance.textField;
			tf.defaultTextFormat=FontManager.instance.textFormat(18, Common.GRAY);
			tf.x=175;
			tf.y=283;
			tf.width=150;
			tf.text='Уважение';
			this.addChild(tf);
			this.items.push([img, tf]);
			
			img=new item5Class() as Bitmap;
			img.x=304;
			img.y=188;
			this.addChild(img);
			tf=FontManager.instance.textField;
			tf.defaultTextFormat=FontManager.instance.textFormat(18, Common.GRAY);
			tf.x=337;
			tf.y=190;
			tf.width=150;
			tf.text='Безопасность';
			this.addChild(tf);
			this.items.push([img, tf]);
			
			this.hideAll();
		}
		
		public function hideAll(alpha:Number=0):void
		{
			for(var i:int=0;i<this.items.length;i++)
			{
				this.items[i][0].alpha=alpha;
				this.items[i][1].alpha=alpha;
			}
		}
		
		public function show(index:int):void
		{
			//this.items[index][0].alpha=1;
			//this.items[index][1].alpha=1;
			Tweener.addTween(this.items[index][0], {alpha:1, time:1, transition:'linear'});
			Tweener.addTween(this.items[index][1], {alpha:1, time:1, transition:'linear'});
		}
		
		public function showOnly(index:int):void
		{
			this.hideAll(0.6);
			//this.items[index][0].alpha=1;
			//this.items[index][1].alpha=1;
			Tweener.addTween(this.items[index][0], {alpha:1, time:1, transition:'linear'});
			Tweener.addTween(this.items[index][1], {alpha:1, time:1, transition:'linear'});
		}
	}
}