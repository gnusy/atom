package game7
{
	import flash.display.Bitmap;
	import flash.filters.GlowFilter;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import ru.flexdev.ui.Rect;
	
	public class DragItem extends Rect
	{
		public var product:Product;
		private var data:Object;
		private var logo:Bitmap;
		private var scale:Number=0.4166666666666667;
		
		public function DragItem()
		{
			super();
			this.width=58;
			this.height=58;
			this.fill(0xffffff).stroke(0xf8ad3b, 1, 2, false).round(this.width, this.height);
			
			this.mouseEnabled=false;
			this.visible=false;
		}
		
		public function set position(value:Point):void
		{
			this.x=value.x-this.width/2;
			this.y=value.y-this.height/2;
		}
		
		public function show(product:Product):void
		{
			this.parent.setChildIndex(this, this.parent.numChildren-1);
			
			this.product=product;
			this.data=product.data;
			this.visible=true;
			
			if(this.logo!=null)this.removeChild(this.logo);
			this.logo=new this.data['image']() as Bitmap;
			this.logo.smoothing=true;
			this.logo.scaleX=this.logo.scaleY=43/58;
			this.logo.x=(this.width-this.logo.width)/2;
			this.logo.y=(this.height-this.logo.height)/2;
			this.addChild(this.logo);
		}
		
		public function hide():void
		{
			this.visible=false;
		}
		
		public function get rect():Rectangle
		{
			return new Rectangle(this.x, this.y, this.width, this.height);
		}
	}
}
