package game7
{
	import flash.display.Bitmap;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import ru.flexdev.events.EventEx;
	import ru.flexdev.ui.Rect;
	import ru.flexdev.ui.SpriteEx;
	
	public class Product extends Rect
	{
		public var data:Object;
		private var logo:Bitmap;
		public var tries:int=1;
			
		public function Product(data:Object)
		{
			super();
			this.width=77;
			this.height=77;
			this.fill(0xffffff).stroke(0xe3e3e3, 1, 2, false).round(77, 77);
			
			this.data=data;
			this.logo=new data['image']() as Bitmap;
			this.logo.smoothing=true;
			this.addChild(this.logo);
			this.logo.x=(this.width-this.logo.width)/2;
			this.logo.y=(this.height-this.logo.height)/2;
			
			var position:Point=data['position'];
			this.x=position.x;
			this.y=position.y;
			
			this.addEventListener(MouseEvent.MOUSE_DOWN, this.onMouseDown);
		}
		
		private function onMouseDown(event:MouseEvent):void
		{
			this.dispatchEvent(new EventEx('product_select', this, true));
		}
		
		public function showLogo(value:Boolean):void
		{
			this.logo.visible=value;
		}
		
		public function get logoPosition():Point
		{
			return new Point(this.x+this.logo.y, this.y+this.logo.y);
		}
	}
}
