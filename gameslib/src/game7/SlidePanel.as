package game7
{
	import caurina.transitions.Tweener;
	
	import flash.display.Bitmap;
	
	import ru.flexdev.ui.SpriteEx;
	
	public class SlidePanel extends SpriteEx
	{
		public function SlidePanel()
		{
			super();
		}
		
		public function slide(product:Product, logo:Bitmap):void
		{
			this.addChild(logo);
			Tweener.addTween(logo, {alpha:1, x:product.logoPosition.x, y:product.logoPosition.y, time:0.5, transition:'linear', onComplete:this.onComplete, onCompleteParams:[logo]});
		}
		
		public function onComplete(logo:Bitmap):void
		{
			logo.visible=false;
		}
	}
}