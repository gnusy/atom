package game7
{
	import caurina.transitions.Tweener;
	
	import common.Background;
	import common.Common;
	import common.Helper;
	
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.ui.Mouse;
	import flash.utils.setTimeout;
	
	import fontmanager.FontManager;
	
	import ru.flexdev.events.EventEx;
	import ru.flexdev.ui.Rect;
	
	public class Game7 extends Sprite
	{
		public static const WIDTH:int=872;
		public static const HEIGHT:int=554;
		
		[Embed(source="assets/image0.png")]private var product0:Class;
		[Embed(source="assets/image1.png")]private var product1:Class;
		[Embed(source="assets/image2.png")]private var product2:Class;
		[Embed(source="assets/image3.png")]private var product3:Class;
		
		[Embed(source="assets/hint.png")]private var hintClass:Class;
		
		private var background:Background;
		private var helper:Helper;
		private var agregat:Agregat;
		private var slidePanel:SlidePanel;
		
		private var targets_data:Array=[
			{product_id:0, text:'Атомная энергетика\n(основная продукция)'},
			{product_id:1, text:'Судостроение'},
			{product_id:2, text:'Атомная энергетика\n(новые продукты)'},
			{product_id:3, text:'Смежные\nбизнес-направления'},
		];
		
		private var product_data:Array=[
			{product_id:0, image:product0, position:new Point(116, 173)},
			{product_id:1, image:product1, position:new Point(116, 259)},
			{product_id:2, image:product2, position:new Point(116, 345)},
			{product_id:3, image:product3, position:new Point(116, 431)}
		];
		
		private var targets:Vector.<DropTarget>;
		private var products:Array;
		
		private var dragItem:DragItem;
		public var errors:int=0;
		
		private var backMask:Rect;
		private var hint:Bitmap;
		
		public function Game7()
		{
			super();
			this.background=new Background(WIDTH, HEIGHT);
			this.addChild(this.background);
			this.background.setTitles('Найдите правильное соответствие между ключевыми продуктами\nи соответствующими направлениями в структуре портфеля дивизиона', '');
			this.background.hideButton('toMenu').hideButton('toMap');
			this.background.setTitleFormat('title1', FontManager.instance.textFormat(21, Common.GRAY, 'regular'));
			this.background.setTitleFormat('title2', FontManager.instance.textFormat(17, Common.GRAY, 'light'));
			this.background.topTitle('title1', 40);
			this.background.topTitle('title2', 73);
			this.background.leftTitle('title1', 35);
			this.background.leftTitle('title2', 35);
			

			this.targets_data.sort(this.shuffle);
			
			this.targets=new Vector.<DropTarget>();
			
			for(var i:int=0;i<this.targets_data.length;i++)
			{
				var dt:DropTarget=new DropTarget(this.targets_data[i]);
				dt.position=new Point(476, 212+86*i);
				this.addChild(dt);
				this.targets.push(dt);
			}
			
			this.products=new Array();
			
			for(i=0;i<this.product_data.length;i++)
			{
				var product:Product=new Product(this.product_data[i]);
				this.addChild(product);
				this.products.push(product);
			}
			
			this.addEventListener('product_select', this.onProductSelect);
			
			this.dragItem=new DragItem();
			this.addChild(this.dragItem);
			
			
			this.agregat=new Agregat();
			this.agregat.bounds(118, 124);
			this.addChild(this.agregat);
			
			
			this.slidePanel=new SlidePanel();
			this.addChild(this.slidePanel);
			
			this.helper=new Helper('Хорошо!', 'Вы правильно определили ключевые продукты машиностроительного дивизиона.');
			this.helper.bounds(0, 0);
			this.helper.width=WIDTH;
			this.helper.height=HEIGHT;
			this.addChild(this.helper);
			this.helper.addEventListener(Event.CLOSE, this.onHelperClose);
			
			
			this.addEventListener('return_product', this.onReturnProduct);
			
			this.backMask=new Rect();
			this.backMask.width=1024;
			this.backMask.height=768;
			this.backMask.fill(0x000000, 0.2);
			this.addChild(this.backMask);
			this.hint=new hintClass() as Bitmap;
			this.hint.smoothing=true;
			this.addChild(this.hint);
			this.hint.x=-68;
			this.hint.y=164;
			
			this.addEventListener(MouseEvent.CLICK, this.onMouseClick);
		}
		
		private function onMouseClick(event:MouseEvent):void
		{
			this.removeEventListener(MouseEvent.CLICK, this.onMouseClick);
			this.hideDarkness();
		}
		
		private function onProductSelect(event:EventEx):void
		{
			if(this.mouseEnabled==false)return;
			var product:Product=event.data;
			product.showLogo(false);
			
			this.dragItem.show(product);
			
			this.dragItem.position=this.globalToLocal(new Point(this.stage.mouseX, this.stage.mouseY));
			this.addEventListener(MouseEvent.MOUSE_MOVE, this.onMouseEvent);
			this.addEventListener(MouseEvent.MOUSE_UP, this.onMouseEvent);
		}
		
		private function onMouseEvent(event:MouseEvent):void
		{
			switch(event.type)
			{
				case MouseEvent.MOUSE_MOVE:
					this.dragItem.position=this.globalToLocal(new Point(event.stageX, event.stageY));
					break;
				
				case MouseEvent.MOUSE_UP:
					this.removeEventListener(MouseEvent.MOUSE_MOVE, this.onMouseEvent);
					this.removeEventListener(MouseEvent.MOUSE_UP, this.onMouseEvent);
					this.dragItem.hide();
					this.checkDropTargets();
					break;
			}
			
		}
		
		private function checkDropTargets():void
		{
			this.mouseEnabled=false;
			
			for(var i:int=0;i<this.targets.length;i++)
			{
				if(this.targets[i].intersects(this.dragItem.rect)==false)continue;
				
				var res:int=this.targets[i].checkProduct(this.dragItem.product);
				if(res==1)
				{
					this.agregat.addSuccess(this.targets[i]);
					if(this.agregat.correct==true)setTimeout(this.testComplete, 1500);
				}
				else if(res==-1)
				{
					this.errors++;
				}
				
				else if(res==0)
				{
					//this.errors++;
				}
				
				setTimeout(this.setMouseEnabled, 1500);
				return;
			}
			this.dragItem.product.showLogo(true);
			this.setMouseEnabled();
		}
		
		private function onReturnProduct(event:EventEx):void
		{
			this.slidePanel.slide(this.dragItem.product, event.data);
			setTimeout(this.dragItem.product.showLogo, 500, true);
		}
		
		private function setMouseEnabled():void
		{
			this.mouseEnabled=true;
		}
		
		private function testComplete():void
		{
			this.helper.show();
		}
		
		private function onHelperClose(event:Event):void
		{
			this.helper.hide();
			var scores:int=0;
			if(this.errors==0)scores=5;
			else if(this.errors==1)scores=3;
			else scores=1;

			this.dispatchEvent(new EventEx('game_score', scores, true));
			this.dispatchEvent(new Event(Event.COMPLETE));
		}
		
		public function center(parentWidht:int, parentHeight:int):void
		{
			this.x=(parentWidht-WIDTH)/2;
			this.y=(parentHeight-HEIGHT)/2;
			
			var local:Point=this.globalToLocal(new Point(0, 0));
			this.backMask.x=local.x;
			this.backMask.y=local.y;
			
			//this.backMask.x=-this.x;
			//this.backMask.y=-this.y;
		}
		
		private function hideDarkness():void
		{
			Tweener.addTween(this.backMask, {alpha:0, time:0.5, transition:'linear', onComplete:this.onDarknessHideComplete});
			Tweener.addTween(this.hint, {alpha:0, time:0.5, transition:'linear'});
		}
		
		private function onDarknessHideComplete():void
		{
			this.backMask.visible=false;
			this.hint.visible=false;
		}
		
		public function shuffle(val1:Object, val2:Object):Number
		{
			return Math.random()>0.5?1:-1;
		}
				
	}
}