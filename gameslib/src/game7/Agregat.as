package game7
{
	import caurina.transitions.Tweener;
	
	import common.Common;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.utils.setTimeout;
	
	import ru.flexdev.ui.SpriteEx;
	
	public class Agregat extends SpriteEx
	{
		[Embed(source="assets/agregat.png")]private var agregatClass:Class;
		[Embed(source="assets/mark_right.png")]private var markClass:Class;
		
		private var count_successed:int=0;
		private var max_successed:int=4;
		private var marksHolder:Sprite;
		
		
		private var bmp:Bitmap;
		
		public function Agregat()
		{
			super();
			this.mouseEnabled=false;
			var img:Bitmap=new agregatClass() as Bitmap;
			this.addChild(img);
			
			
			this.marksHolder=new Sprite();
			this.addChild(this.marksHolder);
			
			this.update();
		}
		
		public function addSuccess(target:DropTarget):void
		{
			if(this.count_successed==max_successed)return;
			this.count_successed++;
		
			var bmpd:BitmapData=new BitmapData(target.width+1, target.height+1, true, 0x00ffffff);
			bmpd.draw(target);
			
			this.bmp=new Bitmap(bmpd);
			this.bmp.smoothing=true;
			
			var point:Point=this.globalToLocal(target.localToGlobal(new Point()));
			this.bmp.x=point.x;
			this.bmp.y=point.y;
			this.addChild(this.bmp);
			setTimeout(this.slide, 1000);
			
			
		}
		
		private function slide():void
		{
			Tweener.addTween(this.bmp, {x:639, y:105+(this.count_successed-1)*54, scaleX:40/77, scaleY:40/77, time:0.5, transition:'linear', onComplete:this.update});
		}
		
		private function update():void
		{
			if(this.bmp!=null)this.removeChild(this.bmp);
			
			Common.removeChildren(this.marksHolder);
			this.graphics.clear();
			
			this.graphics.lineStyle(0, 0xe3e3e3);
			this.graphics.beginFill(0xffffff);
			for(var i:int=0;i<4;i++)
			{
				this.graphics.drawCircle(639+40/2, 125+i*54, 40/2);
			}
			this.graphics.endFill();
			
			this.graphics.lineStyle(0, 0x32ce1b);
			
			
			for(i=0;i<this.count_successed;i++)
			{
				this.graphics.beginFill(0x32ce1b);
				this.graphics.drawCircle(639+40/2, 125+i*54, 40/2);
				this.graphics.endFill();	
				
				var img:Bitmap=new markClass() as Bitmap;
				this.marksHolder.addChild(img);
				
				img.x=638+40/2-img.width/2;
				img.y=124.5+i*54-img.height/2;

			}
			
		}
		
		public function get correct():Boolean
		{
			return this.count_successed==max_successed;
		}
	}
}