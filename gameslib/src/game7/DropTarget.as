package game7
{
	import common.Common;
	
	import flash.display.Bitmap;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import flash.utils.setTimeout;
	
	import fontmanager.FontManager;
	
	import ru.flexdev.events.EventEx;
	import ru.flexdev.ui.Rect;
	
	public class DropTarget extends Rect
	{
		[Embed(source="assets/mark_right_big.png")]private var mark_right:Class;
		[Embed(source="assets/mark_wrong_big.png")]private var mark_wrong:Class;
		
		private var mark:Bitmap;
		private var product_id:int=-1;
		private var _type:String;
		
		private var product:Product;
		private var logo:Bitmap;
		
		public function DropTarget(data:Object)
		{
			super();
			this.product_id=data['product_id'];
			
			this.width=77;
			this.height=77;
			this.round(this.width, this.height);
			this.type='normal';
			
			var text:TextField=FontManager.instance.textField;
			text.defaultTextFormat=FontManager.instance.textFormat(14, Common.GRAY, 'bold');//Common.GRAY
			text.wordWrap=true;
			text.x=91;
			text.width=160;
			this.addChild(text);
			text.text=data['text'];
			text.height=text.textHeight+4;
			text.y=(this.height-text.height)/2;
		}
		
		private function set type(value:String):void
		{
			this._type=value;
			if(this.mark!=null)this.mark.visible=false;
			switch(value)
			{
				case 'normal':
					this.fill(0xe3e3e3).stroke(0xffffff, 1, 2, false);
					break;
				
				case 'right':
					this.fill(0x32ce1b).stroke(0xffffff, 1, 2, false);
					this.mark=new mark_right() as Bitmap;
					this.addChild(this.mark);
					this.mark.x=(this.width-this.mark.width)/2;
					this.mark.y=(this.height-this.mark.height)/2;
					setTimeout(this.showProduct, 1000);
					break;
				
				case 'wrong':
					this.fill(0xffffff).stroke(0xe3e3e3, 1, 2, false);
					this.mark=new mark_wrong() as Bitmap;
					this.addChild(this.mark);
					this.mark.x=(this.width-this.mark.width)/2;
					this.mark.y=(this.height-this.mark.height)/2;
					setTimeout(this.clear, 1000);
					break;
			}
		}
		
		public function set position(value:Point):void
		{
			this.x=value.x-this.width/2;
			this.y=value.y-this.height/2;
		}
		
		public function intersects(value:Rectangle):Boolean
		{
			return new Rectangle(this.x, this.y, this.width, this.height).intersects(value);
		}
		
		public function checkProduct(product:Product):int
		{
			this.product=product;
			if(this.product_id==product.data['product_id'])
			{
				if(this._type=='right')
				{
					this.clear(false);
					return 0;
				}
				this.type='right';
				return 1;
			}
			else if(this.product_id!=product.data['product_id'])
			{
				if(this._type!='normal')
				{
					this.clear(false);
					return 0;
				}
				this.type='wrong';
				return -1;
			}
			this.clear(false);
			return 0;
		}
		
		private function clear(show_normal:Boolean=true):void
		{
			if(show_normal)this.type='normal';
			this.logo=new this.product.data['image']() as Bitmap;
			this.logo.smoothing=true;
			
			var point:Point=new Point((this.width-this.logo.width)/2, (this.height-this.logo.height)/2);
			//point=this.localToGlobal(point);
			this.logo.x=point.x+this.x;
			this.logo.y=point.y+this.y;
			this.logo.alpha=0;
			this.dispatchReturnProduct();
			
		}
		
		private function dispatchReturnProduct():void
		{
			this.dispatchEvent(new EventEx('return_product', this.logo, true));
		}
		
		
		private function showProduct():void
		{
			this.fill(0xffffff).stroke(0xe3e3e3, 1, 2, false);
			this.logo=new this.product.data['image']() as Bitmap;
			this.logo.smoothing=true;
			
			var point:Point=new Point((this.width-this.logo.width)/2, (this.height-this.logo.height)/2);
			this.logo.x=point.x;
			this.logo.y=point.y;
			this.addChild(this.logo)
		}
	}
}