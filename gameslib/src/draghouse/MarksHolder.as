package draghouse
{
	import caurina.transitions.Tweener;
	
	import flash.display.Bitmap;
	import flash.geom.Rectangle;
	import flash.utils.setTimeout;
	
	import ru.flexdev.ui.SpriteEx;
	
	public class MarksHolder extends SpriteEx
	{
		[Embed(source="assets/mark_right.png")]private var mark_right:Class;
		[Embed(source="assets/mark_wrong.png")]private var mark_wrong:Class;
		
		
		private var marks:Object;
		
		public function MarksHolder()
		{
			super();
			this.bounds(0, 0, 0, 0);
			this.mouseEnabled=false;
			this.marks=new Object();
		}
		
		public function bringToFront():void
		{
			this.parent.setChildIndex(this, this.parent.numChildren-1);
		}
		
		public function showMark(rect:Rectangle, type:String):void
		{
			this.bringToFront();
			
			var key:String=rect.x.toString()+'-'+rect.y.toString();
			if(marks.hasOwnProperty(key))
			{
				this.removeChild(marks[key]);
			}
			
			var img:Bitmap;
			if(type=='right')img=new mark_right() as Bitmap; 
			else img=new mark_wrong() as Bitmap;
			
			img.x=rect.x+rect.width-img.width-10;
			img.y=rect.y+rect.height-img.height-10;
			this.addChild(img);
			img.alpha=0;
			this.marks[key]=img;
			if(type=='right')setTimeout(this.show, 250, img);
			else this.show(img);
		}
		
		private function show(mark:Bitmap):void
		{
			Tweener.addTween(mark, {alpha:1, time:0.25, transition:'linear'});
		}
	}
}