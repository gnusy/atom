package draghouse
{
	import caurina.transitions.Tweener;
	
	import flash.display.Bitmap;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import ru.flexdev.events.EventEx;
	import ru.flexdev.ui.Rect;
	import ru.flexdev.ui.SpriteEx;
	
	public class Card extends SpriteEx
	{
		public var data:Object;
		private var dragged:Boolean=false;
		private var _fixed:Boolean=false;
		
		public function Card(data:Object)
		{
			super();
			
			this.data=data;
			var img:Bitmap=new data['image']() as Bitmap;
			img.smoothing=true;
			this.addChild(img);
			
			this.width=img.width;
			this.height=img.height;
			
			
			this.x=data['initPosition'].x;
			this.y=data['initPosition'].y;
			
			this.buttonMode=true;
			
			var rect:Rect=new Rect();
			this.addChildAt(rect, 0);
			rect.fill(0xffffff).bounds(4, 4, 4, 4);
			
			
			this.addEventListener(MouseEvent.MOUSE_DOWN, this.onMouseEvent);
		}
		
		public function get fixed():Boolean
		{
			return this._fixed;
		}

		public function fix(position:Point):void
		{
			this._fixed = true;
			this.buttonMode=false;
			Tweener.addTween(this, {x:position.x, y:position.y, time:0.25 });
		}

		public function get rect():Rectangle
		{
			return new Rectangle(this.x, this.y, this.width, this.height);
		}
		
		public function get center():Point
		{
			return new Point(this.x+this.width/2, this.y+this.height/2);
		}
		
		private function onMouseEvent(event:MouseEvent):void
		{
			if(this._fixed==true)return;
			switch(event.type)
			{
				case MouseEvent.MOUSE_DOWN:
					this.parent.setChildIndex(this, this.parent.numChildren-1);
					this.startDrag(false);
					this.dragged=true;
					
					this.addEventListener(MouseEvent.MOUSE_UP, this.onMouseEvent);
					this.addEventListener(MouseEvent.MOUSE_OUT, this.onMouseEvent);
					this.addEventListener(MouseEvent.MOUSE_MOVE, this.onMouseEvent);
					break;
				
				case MouseEvent.MOUSE_UP:
			//	case MouseEvent.MOUSE_OUT:
					this.stopDrag();
					this.dragged=false;
					this.dispatchEvent(new EventEx('item_drag_end', this, true));
					this.removeEventListener(MouseEvent.MOUSE_UP, this.onMouseEvent);
					this.removeEventListener(MouseEvent.MOUSE_OUT, this.onMouseEvent);
					this.removeEventListener(MouseEvent.MOUSE_MOVE, this.onMouseEvent);
					
					break;
				
				case MouseEvent.MOUSE_MOVE:
					if(this.dragged==false)return;
					this.dispatchEvent(new EventEx('item_dragged', this, true));
					break;
			}
		}
		
		public function returnToInitPosition():void
		{
			if(this.fixed==true)return;
			Tweener.addTween(this, {x:this.data['initPosition'].x, y:this.data['initPosition'].y, time:0.25 });
		}
	}
}