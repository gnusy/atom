package game1
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import common.ToolTip;
	
	import fontmanager.FontManager;
	
	public class Texts extends Sprite
	{

		private var vtexts:Array=['АЭМ-Технологии', 'ЗиО-Подольск', 'ЦНИИТМАШ', 'ЦКБМ', 'ГИДРОПРЕСС', 'ОКБМ Африкантов', 'ARAKO', 'СвердНИИхиммаш', 'СНИИП', 'Турбинные технологии\nААЭМ', 'ЭМСС', 'ВНИИАМ'];
		private var htexts:Array=['Атомная\nэнергетика', 'СКУ РУ \nВВЭР', 'РАО/ОЯТ', 'Водоподго-\nтовка и оп-\nреснение', 'Судо-\nстроение', 'Спецстали', 'Тепловая \nэнергетика', 'Газнефте-\nхимия', 'Общая \nтехника', 'ОПЭБ'];
	
		
		private var tooltips:Array=['Системы контроля и управления реакторными установками водо-водяных энергетических реакторов', 'Радиоактивные отходы / отработавшее ядерное топливо', 'Оптимизированный плавучий энергоблок'];
		
		private var needTooltipTexts:Array;
		
		
		private var toolTip:ToolTip;
		
		public function Texts()
		{
			super();
			var frmt:TextFormat=FontManager.instance.textFormat(10, 0x4d4d4d, 'bold');
			frmt.align='right';
			
			for(var i:int=0;i<12;i++)
			{
				var vt:TextField=FontManager.instance.textField;
				vt.defaultTextFormat=frmt;
				vt.x=0;
				vt.y=192+i*Block.HEIGHT;
				if(i==9)vt.y=vt.y-5;
				vt.width=142;
				vt.text=this.vtexts[i];
				this.addChild(vt);
			}
			
			this.needTooltipTexts=new Array();
			
			frmt.align='left';
			
			for(i=0;i<10;i++)
			{
				var ht:TextField=FontManager.instance.textField;
				ht.defaultTextFormat=frmt;
				ht.wordWrap=true;
				ht.text=this.htexts[i];
				ht.x=162+Block.WIDTH*i;
				ht.y=169-ht.textHeight-1;
				ht.width=Block.WIDTH;
				ht.height=ht.textHeight+5;
				
				this.addChild(ht);
				if(i==1 || i==2 || i==9)
				{
					this.needTooltipTexts.push(ht);
					ht.mouseEnabled=true;
					ht.addEventListener(MouseEvent.ROLL_OVER, this.onMouseEvent);
					ht.addEventListener(MouseEvent.ROLL_OUT, this.onMouseEvent);
				}
			}
			
			this.toolTip=new ToolTip();
			this.addChild(this.toolTip);
		}
		
		private function onMouseEvent(event:MouseEvent):void
		{
			if(event.type==MouseEvent.ROLL_OVER)
			{
				var tf:TextField=(event.currentTarget as TextField);
				
				switch(tf)
				{
					case this.needTooltipTexts[0]:
						this.toolTip.show(this.tooltips[0], new Point(tf.x+tf.width/2, 145));
						break;
					
					case this.needTooltipTexts[1]:
						this.toolTip.show(this.tooltips[1], new Point(tf.x+tf.width/2, 145));
						break;
					
					case this.needTooltipTexts[2]:
						this.toolTip.show(this.tooltips[2], new Point(tf.x+tf.width/2, 145));
						break;
				}
			}
			else this.toolTip.hide();
		}
	}
}
