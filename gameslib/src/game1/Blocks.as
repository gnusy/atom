package game1
{
	import flash.display.Sprite;
	import flash.events.Event;
	
	public class Blocks extends Sprite
	{
		private var map:Array=[
								[1, 0, 1, 1, 1, 0, 1, 1, 0, 1],
								[1, 0, 0, 1, 1, 0, 1, 1, 1, 1],
								[1, 0, 0, 1, 0, 1, 1, 0, 0, 0],
								[1, 0, 1, 0, 0, 0, 0, 0, 1, 0],
								[1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
								[1, 0, 1, 0, 1, 0, 0, 1, 1, 1],
								[1, 0, 0, 0, 1, 0, 1, 1, 0, 0],
								[1, 0, 1, 1, 0, 0, 0, 1, 0, 0],
								[1, 1, 0, 0, 1, 0, 0, 0, 1, 0],
								[1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
								[1, 0, 0, 0, 1, 1, 1, 1, 0, 1],
								[1, 0, 0, 1, 0, 0, 0, 0, 0, 0]
							];
		
		private var count:int=0;
		
		public function Blocks()
		{
			super();
			for(var i:int=0;i<10;i++)
			{
				for(var j:int=0;j<12;j++)
				{
					var block:Block=new Block(map[j][i]);
					block.x=i*Block.WIDTH;
					block.y=j*Block.HEIGHT;
					this.addChild(block);
					block.addEventListener('select', this.onBlockClick);
					this.count+=map[j][i];
				}
			}
		}
		
		private function onBlockClick(event:Event):void
		{
			if((event.currentTarget as Block).selected)
			{
				this.count--;

				if(this.count==0)
				{
					this.dispatchEvent(new Event('blocks_complete'));
				}
			}
			
			
		}
	}
}
