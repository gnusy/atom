package game1
{
	import flash.display.GradientType;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Matrix;
	
	public class Block extends Sprite
	{
		public static var WIDTH:int=65;
		public static var HEIGHT:int=26;
		
		
		private var _state:String;
		private var active:int=1;
		
		
		
		public function Block(active:int)
		{
			super();
			this.active=active;
			this.state='normal';
			this.addEventListener(MouseEvent.ROLL_OVER, this.onMouseEvent);
			this.addEventListener(MouseEvent.ROLL_OUT, this.onMouseEvent);
			this.addEventListener(MouseEvent.CLICK, this.onMouseEvent);

		}

		private function get state():String
		{
			return this._state;
		}

		private function set state(value:String):void
		{
			this._state = value;
			this.draw();
		}

		private function onMouseEvent(event:MouseEvent):void
		{
			if(this.state=='selected')return;
			switch(event.type)
			{
				case MouseEvent.ROLL_OVER:
					this.state='over';
					break;
				
				case MouseEvent.ROLL_OUT:
					this.state='normal';
					break;
				
				case MouseEvent.CLICK:
					if(this.active==0)return;
					this.state='selected';
					this.dispatchEvent(new Event('select'));
					break;
			}
		}
		
		private function draw():void
		{
			this.graphics.clear();
			
			switch(this.state)
			{
				case 'normal':
					this.graphics.beginFill(0xe3e3e3);
					this.graphics.drawRect(1, 1, WIDTH-2, HEIGHT-2);
					this.graphics.endFill();
					
					this.filters=[];
					break;
				
				
				case 'over':
					
					this.graphics.beginFill(0xe3e3e3);
					this.graphics.drawRect(0, 0, WIDTH, HEIGHT);
					this.graphics.endFill();
					
					
					matrix=new Matrix();
					matrix.createGradientBox(WIDTH, HEIGHT/2, Math.PI/2);
					this.graphics.beginGradientFill(GradientType.LINEAR, [0xb5b5b5,  0xe3e3e3], [1, 0], [0, 255], matrix);
					this.graphics.drawRect(0, 0, WIDTH, HEIGHT/2);
					this.graphics.endFill();
					
					matrix.createGradientBox(WIDTH/4, HEIGHT, 0);
					this.graphics.beginGradientFill(GradientType.LINEAR, [0xb5b5b5,  0xe3e3e3], [1, 0], [0, 255], matrix);
					this.graphics.drawRect(0, 0, WIDTH/4, HEIGHT);
					this.graphics.endFill();
					
					matrix.createGradientBox(WIDTH/4, HEIGHT, 0);
					matrix.tx=7*WIDTH/8;
					this.graphics.beginGradientFill(GradientType.LINEAR, [0xe3e3e3, 0xb5b5b5], [0, 1], [0, 255], matrix);
					this.graphics.drawRect(3*WIDTH/4, 0, WIDTH/4, HEIGHT);
					this.graphics.endFill();
					
					matrix.createGradientBox(WIDTH, 20, Math.PI/2);
					this.graphics.beginGradientFill(GradientType.LINEAR, [0xb5b5b5,  0xcccccc], [1, 0], [0, 255], matrix);
					this.graphics.drawRect(0, 0, WIDTH, 20);
					this.graphics.endFill();
				
					break;
				
				case 'selected':
					var matrix:Matrix=new Matrix();
					matrix.createGradientBox(WIDTH, HEIGHT, Math.PI/2);
					this.graphics.beginGradientFill(GradientType.LINEAR, [0xfad29a, 0xf6b249], [1, 1], [0, 255], matrix);
					this.graphics.drawRect(1, 1, WIDTH-2, HEIGHT-2);
					this.graphics.endFill();
					this.filters=[];
					break;
					
			}
		}
		
		public function get selected():Boolean
		{
			if(this.active==1)return true;
			return false;
		}
	}
}
