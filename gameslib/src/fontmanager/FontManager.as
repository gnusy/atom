package fontmanager
{
	import flash.text.AntiAliasType;
	import flash.text.TextField;
	import flash.text.TextFormat;

	public class FontManager
	{
		[Embed(source="assets/TornadoCyr-Bold.otf", fontFamily="TornadoCyrBold", embedAsCFF="false", advancedAntiAliasing="true")] private var font0:Class;
		[Embed(source="assets/TornadoCyr-Light.otf", fontFamily="TornadoCyrLight", embedAsCFF="false", advancedAntiAliasing="true")] private var font1:Class;
		[Embed(source="assets/TornadoCyr-Regular.otf", fontFamily="TornadoCyrRegular", embedAsCFF="false", advancedAntiAliasing="true")] private var font2:Class;
		[Embed(source="assets/TornadoCyr-Medium.otf", fontFamily="TornadoCyrMedium", embedAsCFF="false", advancedAntiAliasing="true")] private var font3:Class;
		[Embed(source="assets/TornadoCyr-Black.otf", fontFamily="TornadoCyrBlack", embedAsCFF="false", advancedAntiAliasing="true")] private var font4:Class;
		
		[Embed(source="assets/digital.ttf", fontFamily="Digital", embedAsCFF="false", advancedAntiAliasing="true")] private var font5:Class;
		
		
		
		private static var _instance:FontManager;
		
		public static function get instance():FontManager
		{
			if(_instance==null)_instance=new FontManager();
			return _instance;
		}		
		
		public function FontManager()
		{
		}
		
		public function textFormat(size:Number, color:int, type:String='regular'):TextFormat
		{
			var tf:TextFormat=new TextFormat();
			
			tf.color=color;
			tf.size=size;
			
			switch(type)
			{
				case 'regular':
					tf.font='TornadoCyrRegular';
					break;
				case 'light':
					tf.font='TornadoCyrLight';
					break;
				case 'bold':
					tf.font='TornadoCyrBold';
					break;
				case 'medium':
					tf.font='TornadoCyrMedium';
					break;
				case 'black':
					tf.font='TornadoCyrBlack';
					break;
				
				case 'digital':
					tf.font='Digital';
					break;
				
			}
			return tf;
		}
		
		public function get textField():TextField
		{
			var tf:TextField=new TextField();
			tf.antiAliasType=AntiAliasType.ADVANCED;
			tf.embedFonts=true;
			tf.selectable=false;
			tf.mouseEnabled=false;
			return tf;
		}
	}
}
