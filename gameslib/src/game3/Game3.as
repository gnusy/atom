package game3
{
	import common.Background;
	import common.Helper;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import ru.flexdev.events.EventEx;
	
	public class Game3 extends Sprite
	{
		public static const WIDTH:int=871;
		public static const HEIGHT:int=528;
		
		private var background:Background;
		private var tests:Tests;
		private var helper:Helper;
		
		public function Game3()
		{
			super();
			this.background=new Background(WIDTH, HEIGHT);
			this.addChild(this.background);
			this.background.setTitles('Ответьте на вопрос, выбрав одно из двух предприятий');
			this.background.hideButton('toMenu').hideButton('toMap');
			
			this.tests=new Tests();
			this.tests.addEventListener('test_complete', this.onTestComplete);
			this.addChild(this.tests);
			this.tests.y=134;
			
			
			this.helper=new Helper('Прекрасно!', 'Вы вооружились знаниями о важных событиях в истории предприятий дивизиона.');
			this.helper.bounds(0, 0);
			this.helper.width=WIDTH;
			this.helper.height=HEIGHT;
			this.addChild(this.helper);
			this.helper.addEventListener(Event.CLOSE, this.onHelperClose);
			
			this.addEventListener(MouseEvent.MOUSE_DOWN, this.onMouseClick);
			
		}
		
		private function onMouseClick(event:MouseEvent):void
		{
			if(event.altKey && event.ctrlKey)this.onHelperClose(null);
		}
		
		private function onTestComplete(event:EventEx):void
		{
			this.helper.show();
		}
		
		private function onHelperClose(event:Event):void
		{
			this.helper.hide();
			this.dispatchEvent(new Event(Event.COMPLETE));
			this.dispatchEvent(new EventEx('game_score', this.tests.score, true));
			
		}
		
		public function center(parentWidht:int, parentHeight:int):void
		{
			this.x=(parentWidht-WIDTH)/2;
			this.y=(parentHeight-HEIGHT)/2;
		}
	}
}
