package game3
{
	import caurina.transitions.Tweener;
	
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import fontmanager.FontManager;
	
	public class Company extends Sprite
	{
		public static const WIDTH:int=352;
		public static const HEIGHT:int=254;

		private var data:Object;
		private var correct:Boolean;
		private var header:Sprite;
		private var blinkSprite:Sprite;
	
		public function Company(data:Object, correct:Boolean)
		{
			super();
			this.data=data;
			this.correct=correct;
			
			//data.name;
			var image:Bitmap=new data['image']() as Bitmap;
			image.width=WIDTH;
			image.height=HEIGHT;
			this.addChild(image);
			this.buttonMode=true;
			this.addEventListener(MouseEvent.CLICK, this.onClick);
			
			
			this.header=new Sprite();
			this.header.graphics.beginFill(0x6d6e71, 0.9);
			this.header.graphics.drawRect(0, 0, 352, 55);
			this.header.graphics.endFill();
			this.addChild(this.header);
			
			var frmt:TextFormat=FontManager.instance.textFormat(17, 0xffffff, 'bold');
			var tf:TextField=FontManager.instance.textField;
			tf.defaultTextFormat=frmt;
			tf.width=200;
			tf.x=16;
			tf.y=16;
			tf.text=data.name;
			this.addChild(tf);
			
			
			this.blinkSprite=new Sprite();
			this.blinkSprite.graphics.beginFill(0xff0000, 0.5);
			this.blinkSprite.graphics.drawRect(0, 55, 352, 254-55);
			this.blinkSprite.graphics.endFill();
			this.blinkSprite.mouseEnabled=false;
			this.blinkSprite.alpha=0;
			this.addChild(this.blinkSprite);
		}
		
		private function onClick(event:MouseEvent):void
		{
			if(this.correct)
			{
				this.header.graphics.clear();
				this.header.graphics.beginFill(0xf8ad3b, 0.9);
				this.header.graphics.drawRect(0, 0, 352, 55);
				this.header.graphics.endFill();
				
				this.dispatchEvent(new Event('correct', true));
			}
			else
			{
				this.dispatchEvent(new Event('incorrect', true));
				Tweener.addTween(this.blinkSprite, {alpha:1, time:0.25, onComplete:this.onBlink});
			}
		}
		
		private function onBlink():void
		{
			Tweener.addTween(this.blinkSprite, {alpha:0, time:0.25});
		}
	}
}