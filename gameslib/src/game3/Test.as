package game3
{
	import caurina.transitions.Tweener;
	
	import common.Common;
	
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import fontmanager.FontManager;
	
	public class Test extends Sprite
	{
		private var data:Object;

		
		public function Test(data:Object)
		{
			super();
			this.visible=false;
			this.alpha=0;
			//this.x=Game3.WIDTH;
			
			this.data=data;
			var frmt:TextFormat=FontManager.instance.textFormat(17, Common.GRAY, 'light');
			var tf:TextField=FontManager.instance.textField;
			tf.defaultTextFormat=frmt;
			tf.width=760;
			tf.wordWrap=true;
			tf.x=52;
			tf.y=0;
			tf.text=data.question;
			this.addChild(tf);
			
			var company:Company=new Company(data.companies[0], data.correct==0);
			company.x=56;
			company.y=75;
			this.addChild(company);
			
			company=new Company(data.companies[1], data.correct==1);
			company.x=464;
			company.y=75;
			this.addChild(company);
		}
		
		public function show(now:Boolean=false):void
		{
			this.visible=true;
			//if(now==false)Tweener.addTween(this, {x:0, time:0.75, transition:'linear'});
			if(now==false)Tweener.addTween(this, {alpha:1, time:0.75, transition:'linear'});
			else this.alpha=1;
		}
		
		public function hide():void
		{
			//Tweener.addTween(this, {alpha:0, x:-Game3.WIDTH, time:1, transition:'linear', onComplete:this.onHideComplete});
			Tweener.addTween(this, {alpha:0, time:1, transition:'linear', onComplete:this.onHideComplete});
		}
		
		private function onHideComplete():void
		{
			this.visible=false;
			this.parent.removeChild(this);
		}
	}
}
